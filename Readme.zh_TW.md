# 關於這本書:

網頁版本: https://aesthetic-programming.net/

Git/原始檔:

- 簡介 (https://gitlab.com/aesthetic-programming/book/-/tree/master/source.zh_TW)
- 目錄 (https://gitlab.com/aesthetic-programming/book/-/blob/master/source.zh_TW/Table_Of_Contents.md)
- 前言 (https://gitlab.com/aesthetic-programming/book/-/tree/master/source.zh_TW/0-Preface)
- 設計說明 (https://gitlab.com/aesthetic-programming/book/-/blob/master/source.zh_TW/OSP.md)
- 第一章：入門 (https://gitlab.com/aesthetic-programming/book/-/tree/master/source.zh_TW/1-GettingStarted)
- 第二章：變數幾何 (https://gitlab.com/aesthetic-programming/book/-/tree/master/source.zh_TW/2-VariableGeometry)
- 第三章：無限迴圈 (https://gitlab.com/aesthetic-programming/book/-/tree/master/source.zh_TW/3-InfiniteLoops)
- 第四章：資料擷取 (https://gitlab.com/aesthetic-programming/book/-/tree/master/source.zh_TW/4-DataCapture)
- 第五章：自動產生器 (https://gitlab.com/aesthetic-programming/book/-/tree/master/source.zh_TW/5-AutoGenerator)
- 第六章：物件抽象化 (https://gitlab.com/aesthetic-programming/book/-/tree/master/source.zh_TW/6-ObjectAbstraction)
- 第七章：言說程式碼 (https://gitlab.com/aesthetic-programming/book/-/tree/master/source.zh_TW/7-VocableCode)
- 第八章：酷詢資料 (https://gitlab.com/aesthetic-programming/book/-/tree/master/source.zh_TW/8-Que(e)ryData)
- 第九章：演算法過程 (https://gitlab.com/aesthetic-programming/book/-/tree/master/source.zh_TW/9-AlgorithmicProcedures)
- 第十章：機器反學習 (https://gitlab.com/aesthetic-programming/book/-/tree/master/source.zh_TW/10-MachineUnlearning)
- 後記：遞迴的想像 (https://gitlab.com/aesthetic-programming/book/-/tree/master/source.zh_TW/11-Afterword_RecurrentImaginaries)
- 參考書目 (https://gitlab.com/aesthetic-programming/book/-/blob/master/source.zh_TW/bibliography.md)
- 計畫列表 (https://gitlab.com/aesthetic-programming/book/-/blob/master/source.zh_TW/projects.md)
- 學生專案精選 (https://gitlab.com/aesthetic-programming/book/-/blob/master/source.zh_TW/showcase.md)
- 致謝 (https://gitlab.com/aesthetic-programming/book/-/blob/master/source.zh_TW/acknowledgments.md)


## 範例程式碼（位於 public/ 資料夾）：

* [RunMe](https://aesthetic-programming.gitlab.io/book/)
* [儲存庫](https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode.zh_TW)


## 所有流程圖（位於 graphviz/ 資料夾）:

原始碼: https://gitlab.com/aesthetic-programming/book/-/tree/master/graphviz


# 網頁以及出版品

網頁版本和出版品版本皆使用 Pelican 工具製作，並且各自有獨立的外觀主題。


## 安裝 Pelican 及相關套件

要生成這個網站，需要安裝 [pelican](https://getpelican.com/) 和其他幾個 Python 函式庫。

*以下步驟應在終端機、Shell 或命令提示符中執行。*

首先，確保您已安裝 Python 3。最簡單的方法是在終端機或命令提示符中輸入 `python3 --version`。它應該輸出一個版本號，如果沒有輸出版本號，這意味著您需要安裝它。您可以在[這裡下載 Python](https://www.python.org/downloads/)。

（最好可以在虛擬環境中安裝所需的套件。）

首先，進入 pelican 資料夾。（提示：輸入 `cd`，隨後必須有打上一個空格。然後將此儲存庫（repository）中的 pelican 資料夾拖曳到終端視窗中。然後按下 Enter 鍵。現在，您應該位於 pelican 資料夾內。您可以通過輸入 `pwd` 然後按下 Enter 鍵來驗證這一點。這應該會輸出目前的工作目錄，它應該以 `pelican` 結尾。）

要安裝所需的套件，執行以下命令：`pip install -r requirements.txt` 或是 `pip3 install -r requirements.txt`.

（如果需要安裝 Python3 環境下的 pip，終端的安裝指令是 `sudo apt-get install python3-pip`）


## 生成網頁

*以下步驟應在終端機、Shell 或命令提示字元中執行。假設您已經進入這個儲存庫中的 pelican 資料夾內。*


### 中文設定

想要產生中文的網頁，您需要先以編輯器編輯 pelican 資料夾下的 `pelicanconf_web.py`（您可以使用任何一種編輯器，nano、vim，或任何您熟悉的）。

找到 pelicanconf_web.py 檔案中的 `PAGE_LANG` 變數，並將其值修改為 `zh`：

```
PAGE_LANG = 'zh'
```

存檔後，接下來的操作過程中，pelican 會嘗試尋找資料夾中的中文語系資源。


### 開發階段


為了開發方便，建議使用 Pelican 的自動重載（autoreload）功能，讓 Pelican 在偵測到文件內容更改時能自動重新產生網頁。您可使用以下命令在本地端啟用一個開發用伺服器（以下命令參數中，`-l 或 --listen` 表示以 http 8000 埠啟用本地端伺服器，`-r 或 --autoreload` 用於自動重載，`-s 或 --settings` 則表示進行設定）：


```
pelican -lr -s pelicanconf_web.py
```

這個動作會在 pelican/ 目錄下產生 output.zh_TW/ 並且嘗試啟用一個 http 服務，並透過 8000 端口寄存你在本機生成的網頁（即 output.zh_TW/ 目錄下的內容）。

如果遭遇 "$HOME/.local/bin not in PATH" 的錯誤，您可以在終端中輸入以下指令：`source ~/.profile`，然後再次嘗試上面的指令。

如果一切順利，現在可以開啟瀏覽器，透過 <http://localhost:8000> 訪問網站。


### 正式發佈

產生用於發佈的網站的操作步驟稍有不同，因為 Pelican 假定該網站將在特定的網域（domain）上線並以此產生對應的內部 URL。此外，在開始建立之前，會清空 output 資料夾。您可以在 `publishconf.py` 檔案中設定 `SITEURL`（然後所有相關文件的路徑將根據您設定的網域生成絕對路徑）。

您需要先以編輯器編輯 pelican 資料夾下的 `publishconf.py`（您可以使用任何一種編輯器，nano、vim，或任何您熟悉的）。

找到 publishconf.py 檔案中的 `SITEURL` 變數，並將其值修改為您的網域名稱（例如：`SITEURL = 'https://aesthetic-programming.net/zh-tw`）

然後執行以下的指令：


```
pelican -s publishconf.py
```

生成的中文網站將被寫入名為 `output.zh_TW` 的資料夾中。接著可使用您偏好的方式將這些檔案上傳到真實的網頁伺服器中（例如：FTP、SFTP...等等）。


#### 持續（後續）的網站更新

*假設 pelican 已在本地主機上設定好，並具您有自己的網域路徑（定義於 `publishconf.py` 中的 `SITEURL`）*

更新步驟如下：

1. 使用 Git 拉取（pull）最新的儲存庫
2. 在 pelican 資料夾中執行開發用的指令（`pelican -lr -s pelicanconf_web.py`）並在本地 Web 伺服器上查看更改 
3. 透過發佈指令產生靜態網站（`pelican -s publishconf.py`）於 `output.zh_TW` 資料夾中
4. 上傳網站到 Web 伺服器（對於機器學習（machine learning）模型，Web 伺服器需要使用二進制傳輸模式（binary transfer mode）- theme/p5_SampleCode/ch10_MachineUnlearning/models/AP_book/）


## 列印出版品（書籍）

*以下步驟應在終端機、shell 或命令提示符中執行。假設您已經進入到此儲存庫中的 pelican 資料夾*

```
pelican -lr -s pelicanconf.py
```

前往 <http://localhost:8000/archives.html> 檢視分頁式出版物。

使用**瀏覽器的列印功能**產成 PDF。
