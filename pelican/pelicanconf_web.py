#!/usr/bin/env python
# -*- coding: utf-8 -*- #

# This file is only used if you use `make devserver-web` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

PAGE_LANG = 'en'
#PAGE_LANG = 'zh'

if PAGE_LANG == 'en':
	PATH               = 'content'
	STATIC_PATHS       = [ 'content' ]
	OUTPUT_PATH        = 'output/'
	TIMEZONE           = 'Europe/London'
	THEME_STATIC_PATHS = ['static']
	SKETCH_P5_PATH     = '/theme/p5_SampleCode/libraries/p5.js'

elif PAGE_LANG == 'zh':
	PATH               = 'content.zh_TW'
	STATIC_PATHS       = [ 'content.zh_TW' ]
	OUTPUT_PATH        = 'output.zh_TW/'
	TIMEZONE           = 'Asia/Taipei'
	THEME_STATIC_PATHS = ['static']
	SKETCH_P5_PATH     = '/theme/p5_SampleCode.zh_TW/libraries/p5.js'
	SLUGIFY_SOURCE     = 'basename'


SITENAME_FIX = 'Aesthetic Programming'
SITENAMES = {
	'en': 'Aesthetic Programming',
	'zh': '美學程式設計',
}

SITENAME = SITENAMES[PAGE_LANG]

THEME = 'theme/aesthetic-programming-web'
DISPLAY_PAGES_ON_MENU = True
DELETE_OUTPUT_DIRECTORY = True


MENUITEMS = {
    'en': [
		('Download sample code',       'https://gitlab.com/aesthetic-programming/book/-/archive/master/book-master.zip'),
		('Git repository',             'https://gitlab.com/aesthetic-programming/book'),
		('Buy a hardcopy of the book', 'http://www.openhumanitiespress.org/books/titles/aesthetic-programming/'),
		('Download as pdf',            'http://openhumanitiespress.org/books/download/Soon-Cox_2020_Aesthetic-Programming.pdf') ],
	'zh': [
		('下載範例程式碼', 'https://gitlab.com/aesthetic-programming/book/-/archive/master/book-master.zip'),
		('Git 儲存庫',     'https://gitlab.com/aesthetic-programming/book'),
		('購買紙本書籍',   'https://zimu-culture.com/'),
		('下載 pdf 格式',  'http://openhumanitiespress.org/books/download/Soon-Cox_2020_Aesthetic-Programming.pdf') ],
}

HOMEPAGE_TITLES = {
	'en': 'A Handbook of Software Studies',
	'zh': '軟體研究手冊'
}

HOMEPAGE_AUTHORS = {
	'en': 'Winnie Soon &amp; Geoff Cox',
	'zh': '孫詠怡 &amp; 傑夫・考克斯'
}

HOMEPAGE_INTROS = {
	'en': 'The book explores the technical as well as cultural imaginaries of programming from its insides. It follows the principle that the growing importance of software requires a new kind of cultural thinking — and curriculum — that can account for, and with which to better understand the politics and aesthetics of algorithmic procedures, data processing and abstraction. It takes a particular interest in power relations that are relatively under-acknowledged in technical subjects, concerning class and capitalism, gender and sexuality, as well as race and the legacies of colonialism. This is not only related to the politics of representation but also nonrepresentation: how power differentials are implicit in code in terms of binary logic, hierarchies, naming of the attributes, and how particular worldviews are reinforced and perpetuated through computation. Using p5.js, it introduces and demonstrates the reflexive practice of *aesthetic programming*, engaging with learning to program as a way to understand and question existing technological objects and paradigms, and to explore the potential for reprogramming wider eco-socio-technical systems. The book itself follows this approach, and is offered as a computational object open to modification and reversioning.',
	'zh': '這本書探討了程式設計由內所展現的技術和文化想像。它秉持著一個原則，即軟體的不斷崛起需要一種嶄新的文化思維和課程，以更好地理解演算程序、資料處理和抽象。書中特別關注在技術領域相對忽視的權力關係，牽涉到階級和資本主義、性別和性取向，以及種族和殖民主義的問題。這不僅牽扯到代表性的政治議題，同時也包括非代表性的層面：權力差異如何隱含在程式碼中，包括二進制邏輯、層次結構、屬性命名，以及特定世界觀如何透過計算被強化和延續。利用 p5.js，本書引入並展示了美學程式設計的反思實踐，將學習程式設計視為理解和質疑現有技術物件和規範的一種方式，同時探索重新編寫更廣泛的生態社會技術系統的潛力。本書本身也遵循這種方法，並呈現為一個可供修改和重新演繹的計算對象。'
}

HOMEPAGE_MENUITEMS = {
    'en': [ ( '中文（台灣版）',             'https://zh-tw.aesthetic-programming.net/' ),
			( 'Git repository',             'https://gitlab.com/aesthetic-programming/book' ),
			( 'Buy a hardcopy of the book', 'http://www.openhumanitiespress.org/books/titles/aesthetic-programming/' ),
			( 'Download as pdf',            'http://openhumanitiespress.org/books/download/Soon-Cox_2020_Aesthetic-Programming.pdf' ),
			( 'NEWS',                       'https://gitlab.com/aesthetic-programming/book/-/blob/master/News.md' ) ],
	'zh': [ ( 'English',       'https://aesthetic-programming.net/' ),
			( 'Git 儲存庫',    'https://gitlab.com/aesthetic-programming/book' ),
			( '購買紙本書籍',  'https://zimu-culture.com/' ),
			( '下載 pdf 格式', 'http://openhumanitiespress.org/books/download/Soon-Cox_2020_Aesthetic-Programming.pdf' ),
			( '最新消息',      'https://gitlab.com/aesthetic-programming/book/-/blob/master/News.md' ) ]
}
