//從來源進行小幅修改：
//https://learn.ml5js.org/#/reference/charrnn

let charRNN;
let textInput;
let lengthSlider;
let tempSlider;
let button;
let runningInference = false;

function setup() {
  noCanvas();
  //建立 LSTM 產生器並向之傳送模型指南
  charRNN = ml5.charRNN('models/AP_book/', modelReady);

  //拿取 DOM 元素
  textInput = select('#textInput');
  lengthSlider = select('#lenSlider');
  tempSlider = select('#tempSlider');
  button = select('#generate');

  //DOM 元素事件
  button.mousePressed(generate);
  lengthSlider.input(updateSliders);
  tempSlider.input(updateSliders);
}

//更新滑桿的值
function updateSliders() {
  select('#length').html(lengthSlider.value());
  select('#temperature').html(tempSlider.value());
}

function modelReady() {
  select('#status').html('模型已載入');
}

//產生新文字
function generate() {
  //若已啟動了另一個實例，則阻止推論的啟動
 if(!runningInference) {
    runningInference = true;

    //更新狀態日誌
    select('#status').html('正在產生...');

    //拿取原始文字
    let txt = textInput.value();
    //檢查是否有要傳送的內容
    if (txt.length > 0) {
      //這便是 LSTM 產生器需要的資料
      //種子文字、溫度、輸出長度
      let data = {
        seed: txt,
        temperature: tempSlider.value(),
        length: lengthSlider.value()
      };

      //利用 charRNN 產生文字
      charRNN.generate(data, gotData);

      //結束之時
      function gotData(err, result) {
        if (err) {
          console.log("error: " + err);
        }else{
          select('#status').html('準備就緒！');
          //刪除最後一個空格之後的字元
          result.sample = result.sample.slice(0, result.sample.lastIndexOf(" "));
          select('#result').html(txt + result.sample);
          runningInference = false;
        }
      }
    }
  }
}
