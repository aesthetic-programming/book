/*
Asterisk Painting by John P.Bell (http://www.johnpbell.com/asterisk-painting/)
Original code in Processing: https://tinyurl.com/AsteriskPainting
Port to p5js and modified by Winnie Soon with comment notes,
last update: 25 Aug 2020

changes log:
1. The color mode has been changed to a variable
as the push/pop function will restore the previous fill color state.
2. Remove font
3. change the bg color
4. add text size
5. remove load signature image
6. change the canvas size and corresponding no. of asterisk
7. display a counter on the bottom left corner and in black color
8. Add extensive comments
9. return a random no in integer
*/

let xDim = 1000;  //畫布尺寸-寬度
let yDim = 600;   //畫布尺寸-高度
let timer = 0;
let speed = 100;  //轉動速度，預設值為 100
let maxSentences = 77;  //初始：77
let sentences = 0;
let xPos = [1, 2, 3, 4, 5]; //初始：8 行
let yPos = [1, 2, 3, 4]; //初始：5 列
let xCtr = 0;
let yCtr = 0;
let waitTime = 10000;
let itr = 0; //迭代次數
let milliStart = 0;
let currentMillis;
let fillColor;

function setup(){
  createCanvas(xDim, yDim);
  background(240);
  /*將每個星號的 x 位置計算為
    以陣列索引值 [0] 開頭的陣列（xPos[]）*/
  for(let i = 0; i < xPos.length; i++) {
    xPos[i] = xPos[i] * (xDim / (xPos.length+1));
  }
  /*將每個星號的 y 位置計算為
    以陣列索引值 [0] 開頭的陣列（yPos[]）*/
  for(let i = 0; i < yPos.length; i++) {
    yPos[i] = yPos[i] * (yDim / (yPos.length+1));
  }
  fill(0);  //以陣列索引值 [0] 開頭的陣列（yPos[]）
  textAlign(LEFT, CENTER);
  text(itr, 10, yDim-30); //呈現計數器
  fillColor = color(
    floor(random(0, 255)),floor(random(0, 255)),floor(random(0, 255))
  );
}

function draw(){
  //millis 代表程式開始運行後所經過的毫秒數，就像 frameCount 一樣
  currentMillis = floor(millis() - milliStart);
  if(currentMillis > timer){
    push()
    translate(xPos[xCtr], yPos[yCtr]);  //列與行
    rotate(radians((360/8)* (millis()/speed)));  //自身旋轉
    timer = currentMillis + speed; //下一次迴圈的時間
    textSize(12);
    fill(fillColor);
    /*與以星號形式寫成的時間字串，
      而此字串永遠會從 0 開始。
      nf：將數字格式化為字串，並在前面加上零
      [https://p5js.org/reference/#/p5/nf]
      小數點前 3 位，以及小數點後 0 位。*/
    text(nf(currentMillis, 6), 3, 0);
    sentences++;
    if(sentences >= maxSentences){  //達到每個星號的最大值
      xCtr++;  //移動到下一個陣列
      //滿足最大行數，需要前往下一列
      if(xCtr >= xPos.length) {
        xCtr = 0;
        yCtr++;  //下一列
        /*程式達到螢幕上所能呈現列數的最大值
          （亦即達到最大列數後）；螢幕已滿
          > 將一切重置，並更新計數器 */
        if(yCtr >= yPos.length){
          yCtr = 0;
          background(240);
          //計數器+1（迭代）
          itr++;
          pop();
          //計數器顯示顏色
          fill(0);
          //再次改變計數器顯示方式
          text(itr, 10, yDim-30);
          //等待下此循環，會從第一個星號開始
          let wait = floor(millis() + waitTime);
          while(millis() < wait){}
          //重置開始時間
          milliStart = millis();
          //重置計時器
          timer = 0;
          push();
        }
      }
      sentences = 0;
      fillColor = color(
      floor(random(0,255)),floor(random(0,255)),floor(random(0,255)));
    }
    pop();  //回復之前的狀態
  }
}
