//跳動者
function setup() {
 //建立繪圖畫布
 createCanvas(windowWidth, windowHeight);
 frameRate (8);  //請試著改變這項參數
}

function draw() {
  background(70, 80);  //嘗試更改 alpha 值檢查此語法
  drawElements();
}

function drawElements() {
  let num = 9;
  push();
  //將物件移至中央
  translate(width/2, height/2);
  /*360/num >> 各個圓形轉動的角度;
    frameCount%num >> 得到餘數，以得知是在
    八個位置中的哪一個 */
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  fill(255, 255, 0);
  //參數 x 即是圓形與中央的距離
  ellipse(35, 0, 22, 22);
  pop();
  stroke(255, 255, 0, 18);
  //靜態線條
  line(60, 0, 60, height);
  line(width-60, 0, width-60, height);
  line(0, 60, width, 60);
  line(0, height-60, width, height-60);
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
