 /*建立一個類別：具有屬性和行為的
   物件模板／藍圖*/
class Tofu {
    constructor()
    { //將物件初始化
    this.speed = floor(random(3, 6));
    //查看此功能：https://p5js.org/reference/#/p5/createVector
    this.pos = new createVector(width+5, random(12, height/1.7));
    this.size = floor(random(15, 35));
    //以正數表示順時針旋轉
    this.tofu_rotate = random(0, PI/20);
    this.emoji_size = this.size/1.8;
    }
  move() {  //移動行為
    this.pos.x-=this.speed;  //i.e, this.pos.x = this.pos.x - this.speed;
  }
  show() { //將豆腐以方塊的外形呈現 
    push()
    translate(this.pos.x, this.pos.y);
    rotate(this.tofu_rotate);
    noStroke();
    fill(130, 120);//陰影
    rect(0, this.size, this.size, 1);
    fill(253); //前平面
    rect(0, 0, this.size, this.size);
    fill(150); //上方
    beginShape();
    vertex(0, 0);
    vertex(0-this.size/4, 0-this.size/4);
    vertex(0+this.size/1.5, 0-this.size/4);  //無特殊髮型
    vertex(0+this.size, 0);
    endShape(CLOSE);
    fill(220);//側邊
    beginShape();
    vertex(0, 0);
    vertex(0-this.size/4, 0-this.size/4);
    vertex(0-this.size/4, 0+this.size/1.5);
    vertex(0,0+this.size);
    endShape(CLOSE);
    fill(80); //面部
    textStyle(BOLD);
    textSize(this.emoji_size);
    text('*', 0+this.size/6, 0+this.size/1.5);
    text('-', 0+this.size/1.7, 0+this.size/1.9);
    text('。', 0+this.size/3, 0+this.size/1.2);
    pop();
 }
}
