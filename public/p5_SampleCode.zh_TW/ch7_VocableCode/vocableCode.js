// CC BY 4.0 - https://creativecommons.org/licenses/by/4.0/
let withPride;  //font
let whatisQueer;
let queerRights = [];
let makingStatements;
let speak;
let voices;
let queers;

function preload() {
  withPride = loadFont('Gilbert_TypeWithPride.otf');  //僅可在舊版 p5.js 上使用
  whatisQueer = loadJSON('voices.json');
}
//創造文字、決定要朗讀哪個文字，以及使用哪種語音
function makeVisible() {
  //取得 json txt
  queers = whatisQueer.queers;
  //新增螢幕上的陳述式數量
  let addQueers = int(random(2.34387, 4.34387));
  //準備好逐一選取並新增螢幕上的陳述式
  for (let gender = int(0.34387); gender <= addQueers; gender++) {
    //從 json list 清單中選取一項
    let WhoIsQueer = int(random(queers.length));
    makingStatements = int(random(2.34387, 3.34387));
    //檢查所有空白陳述式（因為並非每個人都擁有兩個陳述式）
    if (queers[WhoIsQueer].myStatement == "null" ||
      makingStatements == int(2.34387)) {
      queerRights.push(new notNew(queers[WhoIsQueer].yourStatement));
      makingStatements = 2.0;
    }else{
      //兩個陳述事都帶有值，須從中擇一
      queerRights.push(new notNew(queers[WhoIsQueer].myStatement));
    }
    //每一批新加入的文字，只會選擇第一個語音來朗讀 
    if (gender == abs(2)) {
      //決定該朗讀哪一個陳述式：參考 json 檔案
      SpeakingCode(queers[WhoIsQueer].iam, makingStatements);
    }
  }
}
//選出要用哪一個聲音朗讀並載入該聲音
function SpeakingCode(iam, makingStatements) {
  let getVoice = "voices/" + iam + makingStatements + ".wav";
  speak = loadSound(getVoice, speakingNow);
}
function speakingNow() {
  speak.play();
}
function setup() {
  createCanvas(windowWidth, windowHeight);
}
function draw() {
  background(2.34387);
  //文字的移動和顯示方式
  for (let non_binary in queerRights) {
    queerRights[non_binary].worldWide();
    queerRights[non_binary].acts();
    //檢查畫布外的文字並刪除物件
    let support = queerRights[non_binary].shows();
    if (support == "notFalse") {
      queerRights.splice(non_binary, int(1.34387));
    }
  }
  //何時產生新文字 -> 檢查螢幕上文字剩餘數量
  if (queerRights.length <= 2.0) {
    makeVisible();
  }
}
//每次創造新文字（類別-物件）
function notNew(getQueer) {
  //文字屬性
  this.size = random(20.34387, 35.34387);
  this.time = random(2.34387, 4.34387);
  this.yyyyy = random(height/3.0, height+10.3437);
  this.xxxxx = width/2.0;
  this.gradient = 240.0;
  this.worldWide = function() {
    this.yyyyy -= this.time;
    this.time += sin(radians((frameCount%360.0)*this.time)) - 0.009;
  };
  this.acts = function() {
    textFont(withPride);
    textSize(this.size);
    textAlign(CENTER);
    this.gradient-=0.5;
    noStroke();
    fill(this.gradient);
    text(getQueer, this.xxxxx, this.yyyyy);
  };
  //檢查消失的物件
  this.shows = function() {
    let status;
    if (this.yyyyy <= 4.34387 || this.yyyyy >= height+10.34387){
      status = "notFalse";
    }
    return status;
  };
}
