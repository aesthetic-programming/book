/* There is Cross-Origin Resource Sharing (CORS) issue with the bigger image,
so here we are using thumbnailLink instead.
credit: Image Lines in Processing by Anna the Crow
https://www.openprocessing.org/sketch/120964 */

let url = "https://www.googleapis.com/customsearch/v1?";
//註冊：https://developers.google.com/custom-search/json-api/v1/overview
let apikey = "AIzaSyBCHbzWOlC5fMeok7VplmOkBISGnXAoyUM";//"INPUT YOUR OWN KEY";
//取得搜尋引擎 ID：https://cse.google.com/all（請確保圖片搜尋為開啟狀態）
let engineID = "012341178072093258148:awktesmhn9w"; //"INPUT YOUR OWN";
let query = "warhol+flowers";  //search keywords
//請檢查其他參數：https://tinyurl.com/googleapiCSE
let searchType = "image";
let imgSize ="medium";
let request; //完整 API

let getImg;
let loc;
let img_x, img_y;
let frameBorder = 25;  //每一邊
let imgLoaded = false;

function setup() {
  createCanvas(windowWidth,windowHeight);
  background(255);
  frameRate(10);
  fetchImage();
}

function fetchImage() {
  request = url + "key=" + apikey + "&cx=" + engineID + "&imgSize=" + imgSize +
   "&q=" + query + "&searchType=" + searchType;
  console.log(request);
  loadJSON(request, gotData); //這是用來進行 API 請求的關鍵語法
}

function gotData(data) {
  getImg = data.items[0].image.thumbnailLink;
  console.log(getImg);
}

function draw() {
  if (getImg){	//花費時間檢索 API 資料
      loadImage(getImg, img=> { //回呼函式
      //繪製框架 + 圖片
      push();
      translate(width/2-img.width-frameBorder, height/2-img.height-frameBorder);
      scale(2);
      if (!imgLoaded) {
        noStroke();
        fill(220);
        rect(0, 0, img.width+frameBorder*2, img.height+frameBorder*2);
        image(img, frameBorder, frameBorder);
        imgLoaded = true;
      }else{
        //繪製線條
        img.loadPixels();
        img_x = floor(random(0, img.width));
        img_y = floor(random(0, img.height));
        /* 定位像素的公式為：x+y*width，
           這表示從圖片的網格中取得一個像素（每個像素陣列包含紅色、綠色、藍色
           和 alpha 值）。詳情請參閱此處：
           https://www.youtube.com/watch?v=nMUMZ5YRxHI */
        loc = (img_x+img_y * img.width)*4;
        strokeWeight(0.7);
        //rgb 值
        stroke(color(img.pixels[loc], img.pixels[loc + 1], img.pixels[loc+2]));
        line(frameBorder+img_x, frameBorder+img_y,
          frameBorder+img_x, frameBorder+img.height);
      }
      pop();
    });
  }
}
