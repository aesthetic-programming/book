Title: 第一章：入門
Slug: 1-getting-started
page_order: 1


![flowchart](ch1_0.svg)

[TOC]

## setup()

程式設計納入各級教育和跨學科學程已是相當普遍的現象，然而，在藝術和人文學科方面，許是因爲學生的職涯規劃和學習程式設計並無明確關聯，程式設計訓練仍然相當少見。這種現象引出許多問題，如課程規劃中包含或排除的項目有哪些、為何會有這樣的現象，以及哪些知識和技能在某些學科被視為必備基礎，但在其他學科則不然。這些選擇顯然強化了與階級、性別和種族相關，一些特定形式的特權。例如，廣泛而言，傳統上的「高雅文化」是受過大學教育（富裕、白人）者的範疇，而「低俗文化」則屬於未受過大學教育（工人階級）的普通人。程式設計既不屬於高雅文化，也並非低俗文化，作為一種獨門且專業的實踐[^1967]，跨越了階級鴻溝，而程式設計也聚焦於獲得在現實世界的工作和娛樂中都能夠應用的技能。然而，儘管應用範圍甚廣，要掌握程式設計層面的生產操作仍是個問題。

我們或許能在此讓傳統上適用於閱讀和寫作技能的讀寫能力（literacy，或譯「素養」），進一步涵蓋程式碼的閱讀和編寫。事實上，寫程式通常被稱作「當代的素養」，以及「我們必須學會掌握」的二十一世紀技能。[^STEM] 可以說，了解一些基本的程式設計技術不僅能提高未來的就業競爭力，還可以協助人更好地理解事物（程式碼）是如何被「編寫」和「解讀」的。[^Hall] 安妮特・維（Annette Vee）二〇一七年出版的《編碼素養》（Coding Literacy）一書進一步呼應文化研究，以及這個學門在擴展「讀寫能力」的概念，從而納入一般文化方面的基礎，嘗試將我們的關注焦點從技術技能，轉移到更廣泛的社會關係。如她所述，「從讀寫能力的歷史、社會和概念脈絡來看待程式設計，有助於我們了解做為一種重要的溝通現象，而不只是另一種新技能或科技的電腦程式設計。」[^Vee]

那麼，在讀寫能力框架之下的程式設計有什麼樣的涵義，又適用於什麼呢？儘管安妮特・維的書並非程式設計著作，也沒有提及該如何設計程式，卻讓寫作與程式設計之間的平行歷史交織在一起，比較並追溯了更廣義的讀寫能力意義究竟為何，以及該怎麼理解運算的興起，和程式碼與程式設計的重要性所衍生出來的文化話語。事實上，平行地討論書寫和編碼，以及文本和程式碼之間的關係已經逐漸普遍，在電子文學、數位人文和軟體研究領域尤其如此[^Cayley]（我們也將在第七章〈語音程式碼〉更進一步詳細發展這種平行），而這適用於所有人。我們期盼透過閱讀和使用本書，可以促進編碼素養的拓展，而本書也從安妮特・維關於編碼素養的論點中獲得靈感，不再只是「為了理解而閱讀」，更是「為了技術思想而閱讀，同時也透過複雜的結構和想法來書寫。」[^Vee2] 這不僅是新的讀寫方式，也是新的思考和理解其他編碼的途徑。此種讀寫能力相關論述很有說服力，不僅有助於讓個人獲得技能，更具有潛在、更為廣泛的文化和社會衍生影響，可協助推動程式設計脫離特定學科專業的侷限，打開其批判和美學潛力。

二〇一六年，詩人兼學者尼克・蒙特福特（Nick Montfort）出版了《藝術與人文探索性程式設計》（Exploratory Programming for Arts and Humanities），以實務經驗的角度談程式設計。附錄中，他概述了三個關鍵原因，以回應「為何進行設計程式？」[^Montfort]：學習程式設計讓我們能夠引入不同的方法和觀點來提出新的問題，從而以嶄新的方式思考；程式設計使我們得以更深入了解文化和媒體系統，進而學習如何更妥善地開發或分析文化系統；最後，藉由建立、設計和探索程式設計，我們將能協助改善社會。大體而言，我們同意蒙特福特的觀點，但同時也將此視為開闢不同工作方式的手段，並以程式設計做為思考的基礎，從而推敲程式設計實踐的另一種形式和政治想像。

本書第一章介紹了一些入門的想法和練習（可以稱為 `setup()`），並反思為什麼需要學習程式設計，我們期盼這能幫助讀者在後續章節中保持學習動力。此外，我們也認為讀者不一定都想成為專業程式設計師，故強調程式設計是一種以不同方式思考的手段（正如我們在前言中試圖概述的那樣）。這也適用於我們自己，在此過程中，我們亦從他人身上學習，挑戰自身的先入之見，特別是透過與程式設計經驗極少或缺乏的學生一起合作的經驗。學習編碼可以是愉快而有益的，但同時也很令人煩躁沮喪，在涉及複雜的語法和結構時尤為如此。熟悉精確、不容出錯的運算邏輯和程序需要時間，但我們希望，時至今日，學習程式設計的重要性已經確立。我們的選擇很簡單：「設計程式，或被程式設計」。[^Rushkoff]
{: style="    letter-spacing: -0.2px;   word-spacing: -0.2px;"}

## start()

![ch1_1](ch1_1.png){: .float}
:   *圖 1.1：p5.js 網頁介面*

在整本書中，我們將以 JavaScript 為主要的程式設計語言，並特別著重 p5.js 及其相關函式庫。實際上來說，p5.js 是一個以網頁為基礎的函式庫[^library]，利用開源的 JavaScript 框架，可以使用網頁上的程式碼建立專案，並且更容易透過網路分享，例如 p5.js Web Editor、Open Processing 和 Git 伺服器平台等，無須額外安裝。以 JavaScript 為基礎架構的專案可在瀏覽器中使用 URL 網址來運作執行。

JavaScript 係由布蘭登・艾克（Brendan Eich）於一九九五年開發，最初是為了能在 Netscape 瀏覽器中執行程式。[^Severance]有些人可能會將 JavaScript 和 Java 混為一談，但基本上它們是兩種不同的系統。JavaScript 是一種輕量級程式設計語言，通常用於視覺動畫和互動式網頁應用程式，開發之初是為了增進用戶端的介面體驗並作為 Java 功能的補充。實際上，任何「高級程式語言」（即更接近人類語言但更遠離機器語言）都需要翻譯成原生的機器指令/程式碼，以便電腦運作和執行。此翻譯過程通常是透過直譯器或編譯器來完成。JavaScript 是一種直譯語言，在現代瀏覽器上，通常使用直譯器和即時編譯器在運行期間翻譯原始碼[^Clark]，這讓啟動程式碼運行過程更為快速，但當應用程式趨於複雜且互動時間較長時，便會產生額外的運行期間系統開銷。[^Moon] 另一方面，於一九九六年由 Sun Microsystems 首次公開發布的 Java 則是一種經過編譯的複雜程式語言，這表示原始碼通常是在整合開發環境（IDE）中編寫的[^IDE]，需要優化並編譯成靜態位元組碼供 Java 虛擬機 (JVM) 進行處理。[^JVM] 許多電腦與手機程式都是由 Java 驅動的，從 Android 小型應用程式到 Minecraft 等遊戲皆是如此[^Minecraft]，而 JavaScript 主要適用於以網頁為基礎的小型應用程式，例如網站和機器人等。對於本書這種程式設計入門書而言，我們需要一些相對簡單但也能夠發展能力的東西，或稱「低地板、高天花板」[^Papert]，從這個角度來看，JavaScript 是一個很好的工具。

不過，除了從實用的角度引介這項工具，我們還有更多任務要做。本書將使用藝術家勞倫・麥卡錫（Lauren McCarthy）在二〇一四年建立，以 JavaScript 為基礎開發的 p5.js 函式庫，藉此以我們所謂的「美學程式設計」角度進行探索。為了更準確了解其譜系，我們可以看看卡西・瑞斯（Casey Reas）和班・弗萊（Ben Fry）於二〇〇一年開發了深具影響力的開放原始碼專案 Processing[^Processing]，這是一個以 Java 為基礎，在桌機上運作的環境，主要針對視覺藝術家和設計師的需求而設計。然而，麥卡錫觀察到，各種可取得的創造性開源軟體大多由白人男性開發，因此工作環境缺乏多樣性，而程式設計仍是一個極為男性主導的領域。[^laczko] 麥卡錫開始探索 Processing 在網頁上可能呈現的樣貌。重點是，p5.js 的核心思想不僅是將 Processing 部署為以網頁為基礎的運作平台，更是要以認真的態度處理軟體開發和交流的多樣性和包容性問題。正如麥卡錫所言，「思考社區外展和多樣性並不是 p5.js 的次要目標，而是構成整個平台的基礎。」[^McCarthy] 在短短幾年內，p5.js 的貢獻者便發展了一項社區宣言，將該介面翻譯成多種主流語言，如西班牙語和簡體中文[^Chinese]，做為 p5.js 計畫一部分的主頁系列中，也呈現了多位亞洲女性和非順性別程式設計師的訪談和作品[^Jin]，此外，還為視障者新增高對比模式和語音反饋系統[^UX]，並為聽障者開發了一系列名為「手語程式設計師」的創意表達研討會。[^Choi] 正如 p5.js 所示範的，軟體不僅僅是一種工具，也和人與政治息息相關。[^chun]

## 工作環境

您將會需要一個編輯器來編寫和記錄程式碼。我們將使用 [Atom](https://atom.io/)[^Atom]，一個免費開源的文字和原始碼編輯器，可跨不同平台編寫程式碼。之所以選擇可下載的程式碼編輯器而非網頁編輯器，是因為我們不僅僅將程式碼視為一個軟體，它還與電腦和作業系統的配置、各種瀏覽器以及資料文件的行為方式，以及資料夾路徑的組織等相關。

此外，我們使用 [Gitlab](https://about.gitlab.com/) 做為程式碼和文字儲存庫，至少在本書中是這樣。我們也將 Gitlab 用於教學目的，學生每週可以在此上傳他們的 ReadMe 和 RunMe 檔案，以接收同儕反饋並促進學習，並閱讀和分享程式碼和相關思考。我們發現這在獨立和集體工作方面都是很有效的方式，並根據自由和開源軟體開發的最佳原則共享材料，而學生則使用 Readme 來解釋技術方面並發展批判性討論。

### p5.js

1. 首先進入 p5.js[^p5js] 的[下載頁面](https://p5js.org/download/)，點擊並以壓縮的「p5.zip」格式儲存檔案，以取得執行程式碼所需的完整 p5.js 函式庫。

2. 雙擊以解壓縮檔案，從而提取其中所有的文件。這將自動創立一個名為「p5」的新文件夾。

3. 下一部分對正在進行的開發過程至關重要，因為您必須以某種方式確定工作資料夾的位置。如果您不知道，可以考慮使用「桌面」資料夾（「整理資料夾」意為在您的裝置上整理檔案，概念類似於在書架上整理文件、檔案夾和書籍等。日益精簡的使用者體驗（UX）設計，表示人們愈來愈不喜歡尋找和定位如照片等檔案的路徑及目錄，而是習慣將所有內容都放在手機的前幾頁或是桌面上）。 

4. 若將解壓縮後的資料夾「p5」放在自訂目錄中，您應該會看到資料夾中的檔案列表如下：兩個 p5.js 函式庫，一個綜合檔案（p5.js）和一個迷你版本（p5.min.js）。

5. 點擊資料夾「empty-example」，便會看到您需要啟動的檔案列表：

    index.html
    :   預設的超文本標記語言（HTML），它將首先被網頁瀏覽器選定。HTML 是一種定義網頁結構的基本技術，可以進行自訂，以包含文字、連結、圖像、多媒體形式、表格和其他元素。

    sketch.js
    :   編寫 JavaScript 的關鍵工作檔案。這裡的「素描」（sketch）一詞與其在視覺藝術中的意義類似，換句話說，它是一種不太正式的制定、捕捉想法以及嘗試構圖的方式。

    p5.js
    :   p5.js 核心函式庫。

    p5.sound.js
    :   p5.js 聲音函式庫[^sound]，用於網路音訊功能，包括播放、接收音訊輸入、音訊分析和合成等功能。 

<div class="columns" markdown=1>
![ch1_2](ch1_2.png){: .small}
:   *圖 1.2：p5 資料夾層級*

![ch1_3](ch1_3.png){: .small  style="width: 190px;"}
:   *圖 1.3：p5 資料夾層級*
</div>


### 程式碼編輯器 {: style="margin-top: -16px;"}

本書使用 Atom 做為程式碼編輯器。Atom 支援跨平台編輯，可以在 Mac OS、Windows 和 Linux 上運行。

1. 從主頁下載 Atom 軟體：<https://atom.io/>
2. 將剛剛解壓縮的「p5」資料夾拖入 Atom 中。您應可看到專案的左側窗格。試著找到「empty-example」資料夾下的「index.html」檔案，並雙擊該檔案，原始碼應會顯示在右側窗格中。見下圖：

    ![](ch1_4.png){: style="height:300px;"}
    :   *圖 1.4：Atom 的檔案結構*

    「index.html」通常會是網路瀏覽器顯示的預設頁面。您可以自訂頁面的標題和其他樣式，但本章的重點將是運用函式庫和執行您的第一個程式。由於 p5.js 是一個函式庫，第 8-10 行會指示如何使用標籤 `<script>` 和 `</script>` 來納入 JavaScript 檔案與函式庫。
    {: style="letter-spacing: -0.1px;  word-spacing: -0.4px;"}

    這份程式碼目前使用相對路徑，這是一個實用的概念，我們需要以此了解函式庫如何運作及定位檔案，以及未來要怎麼納入新的函式庫與檔案。JavaScript 函式庫只是簡單的檔案，我們必須將這些檔案合併到 HTML 中，以便讓程式匯入和讀取。這表示，當我們使用 p5 語法時，程式可以辨識該語法和相應的函式。對於此特定示例，請務必注意 JavaScript 函式庫和 HTML 檔案位於同一目錄中。若把函式庫移動到其他地方，我們會需要更新路徑。
    {: style="margin-bottom: 16px;"}


3. 接下來，您需要安裝一個名為「atom-live-server」[^liveserver] 的套件，此套件在網頁伺服器的設定方面相當實用，您將能更新程式碼並立即在瀏覽器中查看結果，而無需進行刷新。您可以先在 Atom 選單上的「Packages」查看此套件是否已存在。如果沒有，請到「Edit > Preferences > '+ Install'」，然後輸入「atom-live-server」。點擊藍色的安裝按鈕，您應可在 Packages 選單中再次看到這個套件。

    ![](ch1_5.png)
    :   *圖 1.5：安裝 atom-live-server*

4. 若想自訂窗格的背景顏色等主題，僅需到「Preferences > Themes」即可進行設定。


## 我的第一個程式

預設的 sketch.js 便是工作檔案，其中只包含兩個函式（JavaScript 函式是設計來執行任務的程式碼區塊）。

```javascript
function setup() {
  // 將設定程式碼放在此處
}
function draw() {
  // 將繪圖程式碼放在此處
}
```

`function setup()`
:   → 此函式中的程式碼只會被草圖工作檔案「運行一次」。這通常用於設定畫布大小，從而定義基本草圖設定。

`function draw()`
:   → 從視覺藝術的實體繪畫過程汲取靈感，此函式中的程式碼將不斷迴圈，表示每一次影格（frame）運行都會調用 `function draw()`。預設影格率為每秒 60 幀/次，這在事物處於運動狀態或不斷被捕捉時特別實用（我們將在第三章〈無限迴圈〉中繼續討論這一點）。
{: style="    letter-spacing: -0.1px;"}

上面範例程式碼的註解以「//」標示，代表這是為人類而非電腦寫成的文字，因此電腦在執行程式碼時會自動忽略這些註解。您可以將其視為對特定程式碼區塊如何運作的解釋文字。在整本書中，`//` 表示單行註解，而 `/*……………*/` 則表示多行程式碼註解，帶有起始符號「/*」和結束符號「*/」。

現在就來試試看將上述這些輸入草圖，繪製一個滲入些許灰色的黑色閃爍背景的畫布，然後再於左上角某處繪製一個橢圓型（請仔細檢查拼寫和標點符號，如大括號和分號，分別表示函式的範圍和結尾。程式碼的詳細資訊將在下文中進行解釋）。

<sketch
  src="p5_SampleCode.zh_TW/ch1_GettingStarted/sketch.js"
  lang="javascript"
  data-executable
  data-download-link="https://gitlab.com/aesthetic-programming/book/-/archive/master/Book-master.zip?path=public/p5_SampleCode.zh_TW/ch1_Gettingstarted"
/>

要運行程式碼，需至 Atom 選單上的「Packages > atom-live-server > Start Server」。視窗彈出後，再點擊「empty-example」資料夾，此資料夾應顯示如下內容：
{: style="    letter-spacing: -0.2px;   word-spacing: -0.5px;"}

![](ch1_6.png){: style="height: 6cm"}
:   *圖 1.6：我的第一個程式*  

<div class="section exercise" markdown=1>

## 課堂練習

本練習旨在讓您熟悉工作環境、路徑和原生目錄，並了解在網頁瀏覽器中運行草圖需載入 JavaScript 函式庫的確切路徑。您也可以隨意建立自己的資料夾名稱並重命名檔案 Sketch.js。此外，您也能試著透過更改數字和參數來了解運作原理，這點將在後文中更詳細地解釋。

![](ch1_7.png)
:   *圖 1.7：我的第一個程式 1.1*

1. **暫停伺服器**。從「Packages > atom-live-server > Stop」來停止 atom-live-server。
2. **重新命名資料夾**。試著將資料夾「empty-example」重新命名為「myFirstSketch」（為了方便電腦處理，請不要使用任何空格）。您將為後續章節和下面的練習建立自己的資料夾。
3. **架構 p5 函式庫**
    - 嘗試在「myFirstSketch」下建立一個名為「libraries」的資料夾。
    - 將兩個 p5 函式庫拖放到新建的資料夾「libraries」中。
    - 更改 index.html 中兩個 js 函式庫的相對路徑。
4. **HTML**。更改 HTML 檔案中的標題（第 6 行）。
5. **再次運行**。您能否再次運行該程式（「Atom > Packages > atom-live-server > Start Server」），藉此在網頁瀏覽器中看到與圖 1.7 幾乎相同的視窗？
</div>

### 在網頁控制台中讀取「Hello World」

您現在可能已經意識到，本書並沒有遵循大多數程式設計書籍的習慣，從「Hello World」程式開始，這種程式會在螢幕上顯示或打出「Hello World」的字樣。在 p5.js 中，`print()` 是列印的函式[^print]，但在網頁瀏覽器設定中，`print()` 函式會在「控制台區域」輸出。該區域並非設計給用戶端使用，而是供程式設計師或開發人員查看是否有任何錯誤訊息被記錄到控制台，並檢查程式碼是否以預期方式執行。

範例程式碼（見圖 1.6）的第四行打出了「hello world」兩個字。但若要查看文字，則須開啟網頁主控台區域，該區域的位置取決於您使用的瀏覽器。請試著打開選單來搜尋它。舉例而言，在 Firefox 中，它位於「Tools > Web Developer > Web Console」下（或按鍵盤快捷鍵：Linux/Windows 請按 `Ctrl + Shift + K`，Mac 則請按 `Option + Command + K`）。

![](ch1_8.png){: .medium}
:   *圖 1.8：網頁主控台區域*

在圖 1.8 的底部，網頁控制台區域顯示被突出標示的「hello world」字樣。這代表草圖運作正常，並且能夠讀取打印函式。隨著一步步深入本書，您會愈發了解網頁控制台區域的重要性，因為它還會顯示錯誤訊息，例如語法出現錯誤（我們將在第八章〈查詢（酷兒）資料〉中討論）等。在這種情況下，瀏覽器會提供一些為程式碼除錯的好建議。[^console] 圖 1.9 顯示網頁控制台區域能夠指出哪個文件（sketch.js）和哪行程式碼（第 8 行）有問題（背景的語法是故意拼錯的），甚至建議如何修正。

![](ch1_9.png)
:   *圖 1.9：語法錯誤示例*  

Hello World 程式在運算方面有著悠久歷史，通常用於向初學者介紹程式設計語言，並確保一切正常運行。`print("hello world")` 以「自然」語言編寫，讀者因此可以輕易從字面上理解其意義。同時，電腦亦準確地執行您叫它做的事，透過「指令」打印出文字，立即呈現成果，這可以讓人覺得努力有所回報。當程式設計師開始控制程式碼，並讓程式碼的表達在世界上具有意義時，這樣的即時反饋可以「產生一種力量感」。[^Chun2] 程式設計師學習用一種新的語言表達自己，就好像初次開口說話一般，因此才會採用這種看似天真的話語，向世界宣告自己的存在。喬弗・柯克斯（Geoff Cox）和鄧肯・辛葛頓（Duncan Shingleton）的「[*hallo welt! (hello world!)*](http://www.anti-thesis.net/hello-world-60/)」計畫[^Hello]，就利用這種溝通交流的行為，將不同程式設計語言編寫的一百多個 Hello World 程式以及一些人類語言進行迴圈，組合成一個即時、多語言、機器驅動的語言混亂（就如《巴別塔》一樣）。[^Babel]

## 閱讀參考指南

在進一步說明範例程式碼的其他部分之前，本書將透過教您閱讀參考指南，說明如何進行獨立學習，以便自行探索。在範例程式碼中，您可以在 `print()` 旁邊看到一些函式，包括 `createCanvas()`、`background()`、`random()`、`ellipse()` 等。

簡言之，上面的範例程式碼所做的是初始化確切的畫布大小（`createCanvas(640,480);`），將寬度設為 640 像素，高度設為 480 像素，以提供整體繪圖區域（請參閱範例程式碼的第 3 行）。這就是為何背景僅覆蓋畫布區域，而其餘區域則保持（預設）白色背景。草圖會在給定時間隨機選擇一種顏色（從灰色到黑色）做為背景，覆蓋整個畫布（`background(random(50));`）。最後一部分是在某個位置畫一個固定大小的橢圓型（`ellipse(55,55,55,55);`）。在 `function draw()` 中定位上述程式碼後，程式將不斷重複執行程式碼。背景顏色的變化閃爍，便明顯呈現出程式碼範例的重複執行狀態。

![](ch1_10.png)
:   *圖 1.10：參考指南範例 — ellipse()*

欲了解每個 p5.js 內建函式的參數，例如如像 `ellipse()` 這樣的函式中有多少個參數，我們可以到 p5.js 網站的「參考」頁面。[參考頁面](https://p5js.org/reference/)列出了大部分可供使用的 p5.js 內建函式，一旦習慣了它們的呈現方式，學習和編寫這些語法將變得更加容易和快速。

讓我們一起閱讀圖 1.10 中的參考資料 — [`ellipse()`](https://p5js.org/reference/#/p5/ellipse)。[^ellipse] 參考頁面通常以範例和插圖開始，您可以點按「編輯」按鈕修改程式碼並即時更改參數，成果會立即顯示在螢幕上。描述部分則解釋了函式語法的運作原理，這對不了解函式中每個參數涵義的初學者來說特別實用。語法區演示了如何精確編寫內建函式，例如該函式包含幾個參數，比如說，在 `ellipse(x, y, w, [h])` 的例子裡，語法區解釋了如何使用第一個參數 x 和第二個參數 y，從而透過 x 和 y 坐標設定橢圓的位置。畫布使用像素單位進行劃分，[0,0] 坐標起點為畫布的左上角[^webgl]。參數 w 和 h 代表橢圓的寬度和高度，您也可以將其視為用以定義橢圓的直徑或設定橢圓大小的參數。方括號裡的 [h] 是一個選擇性參數，如果橢圓的寬度和高度相同，則可忽略不填。

![](ch1_11.png){: .medium}
:   *圖 1.11：將橢圓形設定視覺化*

我們想在這裡表達的是，務必先從參考指南開始，然後再探索其他語法和功能，例如矩形和多邊形等形狀，這點十分重要。範例程式碼中還有一些我們沒有詳加解釋的語法，您可以在 p5.js 網站的參考資料中找到相應的資料，自己探索看看。不過，我們也將在下一章中繼續探索顏色的函式，而下下章則會聚焦 `random()` 函式。

## Git

我們使用 Git 來編寫本書和進行教學。Git 是 Linux 作業系統中 Linux Kernel 架構的創造者林納斯・托瓦茲（Linus Torvalds）於二〇〇五年開發的開源軟體管理系統，可用來追蹤任何檔案的更動，促進分散式網路中的變化版本控制。此系統在大規模協作的程式設計方面特別實用，在這種情況下，個人使用自己的機器，透過複製（分叉 forking）、拆分（分支 branching）和組合（合併 merging）來處理軟體的不同部分。Git 使用分散式模型，其中每位貢獻者都維護並擁有主儲存庫的副本。

GitLab 是一個開放原始碼、以網路為基礎的 Git 儲存庫平台，存放軟體開發人員貢獻的軟體庫和原始碼。GitLab 也是一個社群平台，人們可以在其上發表評論、關注其他軟體開發流程、將整個程式分叉到自己的儲存庫中等等。本書的所有內容，包括 readme 檔案、原始碼和函式庫，均根據創用 CC 授權（creative commons license）儲存在 GitLab 平台上，賦予其他人共享、使用和建構此作品的權利。我們想像，這只是本書的第一次迭代，期望之後能看到整本書經過多種重新挪用和分叉，以便人們使用現有框架進行修改，例如添加新的章節、範例和練習，以及更多相關的內容和參考資料，促進程式設計和思維之間的互動。

為求簡單明瞭，我們使用 GitLab 的網頁介面進行編寫和教學，並供學生繳交每週的 RunMe[^runme] 和 ReadMe[^readme] 作業檔案。我們也使用 GitLab 進行同儕反饋，以便學生閱讀和學習彼此的創作。

![gitlab](ch1_12.png)
:   *圖 1.12：用 GitLab 網頁介面建立新的專案*

1. 進入 [Gitlab.com](Gitlab.com)，於選單點擊「Register」註冊帳號。
2. 欲建立新專案：於左側選單點擊「Projects > Create New Project」並選擇「Create blank project」。
3. 輸入專案名稱和描述，如果您希望其他人無需任何身份驗證即可取用此專案，請點按「公開」（Public）（請見圖 1.12）。
4. 此時，若您想要在儲存庫中初始化一個 ReadMe 檔案，您可以勾選該項目的確認框。
5. 按下「Create project」按鈕後，您的儲存庫中將會自動建立一個資料夾。
6. 欲上傳檔案或建立目錄，只需點選儲存庫專案名稱下的「+」號（見圖 1.13）即可。GitLab 可讓使用者自訂提交更改說明（commit message，如此便可從一般性和溝通的角度來追蹤修改紀錄），因此，我們便可在點選「提交更改」（Commit changes）按鈕之前輸入提交更改說明。

![gitlab1](ch1_13.png){: .medium}
:   *圖 1.13：用 GitLab 網頁介面管理資料夾/檔案*

如需其他功能，GitLab Web IDE 編輯器內有一些可供使用的進階功能（位於右上角，請見圖 1.13 和 1.14）：如預覽 Markdown 文件、刪除或重新命名檔案/資料夾等。

![gitlab2](ch1_14.png){: .medium}
:    *圖 1.14：GitLab Web IDE*

## While()

上文已簡單介紹，Git 是一個分散式版本控制系統，用以在軟體開發過程中追蹤原始碼的變化，其設計旨在協調程式設計師之間的工作，但也可追蹤任何檔案集的更改，包括本書各章的工作流程。Git 是一個儲存庫，擁有完整的更動歷史和版本追蹤功能。此系統雖具齊全的工作流程管理功能，但關於「Git」這個名稱本身以及其是否為縮寫的各種猜測，在某種程度上削弱了這個系統的嚴肅性。據說，「Git」之名是這個 Linux 內容追蹤器的開發者托瓦茲命名的，他以「愚蠢的老 git」這句冒犯性的英國俚語來自嘲。同樣的，Git 也被暱稱為「愚蠢的內容追蹤器」，但與此相反，它其實是一個快速、可擴展且有效的分散式修訂控制系統。[^Git] 清楚起見，git 在用於指令時拼寫為「git」，用於產品時拼寫為「Git」，而不是用於罵人的「Git」，這個名稱並非縮寫，而是嘗試表達它所能做的事：一個不試著裝成過度聰明的樣子，但也並不愚蠢的工具。此外，製作多個版本的能力這樣明顯而公開的社會行為，可說是一種在公共領域一同思想、勞動（就像 p5.js 社群[^p5Community]）的信念，受到「GNU 通用公眾授權條款」（GNU General Public License）的法律保護，並確保使用者可以自由運用、學習、共享和修改他們的軟體。[^GNUGPL]

就像關於「Git」和「git」的爭論一樣，在程式設計和日常生活中，詞彙的使用、其意義和在世上所能達成的事變得至關重要。另一個與軟體去殖民化有關的例子，是程式設計術語「主」和「從」的使用（在一段依賴關係中，一個流程對另一個流程施加控制），羅恩・愛格拉許（Ron Eglash）稱之為「一個壞掉的隱喻」，網際網路工程任務組（The Internet Engineering Task Force，簡稱 IETF）則謂之「壓迫性的隱喻」。[^term] 在日常人際溝通中的另一個語言相關示例，是代名詞的政治，以及「她」、「他」或「他們」等詞的使用，如何在指稱人或性別化物件時，賦予了主詞特定的定位，而這是因為，語言往往是「男（人）造的」。[^Spender] 上述這點的重要性在於，因為語言具有影響力，故應以符合倫理的方式用語言來做事，正如朱迪思・巴特勒（Judith Butler）以及其他人所明確指出的一般；在《受激的言說》（Excitable Speech）中，巴特勒展示了言語可能會「傷害人」。[^Butler] 我們將在第七章中回來探討語言和程式設計之間的類比，目前而言，只需說明詞語能導致社會和政治性結果便已足夠，而這也可以延伸到運算物件和函式的命名[^Cox]，無論能否直接執行，它們都會造成某些效果。

本章的第一部分涉及了上述這種語言政治，經由閱讀、寫作和「程式設計」的能力（我們或許可以把它們稱之為「擴大的讀寫能力」），更深入理解在更廣泛的文化領域之下，詞語的意義和行為之間的關係。欲解釋我們為何需要新的閱讀和寫作類型，來說明包含存取權（access）等重大的文化和技術變遷，讀寫能力可謂至關重要。更清楚一點來說，我們可以回顧文化研究領域的開端，以及李察・霍加特（Richard Hoggart）的《讀寫何用》（The Uses of Literacy，一九五七年出版），這本書將工人階級（或大眾）文化納入以前只保留給菁英的所謂「文化」領域，從而擴展了讀寫能力（素養）的概念。[^Hoggart] 顯然，讀寫能力是一個不斷變化的概念，依據文化而有所不同，並以口語和寫作之間不斷改變的關係為基礎，沃爾特・J・翁恩（Walter J. Ong）也在《口述與書寫》（Orality and Literacy）探討過這點，他認為電子化時代透過社群媒體依賴各種方式寫作的「第二口述性」，使我們的理解力更加敏銳。[^Ong] 例如，程式設計的書面文字便展現了我們的語言如何以新的形式得到進一步加強，以及寫作如何成為一種行動的形式，而不僅僅是思維的指稱對象。

在這本書中，我們將人類語言和運算語言的詞彙及行動交織在一起，並認知到它們本身並非等價之物。本書使用的 JavaScript 語法就是一個對學習程式設計基礎和基本概念非常有用的實例，但也可以使用「輔助符號」進行實驗，此處意為調整正規符號使其更容易理解，並藉由模糊的語義為其他創造性表達開拓空間。舉例來說，我們可以想想看，「階層」（class）這個詞既可用以描述物件導向程式設計中的一個或多個物件，也可代表基於經濟和社會地位的社會分層。哈伍德（Harwood）的程式碼相關作品〈階層函式庫〉（Class Library）就是一個很好的例子，它融合了程式碼和書寫文字，強調使用程式碼的物質條件和階層式行動的可能性。[^Graham]您可以在參考資料部分查詢該術語以釐清技術原理。[^class]

將程式設計或編碼做為當代生活的必要技能這一論點似乎無庸置疑，許多實力，包括線上教學及 Codecademy.org 和 Code.org 等網站的發起，都與運算的素養和思維相關。正如本章開頭所介紹，安妮特・維的《編碼素養》一書也探討了這些連結，論證讀寫能力的概念如何強調了使用電腦編寫，以及為電腦編寫這兩件事的重要性、靈活性和力量。[^Vee3] 一個相當重要的角度是，這不僅有助於我們更清楚地理解程式設計與社會、科技和文化之間的動態關係，更拓展了我們對讀寫能力的概念，以及這項概念與排斥政治的關聯（就如其他非標準的讀寫能力/素養）。此外，程式設計與其他形式的寫作一樣，都演示了各式行動，因此，程式設計也是一種重新思考政治的方式：不只是寫作或說話、爭論或街頭抗爭，更在認識到現在的權力和控制常常是建立於基礎建設層級之上的同時，展現了修改其賴以執行行動的技術流程的能力。[^Kelty]

您的第一個程式是與這些想法產生連結的第一步，先從跑一些程式碼開始吧。由於我們並不希望這只是個單純的技術練習，當然實際上也不可能是這樣，因此，我們要請您在寫 ReadMe 和 RunMe 作業時，明確地把關鍵和實務的層面放進去。本書是這個初始任務以及後續任務的指南：請您試著運行一些程式碼，並透過它來思考。


* * *

<div class="section exercise" markdown=1>

## 迷你習作：ReadMe 和 RunMe

請製作 RunMe 和 ReadMe 檔案。

**目標**

* 學習基本設置，包括使用程式碼編輯器編寫程式碼、使用網頁瀏覽器運行程式碼、獨立學習程式碼語法，以及建立 ReadMe 檔案等。
* 從概念層面開始思考程式設計。

**更多靈感來源**

* 薩絲琪雅・弗利琪（Saskia Freeke）的 *Daily Sketch in Processing*：<https://twitter.com/sasj_nl>（另可於下列連結觀看她的演講 <https://youtube.com/watch?v=nBtGpEZ0-EQ>)；薩絲琪雅・弗利琪的 *All the Daily Things（2018）*：<https://vimeo.com/309138645>。
* 利沙克（Zach Lieberman）的 Instagram 頁面：<https://instagram.com/zach.lieberman>。
* "OpenProcessing 的基礎知識"：<https://openprocessing.org/browse/?q=basics&time=anytime&type=all#>。
* 「Creative Coding with Processing and p5.js」<https://fb.com/groups/creativecodingp5>。

**任務（RunMe）**

1. 從 p5.js 參考頁面 <https://p5js.org/reference> 學習至少一個語法範例（當然，能知道更多總是好的。請保持好奇！）
2. 熟悉參考資料的架構：範例、描述、各種語法和參數（這些知識將為您帶來自學新語法不可或缺的終身技能）。
3. 使用、閱讀、修改（甚至組合）範例程式碼（最基本程度的是更改參數數字），並產生出一個新的草圖，做為 RunMe 檔案。

**在您的 ReadMe 檔案中可供思考的問題**

* 您製作了什麼？
* 如何描述您第一次獨立撰寫程式的體驗（與思考、閱讀、複製、修改、編寫程式碼等方面的關係）？
* 編碼過程與一般文字的閱讀和寫作有何不同或相似之處？
* 程式碼和程式設計對您而言有什麼樣的意義？下列的閱讀資料又如何幫助您進一步反思這些概念？
</div>

## 指定讀物

* Lauren McCarthy, "Learning While Making p5.js," *OPENVIS* Conference (2015), <https://youtube.com/watch?v=1k3X4DLDHdc>.
* Annette Vee, "Coding for Everyone and the Legacy of Mass Literacy," in *Coding Literacy: How Computer Programming Is Changing Writing* (Cambridge, MA: MIT Press, 2017), 43-93.

## 延伸讀物

* Wendy Hui Kyong Chun, "On Software, or the Persistence of Visual Knowledge," *Grey Room* 18, January (2005): 26–51, <https://doi.org/10.1162/1526381043320741>.
* Brian Lennon, "JavaScript Affogato: Programming a Culture of Improvised Expertise," *Configurations* 26, no. 1, Winter (2018): 47-72.
* Nick Montfort, “Appendix A: Why Program?” Exploratory Programming For the Arts and Humanities (Cambridge, Mass.: MIT Press, 2016), 267-277.

## 註釋

[^1967]: 1960 年代，無論性別和教育背景為何，程式設計領域的就業機會都相當多，不僅進入門檻低，不需要程式設計經驗或大學教育文憑，公司通常也提供在職培訓。根據《柯夢波丹》雜誌 1967 年 4 月號路瓦・曼德爾（Lois Mandel）的報導〈電腦女孩〉（"Computer Girls”）所言，許多女性在電腦歷史早期便進入了程式設計領域，並爬上「運算階梯」。然而，內恩・恩斯曼格（Nathan Ensmenger）卻在他的論述〈程式設計的男性化〉（Making Programming Masculine）中表示，儘管電腦程式設計「剛開始的性別分野是模糊的，後來卻逐漸而刻意地轉為一門高階、科學而男性化的學科」。者篇文章收錄於湯瑪斯・J・米沙（Thomas J. Misa）等人編輯的《性別編碼：為什麼女性離開運算領域》（Gender Codes: Why Women are Leaving Computing）。（Hoboken, NJ: John Wiley, 2010），115-141。

[^STEM]: 擁有程式設計技能已成為全球教育和商業領域的必備條件。舉例來說，請參見 <https://ec.europa.eu/digital-single-market/en/coding-21st-century-skill> 和 <https://news.microsoft.com/apac/features/coding-way-brighter-future-2018-beyond/>。
程式設計在專業學科之外的開放，即所謂的「STEM」學科（科學、技術、工程與數學），為我們所謂的「美學程式設計」創造了條件。

[^Hall]: 我們在此想到的是斯圖亞特・霍爾（Stuart Hall）的文章〈編碼/解碼〉（Encoding/Decoding），他在文中表示，人們可以在解碼訊息中扮演積極的角色，該文收錄於斯圖亞特・霍爾等人編輯的《文化、媒體和語言》（Culture, Media, Language）（London: Hutchison, 1980），128-38。

[^Vee]: 安妮特・維（Annette Vee）《編碼素養：電腦程式設計如何改變寫作》（Coding Literacy: How computer programing is changing writing）（Cambridge, MA: MIT Press, 2017）第 4 頁。除了編碼的素養，我們還可以觀察主流媒體、政策制定和學術話語中其他類型的素養，例如程序、資料和數位讀寫能力等。請參見：Ian Bogost, "Procedural Literacy: Problem Solving with Programming, Systems, & Play," *The Journal of Media Literacy* 52, no. 1-2 (2015); Michael Mateas, "Procedural Literacy: Educating the New Media Practitioner," *On the Horizon. Special issue. Future of Games, Simulations and Interactive Media in Learning Contexts* 13, no.1 (2005); Annette N. Markham, “Taking Data Literacy to the Streets: Critical Pedagogy in the Public Sphere," *Qualitative Inquiry* (August 2019). doi:10.1177/1077800419859024; Teressa Umali, "Exclusive: Promoting Digital Literacy in the Philippine Education System," *OpenGov Asia*, available at  <https://www.opengovasia.com/promoting-digital-literacy-in-the-philippine-education-system/>。

[^Cayley]: John Cayley, "The Code is Not the Text Unless it is the Text," *Electronic Book Review* (2002)，請參見 <http://electronicbookreview.com/essay/the-code-is-not-the-text-unless-it-is-the-text/,>，另請參見 Katherine Hayles, *Writing Machines* (Cambridge, MA: MIT Press, 2002)。

[^Vee2]: Vee, *Coding Literacy: How computer programing is changing writing*, 45-58.

[^Montfort]: Nick Montfort, *Exploratory Programming for the Arts and Humanities* (Cambridge, Mass.: MIT Press, 2016).

[^Rushkoff]: 這句話取自道格拉斯・拉什科夫（Douglas Rushkoff）的《程式設計或被程式設計：數位時代十誡》（Program or Be Programmed: Ten Commandments for a Digital Age）(New York: OR books, 2010)。

[^library]: 函式庫是一組程式碼形式的資源整合體，包含各種函式及訊息。這些功能可用於開發軟體和應用程式。

[^Severance]: Charles Severance, "Javascript: Designing a Language in 10 Days," *IEEE Computer Society*, February (2012), 7-8.

[^Clark]: 任職於 Mozilla 的林・克拉克（Lin Clark）將程式碼變成漫畫。在此，她解釋了 JavaScript 如何在瀏覽器中運行，請參閱 <https://hacks.mozilla.org/2017/02/a-crash-course-in-just-in-time-jit-compilers/>。

[^Moon]: Seong-Won Lee and Soo-Mook Moon, "Selective Just-in-time Compilation for Client-side Mobile JavaScript Engine", in *Proceedings of the 14th International Conference on Compilers, Architectures and Synthesis for Embedded Systems (CASES '11)* (New York: ACM, 2011), 5-14. DOI: <https://doi.org/10.1145/2038698.2038703>

[^IDE]: IDE（整合式開發環境）是一種軟體應用程式，為軟體開發提供更全面和整合的環境，通常由原始碼編輯器、自動化建立工具和除錯程式組成。在本書中，我們使用 Atom 做為程式碼編輯器，但它需要另一個除錯工具，例如瀏覽器的網頁控制台。另一個 IDE 範例是 Processing，一個為視覺和電子藝術社群打造的開源獨立應用程式。請參閱 <https://en.wikipedia.org/wiki/Integrated_development_environment>。

[^JVM]: JVM （Java 虛擬機器）是指機器（通常是電腦）中的一種虛擬環境，可以運行和執行以 Java 等程式語言想成，採去 Java 位元碼形式的程式。JVM 執行載入、驗證、執行程式碼等操作，並提供運作環境。請參閱 <https://en.wikipedia.org/wiki/Java_virtual_machine>。

[^Minecraft]: 請參見 <https://minecraft.gamepedia.com/Development_resources>。

[^Papert]: 一個由數學家、電腦科學家和教育家西摩爾・派普特（Seymour Papert）首先提出的概念，派普特曾是麻省理工學院教授，並為一種名為 Logo 的程式設計語言建立了設計原則。參見 Seymour Papert, *Mindstorms: Children, Computers, and Powerful Ideas* (New York: Basic Books, 1980)。

[^Processing]: 參見 <https://processing.org>。

[^laczko]: 缺乏性別多樣性這點，也反映在 2017 年的美學程式設計課程反饋中，當時有位學生如此評論：「程式碼不是專屬於電腦科學家的紳士俱樂部」。藝術家茱莉・拉克茲蔻（Juli Laczko）的研究和實作探討了電腦史上的性別和刻板印象問題，她的反向類比式編織電腦作品〈webmachine〉便是其中一例。該作品運用打卡技術的線上軟體將二進制文本轉換為可視的程式碼。該專案研究了編織構造的歷史和生產實作，以及文化如何強化運算領域的刻板印象。請參閱 <https://digital-power.siggraph.org/piece/webmachine/>。

[^McCarthy]: Lauren McCarthy, "P5js Diversity & Floss Panel Introduction" (2015). 影片請見 <http://opentranscripts.org/transcript/p5js-diversity-floss-panel-introduction/>。

[^Chinese]: p5.js 現在有西班牙文版本，請參見 Maya Man, *Processing Foundation* (2016)，網址如下：<https://medium.com/processing-foundation/p5-js-is-now-available-in-spanish-3d1eab9dffa0>；另請參見 Kenneth Lim, Chinese Translation for p5.js and preparing a future of more translations (2018)，網址如下：<https://medium.com/processing-foundation/chinese-translation-for-p5-js-and-preparing-a-future-of-more-translations-b56843ea096e>。

[^Jin]: 此一主頁系列置於 p5.js 的子域之下，由 Chelly Jin 建立和策劃，專注於實踐程式碼+藝術多樣性，diversity.p5js.org。

[^UX]: 此為克萊兒・契爾尼・弗洛普（Claire Kearney-Volpe）的 UX（使用者體驗）研究專案，<https://www.clairekv.com/p5js-ux-research>。

[^Choi]: 此為藝術家兼教育家 Taeyoon Choi 的專案，<http://taeyoonchoi.com/soft-care/signing-coders/>。

[^chun]: 全喜卿將軟體的概念政治化，並且特別追溯自動程式設計的歷史、軟/硬體二元區分的興起，以及凝視的抹除。參見 Wendy Hui Kyong Chun, “On Software, or the Persistence of Visual Knowledge,” *Grey Room* 18 (January 2005): 26–51. <https://doi.org/10.1162/1526381043320741>。

[^Atom]: <https://atom.io/>.

[^p5js]: p5.js 下載頁面：<https://p5js.org/download/>。

[^sound]: <https://p5js.org/reference/#/libraries/p5.sound>.

[^liveserver]: <https://atom.io/packages/atom-live-server>.

[^print]: <https://p5js.org/reference/#/p5/print>.

[^console]: 為幫助程式設計師（尤其初學者），p5.js 內建「友善錯誤系統」。該函式庫的設計者認為設計良好、使用者友善、「以適當的語氣編寫」的除錯程式可為沒有經驗的使用者降低門檻。參見 A. Mira Chung, "Friendly Error System for p5.js", *Processing Foundation* (2017), <https://medium.com/processing-foundation/2017-marks-the-processing-foundations-sixth-year-participating-in-google-summer-of-code-d365f62fc463>。

[^Chun2]: 全喜卿和安德魯・利森（Andrew Lison）認為，我們所學到的第一個程式「Hello World」既有趣又迷人。我們將在下一章中更深入說明這一點。參見 Chun and Lison, "Fun is a Battlefield: Software between Enjoyment and Obsession," in Olga Goriunova, ed., *Fun and Software: Exploring Pleasure, Paradox and Pain in Computing* (New York, London: Bloomsbury, 2014), 180。

[^Hello]: *hallo welt! (hello world!)* 是由喬弗・柯克斯（Geoff Cox）和鄧肯・辛葛頓（Duncan Shingleton）共同合作的計畫，請參見 <http://www.anti-thesis.net/hello-world-60/>。

[^Babel]: 以通往天堂為目標的 *The Tower of Babel*（巴別塔）觸怒了上帝，因此「祂」決定打亂亞當時代的單一語言，使人們無法理解彼此（《創世紀》2:19 和 11:1-9）。往後，每個人都只能各自用不同的語言「胡言亂語」，這即是所謂的語言混亂。程式碼彰顯了這種混亂，但也喚起了言論自由，讓網路瀏覽器得以根據軟體所說/寫的內容「發聲」。正如柯克斯所述，「就腳本和表現兩個方面而言，它既是一種電腦可讀的邏輯符號，同時又再現了這個過程；在這層意義上，它就像被訴說的言語一樣」。參見 Geoff Cox, *Speaking Code: Coding as Aesthetic and Political Expression* (Cambridge, Mass: MIT Press, 2013), 3。

[^ellipse]: 請參閱 <https://p5js.org/reference/#/p5/ellipse>.

[^runme]: 欲使用網路瀏覽器在 GitLab 上運行 JavaScript 程式，需在上傳任何原始碼之前先於儲存庫中進行一些配置。專案的儲存庫根目錄中會新增一個檔案（.gitlab-ci.yml），其中包含在 GitLab 上運行所需的一組作業及其規範。您可以在 <https://gitlab.com/pages/plain-html/-/blob/master/README.md> 閱讀 GitLab 指南（請參閱 yml 檔案中的程式碼，以及如何使用儲存庫網站）。

[^readme]: Readme 檔案採用 Markdown 格式，文件副檔名為「.md」。Markdown 是一種輕量級標記語言，支援具有特殊語法的簡單文本格式。GitLab 可處理帶有此副檔名的檔案，並以可讀性更高的形式在網路上顯示。更多關於 Markdown 編寫的語法請見：<https://docs.gitlab.com/ee/user/markdown.html>。

[^Git]: 這段討論的大綱請見：<https://stackoverflow.com/questions/43959748/what-is-the-abbreviation-of-git>。

[^p5Community]: 參見 <https://github.com/processing/p5.js/wiki>。

[^GNUGPL]: 參見 <https://www.gnu.org/licenses/lgpl-3.0.txt>。

[^term]: 不久之後，諸如 Django 和 Python 等許多軟體社群便決定停用「主從」（master-slave）一詞，改以其他詞語替代。請參閱 <https://tools.ietf.org/id/draft-knodel-terminology-00.html#rfc.section.1.1>。根據羅恩・愛格拉許（Ron Eglash）的說法，主從一詞暗示了依賴關係中的控制要素，但這在技術上卻無精確描述，此外更進一步引出在運算中使用壞掉的隱喻的倫理問題。參見 Ron Eglash, "Broken Metaphor: The Master-Slave Analogy in Technical Literature," *Technology and Culture* 48, no.2 (2007): 360–69. <https://doi.org/10.1353/tech.2007.0066>。

[^Spender]: 關於這個問題的基礎讀物如下：Dale Spender, *Man-Made Language* (1980), <https://www.marxists.org/reference/subject/philosophy/works/ot/spender.htm>。

[^Butler]: Judith Butler, *Excitable Speech: A Politics of the Performative* (London: Routledge, 1997).

[^Cox]: 另見 Geoff Cox & Alex McLean, *Speaking Code: Coding as Aesthetic and Political Expression* (Cambridge, MA: MIT Press, 2013)。

[^Hoggart]: Richard Hoggart, *The Uses of Literacy: Aspects of Working Class Life* [1957] (London: Penguin, 2009).

[^Ong]: Walter J. Ong, *Orality and Literacy: The Technologizing of the Word* [1982] (London: Routledge, 2002).

[^Graham]: 參見哈伍德（Harwood）的作品〈階層函式庫〉（Class Library），收錄於富勒（Fuller）編輯的《軟體研究》（Software Studies）第 37-39 頁。

[^class]: 參見 <https://p5js.org/reference/#/p5/class>。

[^Vee3]: Vee, *Coding Literacy*.

[^Kelty]: 這一論點主要來自克爾第（C.M. Kelty）的著作《二位元》（Two Bits），這本書使用「運行程式碼」（running code）一詞來描述「由科技產生的引數」（argument-by-technology）和「由話語產生的論點」（argument-by-talk）之間的關係。參見：Christopher Kelty, *Two Bits: the Cultural Significance of Free Software* (Durham: Duke University Press, 2008), 58。顯然，程式設計師也能夠像一般人一樣，以其他修辭形式提出論點，請參閱：Kevin Brock, *Rhetorical Code Studies: Discovering Arguments in and around Code* (Ann Arbor, MN: University of Michigan Press, 2019)。

[^webeditor]: Processing Foundation announced the official release of the p5.js Web Editor in 2018, an online platform for learning and running code, and it is easy to get started with no additional installation of software. See <https://medium.com/processing-foundation/hello-p5-js-web-editor-b90b902b74cf>.

[^webgl]: 左上角的原點僅適用於 p5.js 中的 2D 環境。如果我們使用 WebGL（Web 圖形庫）實現 3D，例如，使用 `createCanvas(600, 600, WEBGL);`，則原點位於畫布的中心。

[^Reference]: 參見 <https://p5js.org/reference/>。
