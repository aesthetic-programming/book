Title: 第十章：機器反學習
Slug: 10-machine-unlearning
page_order: 10

![flowchart](ch10_0.svg)

[TOC]

## setup()

![ch10_1](ch10_1.png)
:   *圖 10.1：約瑟夫・維森鮑姆（Joseph Weizenbaum）的 ELIZA 關鍵字偵測基本流程圖（1996），圖片版權：Communications of the ACM*[^acm]

本章始於一張描述聊天機器人如何運作的流程圖－包含流程和邏輯。我們以這個與機器學習相關[^pathmind]的歷史範例為引子，進入了這本書的最後一個章節，廣義來說，機器學習是一種用於分析實驗或觀察資料的模型、統計方法和操作演算法的集合。有鑒於大量資料正在被產生和探勘、機器學習在日常語音控制裝置（如 Apple 的 Siri 或亞馬遜的 Echo[^diagram]）中的廣泛應用，以及在邊境控制人臉辨識軟體中更險惡的應用方式，機器學習會成為一門大生意倒也不足為奇。

機器學習是亞瑟・塞謬爾（Arthur Samuel）於 1959 年在 IBM 進行遊戲開發研究時創造的術語，最終目標是減少甚至完全消除對「細節的程式設計工作」的需求。[^samuel]關於電腦是如何開始為自己編寫程式，在針對人工智慧較早期的討論中便已現端倪。至於電腦是否能夠對輸入做出令人信服的回應這類猜測，則反映在相對簡單的聊天機器人示例中，這個機器人會利用關鍵字來產生「有足夠知識」的回應或提出後續問題。ELIZA 是最早期的聊天機器人之一，由麻省理工學院的約瑟夫・維森鮑姆於 1964 至 1966 年之間創造建。它模擬了羅傑式心理治療師與患者之間的對話，提示用戶進行輸入，然後以原始的「自然語言處理」[^nlp]，利用以關鍵字關聯性和語言模式的簡易對話腳本（見圖 10.1）來將這種對話輸入轉換為看似有意義且通常為反問形式的輸出。雖然表面上看起來很簡單，但這些回應卻非常有說服力（稍後您將對此有親身體驗）。

與 ELIZA 對話時會出現一種「幻覺」[^Weizenbaum]，讓機器看起來好像人類一般。我們會希望科技能夠擬人化，而 ELIZA 顯然利用了此一願望，此外更應用了所謂的智慧，它不僅能夠保持看似相關且個人化的對話，而維森鮑姆更指出，「有些受試者很難相信 ELIZA 不是人類」[^Weizenbaum2]。此處我們再次援引了圖靈測試。[^Turing]機器能否以類似於人類的輸出，對輸入做出令人信服的回應，或者更準確地說，機器是否能模仿理性思考？有趣的是，ELIZA 的名稱源自蕭伯納劇作《賣花女》（Pygmalion）[^Pygmalion]中的角色伊莉莎・杜立特（Eliza Doolittle），該劇以工人階級的賣花女為主角，一位語音學教授與朋友打賭，說他將屈尊俯就地教導賣花女紳士淑女的禮儀以及與工人階級考克尼方言相對的「得體的英語」，從而提升賣花女的社會階級（有趣的是，考克尼方言本身的如密碼般的編碼形式，就是一種對向上的社會流動的拒絕）。[^class]我們或許可以說，時下語音助理技術的運作原理與賣花女的故事類似，因為它們不僅可以處理內容，還能夠學習人類語言的風格。

在機器學習領域，我們通常認為語言風格乃是透過能夠處理和分析大量（自然語言）資料的技術，從訓練資料集中習得。因此，諸如「風格轉換」之類的機器學習技術，實依賴於歸納概括的過程，方能辨識各種語言模式。然而，這種「模式辨識」顯然並非中立的過程，它涉及輸入資料的辨識和資訊的「差別對待」[^clemens]。這種過程中顯然還有其他類型的差別對待，如語音助理[^assistants]或線上翻譯工具[^online]中反映出的固有刻板印象，其他例子還包括被視為種族主義者的 AI 聊天機器人 Tay[^tay]，或亞馬遜軟體和其他人工智慧系統的臉部辨識[^face]功能中的性別和種族偏見等。若從上述角度來理解，我們將可以知道，模式辨識創造了「主體與服從、知識、權威」，以及歸類和分類[^hito]，因此模式辨識不僅只關乎在技術操作層面讓任務流暢進行並做出準確預測，更關乎政治操作。

本書的最後一個章節恰如其分地將前幾章中所討論的諸多關鍵技術問題進行彙整，並放在機器學習研究的脈絡下探討。我們舉作為程式的 ELIZA 和作為人類的伊莉莎為例，將他們並列，以強調在機器學習方面，人類和機器是如何既訓練這些流程，同時也被這些流程所訓練。[^current]機器並不獨立於我們之外，而是更廣泛的社會技術聚合體的一部分，而學習程式設計將有助於釐清這點。[^suchman]就本書而言，若假設人必須先學習某些知識，才能將之實際教導給他人，而教導別人的過程也可以學習到一些事物，我們或許會在內心揣測，自己到底是在寫一本教學用書，還是一本學習之書。我們試圖讓「訓練」達到什麼程度？又是誰在訓練誰呢？我們將在本章結尾處回頭討論這點，但現在，我們顯然要先更深入地理解這些操作，且無論是機器還是人類層面，都應該對學習這件事更認真以待。

## start()

我們先利用諾伯特・蘭德施泰納（Norbert Landsteiner）於 2005 年製作的測試應用程式，來仔細檢視並反思 ELIZA 聊天機器人：

![10_2](ch10_5.png){: .medium}
:   *圖 10.2：《ELIZA Terminal》（2005）在網頁瀏覽器上的螢幕截圖，程式由諾伯特・蘭德施泰納編寫實施。照片由設計師提供*

任務：

1. 請點按「下一步」按鈕，以進入「ELIZA 測試」（2005）頁面，<https://www.masswerk.at/elizabot/eliza_test.html>，您可以在此看到 維森鮑姆在他發表的文章中提出的原始示例。[^Weizenbaum3]
2. 然後，請點入連結 <https://www.masswerk.at/elizabot/eliza.html>查看 《ELIZA Terminal》（2005）這項作品，並試著開始屬於自己的對話。[^bot]
3. 請分享您對維森鮑姆創造的原始對話的感想，以及您自己與聊天機器人對話的體驗：
    * 您會如何描述 ELIZA 帶來的體驗（例如語言的使用、談話風格和社交互動品質）？
    * 您會怎麼評估這類技術擷取和建構感受和體驗的能力？這樣的能力有哪些限制？

## 輸入和輸出之間

前文已簡要介紹了機器學習的概念，但這裡我們想澄清一下這個詞的實際含義。在當代語境中，機器學習指涉的是各種「資料處理」的技術[^samuel1]，或更準確地說，是統計和資料分析，通常可分為三個組成部分：輸入、建模（或學習）和輸出，並需要收集、解析和清理大量資料。[^shit]資料清理（data cleansing）這個術語在電腦或資料科學領域經常使用，乃是描述準備文字、影片、圖片和手勢等資料，並透過刪除或修改不相關、重複或格式不正確的資料，來調整資料不一致性，從而製造出輸入資料的過程。在資料準備的各種過程中，須就辨識和存取資料集、建構原始資料，以及處理資料不一致等方面進行決策。如果資料具多重來源，那麼就必須考慮要如何規範化資料，從而建構一個具備統一性的資料集了。[^papa]在第四章〈資料擷取〉中，我們已經明白這個過程充滿了關於應該包含和排除什麼、上述決策的依據和實施方式，以及該由誰決定和實作等問題。資料集不僅是達成目的的手段，更是我們有必要更深入理解、非常重要的文化物件。[^dataset]

<div class="section exercise" markdown=1>
## 課堂練習

在以下練習中[^michelle]，我們將利用實驗性人工智慧專案 *Teachable Machine*（第一版）[^tm1]，更密切地參與涉及輸入和輸出的機器學習過程，以了解兩者之間的關係：<https://teachablemachine.withgoogle.com/v1/>。

![ch10_2.png](ch10_2.png)
:   *圖 10.3：Teachable Machine（第一版）介面*

這個網頁版應用程式包括輸入、學習和輸出三部分。該程式將透過網路攝影機捕捉圖片作為輸入資料，並備有三種「訓練分類器」供您利用。
{: data-style-print="margin-bottom: calc(4 * var(--line-height))"}
* * *
**練習步驟：**

請準備三組可以被網路攝影機拍攝下來的手勢。每個手勢都必須透過長按彩色的「訓練」（TRAIN）按鈕來進行重複訓練，按下按鈕後，拍下的圖片會做為輸入資料（也稱為「訓練資料集」），從而生成機器學習模型（見圖 10.3）。我們用這個過程來訓練電腦辨識特定的手勢／圖片／姿勢，當有新的圖片輸入（所謂的「測試集」）時，學習／可教育的機器便能根據各種信心水準對這些手勢進行分類，然後預測相應的輸出結果。預設的輸出模式有 GIF、聲音和語音三種，可透過搜詢其他圖片、聲音和文字集來進一步修改。

**最簡易的入門方式如下：**

1. 請使用三種不同的手勢／臉部表情來訓練機器，隨後觀察以各種輸出來顯示的預測結果。
2. 請測試一下辨識或分類問題的界線，舉例來說，您可以使用不同的測試集，或在不同條件下（例如可調整的照明和距離）測試。可以被辨識的有哪些？無法辨識的又有哪些呢？
3. 若您僅使用少少幾張圖片會有什麼後果？這樣的訓練輸入量如何改變的機器的預測內容？

這個初步練習旨在讓您熟悉機器學習的三部分：輸入、學習和輸出，並探索資料與這三部分之間的關係。此外，這個練習也為思考機器從資料中學習、辨識資料模式、做出決策和預測的方式奠定了基礎。

## 學習演算法

機器學習利用各種統計演算法來處理（訓練）資料集。例如，它可以透過測量一系列源自訓練資料的已知圖片的色彩梯度，從而辨識人的圖片，這些色彩梯度將「教導」演算法辨識人類的構成元素。

廣義而言，演算法有三種類型：監督式學習（Supervised Learning）、非監督式學習（Unsupervised Learning），以及強化學習演算法（Reinforcement Learning）。

**監督式學習** - 這個模型是以一組訓練資料集為基礎，其中以兩兩相對的輸入／輸出為預期答案。垃圾郵件是這種類型的一個經典案例，在此演算法會從標記為「垃圾郵件」或「非垃圾郵件」的電子郵件樣本中學習，目標是將輸入資料與輸出標籤配對。舉個例子來說，以新電子郵件做為輸入，預期的輸出結果會是什麼？是否可以將之歸類為垃圾郵件，而後移至垃圾郵件信箱？這在數學中可以表示為 Y=f(X)，目標是從新輸入資料（X）中預測輸出的變數 Y。不過，這個預測過程須依賴分類技術，例如二元分類（是／否、垃圾郵件／非垃圾郵件、男性／女性等）和多分類（如圖片辨識中不同的物件標籤）等。分類技術奠基於資料標記的過程，但此過程就是不一致會發生的地方。資料以分立的方式分類，可能導致「標準且規範化」的預測的原因相當多，在涉及性別、種族和身份等超出二元分立型分類範疇的複雜主題時，這種規範化預測的問題尤為嚴重。

關於這個主題，藝術家兼研究員尼可拉・馬勒威（Nicolas Malevé）投注了大量心力於 ImageNet 資料集[^imagenet]的研究之上，ImageNet 由史丹佛大學的李飛飛教授於 2009 年開發，在電腦視覺領域極具影響力。該資料集非常龐大，其中包含的一千四百多萬張照片，則被整理成兩萬一千多個「同義詞集合」（synset，類別），「同義詞集合」一詞源自於名為 WordNet 的詞彙資料庫。[^net]標記工作係透過群眾外包平台 Amazon Mechanical Turk，由兩萬五千多位人士在兩年之內完成。2019 年夏天，馬勒威的腳本在倫敦攝影師藝廊（Photographers’ Gallery, London）的媒體牆展出，同時進行網路直播，展期兩個月，以每張圖片九十毫秒的速度循環播放資料集的全部內容，並隨機暫停使觀眾能夠「看到」某些圖片，以及圖片的分類方式（見圖 10.4）。這促使大家開始思索，訓練演算法所需的大量圖片與人類勞動之間的比例關係如何拿捏，並注意到對圖片進行註釋和分類所需的注意力（或這種注意力的缺乏）。[^TPG]網路上提供標題為《12 Hours of ImageNet》的工作現場錄影摘錄可供大眾觀看。

<div class="columns" markdown=1>
![cock](ch10_7.png){: style="height: 200px; "}
:   *圖 10.4：尼可拉・馬勒威在《Exhibiting ImageNet》（2019） 中對「公雞」的分類。圖片由藝術家本人提供*

![k-means](ch10_4.gif){: style="height: 200px; "}
:   *圖 10.5：使用 K-means 分群的非監督式學習的說明。圖片來自維基共享資源*
</div>


**非監督式學習** - 與前一個的學習模型不同，此處的訓練資料集並不包含一組標記資料。非監督式學習的一項常見任務是「分群」（分群，使用 K-means 和 Hierarchical Clustering 等演算法），其目標是發現相似之處，並利用探索性和分群分析，針對潛在模式和資料集之中不同小組之間的關係提供洞見。因此，同一組或分群中的項目會共享類似的屬性和指標（見圖 10.5）。分群背後的想法，是辨識資料集裡的相似資料組，並根據相似特徵將項目分門別類。商業和行銷部門經常會用這種方式來了解客戶偏好，根據客戶對某些類型商品的購買行為，對客戶進行分組，從而提供個人化購物體驗並實現資料行銷。

藝術家喬安娜・期考（Joana Chicau）和喬納森・羅伊斯（Jonathan Reus） 以非監督式學習模型為基礎，創造出研究計畫《Anatomies of Intelligence》[^AI]，以增進大家對解剖知識和運算學習的理解（見圖 10.6）。兩人在自己主辦的人工智慧（AI）研討會[^chicau]中，建議參與者針對內含約 15 張圖片的小型圖片資料集，試著提出兩個可供評估的特徵（例如「可愛度」和「捲曲度」），並根據這些特徵對每張圖片進行評分和排序（以 x 軸和 y 軸的形式），評分需有一定數字範圍，例如 0.0 到 1.0 之間（以統計術語而言，這樣便於標準化和重新縮放，使各種資料處於相同的比例）。您也可以加入更多特中，但在實體工作坊中，設定兩個特徵操作起來會比較方便。隨後，您便能透過一組特徵相關數值來描述每張圖片。幾個分群就此形成，為我們帶來新的視角，得以觀察圖片之間的異同關係（見圖 10.7）。這個練習很簡單，但顯然，我們可以透過決定分群的數量和計算資料點之間的分佈方式／距離等手段，將此練習進行延伸、系統化和自動化。這也有助於強化為辨識模式而設計，被稱為神經網路的演算法[^neuralnets]，這種演算法大致根據人腦模型的運行方式，以及它學習區分物件的能力都將得到改善。

**強化學習** — 這種類型的學習技巧乃是基於與環境的互動，將情況的分析與行動對應。[^rl]學習者（或代理）沒有任何先前的資料可供依循，故無法根據以往的資料來確認或預測該採取哪個行動，反之，學習者必須透過反複試驗來取得最佳結果。例如，電腦程式 AlphaGo[^alphago]在 2017 年擊敗了圍棋這種抽象策略棋盤遊戲的世界冠軍。AlphaGo 能夠透過自學過程對各種位置進行評估並選出最佳棋步。這種類型的學習會找出特定環境中所能採取的最佳行為或路徑，將狀態與動作配對，以實現最佳結果。就像在行為心理學中一樣，強化物的存在是用來促成未來的行動，就像孩子服從指示去做而獲得獎勵一般。與依賴於輸入訓練資料的監督式學習不同，強化學習的特點，是程式會從整體上理解環境[^environment]，並可以評估所採取的每項行動的有效性，從而自其經驗中學習，其中採用的「試錯法搜尋」和「延遲獎勵」[^rl2]乃是基於順序決策、運算、重複嘗試，以及對成功操作的回饋。

![anatomies](ch10_3.png)
:   *圖 10.6：喬安娜・期考和喬納森・羅伊斯的研究計畫《Anatomies of Intelligence》（2018-）。圖片由藝術家本人提供*


![anatomies2](ch10_4.png){: .medium}
:   *圖 10.7：喬安娜・期考和喬納森・羅伊斯在《Anatomies of Intelligence》的工作坊中展示基於「可愛度」和「捲曲度」的圖片分群*


## ml5.js 函式庫

有鑒於時空方面的限制，以及與本書迄今為止的內容保持一致的需求，本章將使用 ml5.js 機器學習函式庫進行實驗，這是一個像 p5.js 一樣可在網頁瀏覽器中運行的 JavaScript 框架。為了方便廣大受眾（尤其是程式設計初學者）接觸並了解機器學習，ml5.js 是建立在更為複雜的 TensorFlow JavaScript 函式庫之上。[^tensor]此外，ml5.js 站點包含大量程式碼示例，以及附帶預先訓練模型的教學課程，這些模型則是利用以往的訓練過程所創造。[^ml5]

承上文，我們可以將本書的最後一章〈後記：循環想像〉比喻為迄今為止你學到的內容的積極強化，為了完成後記，我們從 ml5.js 中挪用 `CharRNN_Text` 這個例子。我們的訓練模型並非由 ml5.js 所提供，以維吉尼亞・吳爾芙（Virginia Woolf）的文學作品訓練，而是以本書所有章節為基礎的預先訓練模型。[^training]本書的最後一個示例將透過這種方式從前幾章中學習，並根據其他章節的通用風格產生出新的文字。當然，這麽做的確會涉及簡化的過程，我們在前幾章於知識生產方面所提出的一些政治問題，將在此得到體現。[^issues]

訓練過程使用「循環神經網路」（RNN）和「長短期記憶」（LSTM），對順序資料進行逐字元分析和建模。文字的順序和上下文在產生對人類讀者有意義的句子方面都很關鍵（這與「自然語言處理」的領域有關），因此在逐字元訓練方面，上述兩者都相當實用。這種循環類型的神經網路可以擷取語料庫中的長期依賴關係，以便透過訓練過程的多次迭代來理解文字模式，並以標記式語言（此處為來自各個章節的字元和符號）為原始資料。我們最終得出的內容或多或少會是有意義的，這在章節的文字處理，以及原始碼、圖片連結、標題等方面都有所彰顯，但最重要的是，在接下來的額外章節中，機器產生的文字將讓我們看到，機器從這本書中所學到的內容，對比我們人類所學到的會有什麼樣的不同[^rnn]。在此我們回到本書的其中一項主要目標，亦即探索人類與機器在閱讀和寫作之間的一些異同：而我們將此稱為美學程式設計。

![ch10_7](ch10_6.png)
:   *圖 10.8：自動章節產生器*

[RunMe：sketch](https://aesthetic-programming.gitlab.io/book/p5_SampleCode.zh_TW/ch10_MachineUnlearning/)


## 原始碼

**JavaScript:**
<sketch
  data-executable
  lang="javascript"
  src="p5_SampleCode.zh_TW/ch10_MachineUnlearning/sketch.js"
  data-base-url="p5_SampleCode.zh_TW/ch10_MachineUnlearning/"
  data-download-link="https://gitlab.com/aesthetic-programming/book/-/archive/master/Book-master.zip?path=public/p5_SampleCode.zh_TW/ch10_MachineUnlearning"
  data-requirements="js/samplePatches/ch10HTMLGenerator.js, p5_SampleCode.zh_TW/libraries/ml5.min.js"
  />

**HTML:**

```javascript
<html>
<head>
  <meta charset="UTF-8">
  <title>Auto Chapter Generator</title>
  <script src="https://unpkg.com/ml5@0.4.3/dist/ml5.min.js"
  type="text/javascript"></script>
  <script language="javascript" type="text/javascript"
  src="../libraries/p5.js"></script>
  <style>
    body {background-color: white;font-family:"Lucida Console", Monaco,
    monospace;font-size:12;color:grey;}
    h1   {color: blue;}
    p    {color: black; font-size:14;}
  </style>
</head>

<body>
  <h1>Auto Chapter Generator</h1>
  <h2>This example uses a pre-trained model on the collection of all the
     chapters (in the form of markdown) of the book Aesthetic Programming:
      A Handbook of Software Studies</h2>
  <p>seed text:
    <input id="textInput" value="Afterword: Recurrent Imaginaries" size="30"/>
  </p>
  <p>length:
    <input id="lenSlider" type="range" min="100" max="2000" value="1000"/>
    <span id="length">1000</span></p>
  <p>temperature:
    <input id="tempSlider" type="range" min="0" max="1" step="0.01"/>
    <span id="temperature">0.5</span></p>
  <p id="status">Loading Model</p>
    <button id="generate">generate</button>
   <hr>
  <p id="result"></p>
  <script src="sketch.js"></script>
</body>
</html>
```

### 閱讀自動章節產生器

**index.html**

欲將 ml5.js 函式庫做為整體草稿碼的一部分來載入，您須在 index.html 中加入以下這行程式碼，就像使用 clmtrackr 函式庫匯入第四章〈資料擷取〉中所提及的其他函式庫一樣。針對此例，我們使用 ml5.js 函式庫（版本 0.4.3）。

```javascript
<script src="https://unpkg.com/ml5@0.4.3/dist/ml5.min.js"
type="text/javascript"></script>
```

除了新的 ml5.js 之外，HTML 檔案還包括以下 DOM 元素（見圖 10.8），可顯示相應的資料，並在那裡與用戶進行互動。sketch.js 主要被用來處理來自 DOM 和表單元素的資料，而並未用於畫布繪製（第 12 行的 `noCanvas()` 被使用於函式 `setup(){}` 中）。

1. **文字輸入框**用來輸入種子／輸入文字。在這個例子中，我們以「後記：循環想像」（Afterword: Recurrent Imaginaries）為序列輸入來產生下一個字元，並不斷形成新的種子序列進行下一個字元的預測：`<input id="textInput" value="Afterword: Recurrent Imaginaries" size="30"/>`
2. **用來決定字元生成數量的滑桿**，數值範圍為 100 到 2,000：`<input id="lenSlider" type="range" min="100" max="2000" value="1000"/>`
3. **用來設定溫度的滑桿**（控制預測不確定性的值）[^temp]，數值範圍為 0 到 1：`<input id="tempSlider" type="range" min="0" max="1" step="0.01"/>`
4. **顯示程式狀態的文字**，例如「正在載入模型」、「模型已載入」、「正在產生…」、「準備就緒！」等：`<p id="status">Loading Model</p>`
5. **可點擊的按鈕**，帶有「產生」（generate）字樣：`<button id="generate">generate</button>`
6. **結果顯示區域**，顯示生成式文字：`<p id="result"></p>`

**sketch.js**

程式碼將載入預先訓練模型，並根據收集的資料（種子文字、其長度和溫度值）產生文字。

```javascript
let charRNN;

function setup() {
  charRNN = ml5.charRNN('models/AP_book/', modelReady);
  …
}
```

第一個步驟是使用 ml5.js 函式庫中的 `charRNN` 方法（請參閱上文第 4 行程式碼），以路徑 `models/AP_book/` 將經過訓練的模型初始化並載入您的草稿碼中。當模型成功載入到草稿碼時，將執行回呼函式 `modelReady`，並將其狀態從「載入模型」更改為「模型已載入」。

```javascript
function setup() {
…
  // 選取 DOM 元素
  textInput = select('#textInput');
  lengthSlider = select('#lenSlider');
  tempSlider = select('#tempSlider');
  button = select('#generate');

  // DOM 元素事件 
  button.mousePressed(generate);
  lengthSlider.input(updateSliders);
  tempSlider.input(updateSliders);
…
}
```
程式以物件的形式收集資料（使用 `select` 語法搜詢 HTML 元素，特別是在 index.html 中已經定義的 `input id`）：種子文字（基於文字輸入）、預測性文字的長度（基於滑桿），以及溫度值（基於另一個滑桿）。

```javascript
function generate() {
…
  let data = {
    seed: txt,
    temperature: tempSlider.value(),
    length: lengthSlider.value()
  };
  charRNN.generate(data, gotData);
…
}
```

用來產生文字的種子文字、溫度和長度（字元數）是產生器所需的關鍵資料。這些資料物件會被傳遞給 charRNN：`charRNN.generate()`，以便透過預先訓練的模型，使用回呼函式 `gotdata()` 來處理種子文字。這種稱為 `.generate()` 的方法將把文字物件 `sample` 回傳為樣本輸出。理論上，此預測式文字將可粗略學習所有章節的風格，隨後產生相應的新文字。

```javascript
function gotData(err, result) {
…
  select('#result').html(txt + result.sample);
…
}
```

最後，結果將透過 `gotData()` 函式顯示在螢幕上。請留意，ml5.js 函式庫亦會使用引數 `err` 來檢查錯誤。

<div class="section exercise" markdown=1>

## 課堂練習

1. 請使用自動章節產生器（Auto Chapter Generator）程式，並試著根據不同的長度和溫度值產生文字。
2. 在代理性、不可預測性和產生性等方面，這種生成式文字範例也與第五章〈自動產生器〉有關，不過，我們在本章中習得的機器學習知識，會如何改變我們對上述這些術語的理解？在此脈絡之下的「學習」是指什麼？機器教會了我們什麼？在預測的產出過程中，機器學習所需要得又是什麼？[^predict]
</div>

## While()

本書各章探討的諸多問題，都聚焦於機器學習及其對批判性技術實踐的意義。在此我們特意再次引用阿格雷和他的文章〈邁向批判性技術實踐〉（“Toward a Critical Technical Practice”）[^agre]，以強調人工智慧等技術領域在社會和政治方面的重要性。他斷言，人工智慧是一種論述實踐，這是因為其中的技術性術語向我們展示了智力產生的方式，並在各個領域，以及其他不同的技術性和批判性活動與知識傳統之間作出了深刻的類比。這裡的問題的根源，有一部分是因為人們有將表徵與其所代表的事物混為一談的傾向。一方面，我們的技術傳統尋求精確，而另一方面，我們的批判性傳統又尋求意義模糊[^Hall]，然否認任何一種方法都毫無意義。舉例來說，在一次工程師關於深度學習的研討會上，魯哈・本傑明（Ruha Benjamin）便將沒有社會學深度的運算深度稱為「表面學習」[^superficial]。

在此「學習」這個詞就是一個非常切題的例子。塞謬爾率先將機器學習定義為「讓電腦無需明確的程式設計即可進行學習的研究領域」，將人類和機器學習之間做了類比，並將書籍加速人類學習與機器在西洋跳棋等遊戲中取得成功進行比較[^samuel2]。當然，把機器智慧與人類（尤其是兒童）認知發展相比較是很常見的。廣義來說，這種（建構主義式）思想，是將學習視為透過體驗世界並從中吸取知識的過程。然而，這種說法或許顯得有些膚淺，以下舉尼可拉・馬勒威對李飛飛的引用為例，這段話描述了李飛飛對「教導機器去看」這件事的一些深刻觀察，為 ImageNet 的發展提供了參考依據：

>「若把孩子的眼睛看作一對生物相機，他們每兩百毫秒將『拍攝』一張照片，這是眼球運動的平均時間。到三歲時，孩子將擁有數億張現實世界的照片，這代表著非常多可用於訓練的示例。因此我認為，焦點不應只放在越來越進步的演算法，而應專注於為演算法提供訓練資料，在數量和品質上，都向孩子們從經驗中所獲得的資料看齊。」[^pedagogy1]

上述示例展示了人類和機器視覺之間簡化的等價關係。但我們更感興趣的，則是訓練、教學和一般學習的隱含意義。我們每個人都有參與到教導機器看懂圖片的過程，而馬勒威更描述了我們使用智慧型手機和電腦等日常裝置時所提供的大量訓練。他想要研究的與其說是我們在這些過程中的共謀關係，不如說是具實用性的教學方法可能有哪些。從機器學習的動態中，我們可以學到什麼？用他的話來說，我們該如何「轉化它並被它轉化」？或者，以更接近李飛飛的話來表述，我們如何才能有效地思考這一代人類和演算法正在共同學習看懂圖片的事實？[^pedagogy2]這裡馬勒威打斷了這層思考，並問道，機器學習與和批判教育學（radical pedagogy，又稱激進教育學）在何種程度上能夠相互學習，並超越物體之中壓迫性的主客體關係，以便學習者可以更積極地參與到他們自己的學習中。[^radical]我們必須學習如何學習。

如果視覺素養（visual literacy）這種教育任務不再僅屬於人類，機器也必須參與，那麼它便在廣義上成為了一種人機素養相關議題。在許多方面，我們仍然認為，約翰・伯格（John Berger）的著作《觀看的方式》（Ways of Seeing）會是很實用的參考。當然，從伯格寫下「所見與所知之間的關係永遠不會穩定下來」[^Berger]的時代迄今，很多事情已然改變，但根據我們對機器學習的了解，我們確實可能會想問，這種關係是如何進一步變得更不穩定的。[^cox]我們口中所說的機器「看到」或「學習」，其實是對運算實踐的簡化說法，而這些運算實踐僅會透過各種演算法和模型來逼近可能的結果。伯格有一段針對電視媒體的反思，曾透過電視媒體本身傳播，而知識組成元素的排列方式，可以進一步喚起人們對這項反思的記憶，伯格對電視媒體的省思如下：

>「但請記得，我正為了自己的目的來控制和使用這些程式所需的複製手段 […] 在這個程式中是如此，在其他所有程式中亦然，您將會收到排列好的圖片和含義。我希望你能將我的排列納入考量，但同時也請對這種排列方式保持懷疑態度。」[^Berger2]

我們想在這裡重申這一點，並鼓勵讀者深入地反思諸如這本書等事物的生產方式，也許，與教學和學習相關的書籍更值得質疑。學到的知識不應該脫離其傳播的方式，也不應該脫離人到機器或機器到人的知識傳播方向。更直指重點地說，我們討論的核心就是意義的產生，並同時關注正在學習的內容，以及我們的學習效果在多大程度上被簡化的世界運作觀念所減損或影響。 對此，麥肯齊曾問道：「我們對機器學習有點雜亂的概括 […] 是否證明知識、決策和控制已被重新定義，產生一種『系統遭到轉換』的全新操作形式？」[^Learners]在這些條件下，人類與機器學習之間的關係變得模糊。學習的總體理念，隱含了以新的形式來控制已知的事物和方式。在這裡，麥肯齊以傅柯的理論為基礎，將機器學習解釋為知識生產的形式和權力策略。他試圖了解機器學習者如何透過他們之間的差異，產生出不同種類的知識，例如說，不同機器學習者分類和歸類資料的方式可能有所不同（圖片中的這個人屬於某性別、種族、可能的恐怖分子等等）。在這種情況下，知識的地位通常會被定調為「最小公分母」，並由從知識中獲利的公司規模龐大的基礎設施所支持，亞馬遜和 Google 等公司的情況正是如此，這類平台媒體帝國在這項技術上進行鉅額投資，讓用戶可以成為資料提供者，並學習成為更好的消費者。總之，考慮到機器學習技術產生的廣泛脈絡，人們對機器學習產生的知識形式心懷深切擔憂。從這方面來說，所謂的聰明，也就只是膚淺的學習罷了。

上述這些想法，是進一步的工作和反思的起點。[^refs]虛實之間的相互作用是其中的一部分，例如「深度偽造」（deepfakes，由英文的「深度學習」和「偽造」兩字組合而成）技術便是一個很好的額外案例研究，可供觀察合成的實例如何傳遞真實資料。對此過程的簡要描述，以及「生成式對抗網路」（GAN）的運作，或許可在這裡帶來有用的補充。[^GAN]GAN 中包含兩個神經網路，一個是產生新資料實例的「產生器」，另一個則是區分產生器創造的虛假資料與真實資料的「辨識器」，兩者會藉由越來越逼真的假資料相互挑戰，並據此優化他們的產生與辨識策略，直到產生出來的資料與真實資料變得無法區分為止。這也是一種（非監督式）訓練方法，機器會根據自己的分析來產生分類，因此無須仰賴人類對輸入圖片的標記。這與辯證唯物主義相呼應，因為這種唯物主義認為，世上一切皆是透過在矛盾中轉化的過程，從而化為技術性現實，批判性理論能否從 GAN 的例子中學到一些什麼呢？GAN 使用的方法有可能為運算邏輯和政治的混同另闢蹊徑嗎？[^transmediale]

開始以這種方式思考運算操作時，我們透過概括化、預測和產生未來可能性的過程，廣泛地繼承了迄今所學到的機器學習知識，以作為概念模型或圖表來參考。我們會在這章中指出未來將要著手進行的關鍵工作，並反思作為一種手段，可從資料中學習內容的機器學習，而這些資料與我們在程式設計實踐過程中的學習經驗是齊步並行的。正如阿格雷所言：「至少在可預見的未來，批判性的技術實踐將需要一個分裂的身份認同，我們必須將一隻腳扎根於設計的工藝性工作，另一隻腳則以反思性的批判工作為基礎。」[^agre2]隨後的挑戰，則是在工作時橫跨兩種模式並從中學習，不是以一種分裂的身份，而是以一種酷兒（queer）的認同，在多元環境下開闢流暢工作的方式。在這方面，我們認為批判性技術實踐是一種酷兒的實踐，而我們也希望，本書已經清楚地說明了這一點。美學程式設計透過這種方式，展示了一些可能的方法，藉此進一步打破理論與實踐、思維與行為、藝術與技術、人類與機器等多種二元分裂情況。

上述這些論點都值得花更長的時間討論，但在本書即將結束的最後幾段中，我們並沒有空間進行。不過，這種限制同時也使我們能夠劍指本書之外的領域，也許還能發展為另一本尚未開始邊寫的書，如果工作流程能夠自動化，那麼我們做為作家、編輯、設計師、程式設計師和教師的工作也將自動化。我們對這種模式潛在的擔憂，是我們的決策、思考和創造力將被自動化，而決定未來的能力又將受到預測式演算法的影響。[^tang]我們希望，透過這本書，可以針對大眾演算法力量的質疑，在此處提供一些見解，以在一定程度上控制這些過程，並導向另類的結果和「循環想像」（下一章的副標題）。

麥肯齊聚焦的領域，是透過特定的演算法和資料實踐，來了解人機關係及其轉換的特殊之處，尤其強調不確定性和偶然性在這些過程中扮演的角色，所以我們可以說，這種對未來可能性的見解，也是麥肯齊研究的特殊價值所在。換言之，機器學習的過程絕不是決定論式的（正如我們在本章的練習中所看到的），而是不斷地進行修訂和修改，就其本質而言，機器學習更是由過程所驅動的。橫跨多處開源平台和利益共同體，機器學習也在不同程度上被應用於多種學科和領域的實踐，並在此過程中不斷地自我改造及被改造[^Learners3]，展現出一個程式不僅僅是關乎原始碼，更是關乎一整套可讓多種過程以多種方式改變的遞迴式操作。[^Learners4]至此，我們的問題轉為：這種不同的機器學習編程模式，會在多大程度上導致不同的知識生產模式，並改變人機關係。就本書整體而言，這種模式又會導出哪些另類知識和美學實踐？

<div class="section exercise" markdown=1>

## 迷你習作：期末專案

美學程式設計是一項批判性的技術實踐，探索閱讀、寫作和建構，以及思考和理解支撐我們經驗和現實的複雜運算過程。為了處理這些實踐的交集之處，我們介紹了程式設計的基本概念，作為更深層反思的起點，其中考慮了技術詞彙的精確性和模糊性，以及特定的運算實踐，從而奠定基礎，以便進一步理解文化現象的建構和運作。

您的任務是運用包括各種理論和概念文字的課程內容，與同學分組將所選的運算產出物概念化，並試著設計、實施並闡明之。到了現在，我們期望這個練習應已能證明您有能力整合實際的程式設計和概念性技能，以表達並開發一種可以探索軟體的美學和政治的批判性技術產出物，這點自不待言。

此處提供一些提示，協助您發想的專案：

* 您可以再次查看本書的主題，協助啟發靈感，主題包括：素養／入門、變數幾何、無限迴圈、資料擷取、自動產生器、物件抽象、言說程式碼、酷詢資料、演算法過程、機器學習，以及寫作和編程、臉部辨識、表情符號、（微）時間性、全部擷取／資料化、互動性、基於規則的系統、物件導向、語言和語音、表現力、演算法文學、資料處理和學習的政治等，而上述這些主題都仰賴大眾對於種族、階級和性別政治的關注。
* 請再看一次之前所有的迷你習作和提問，其中是否有您想更深入探索的呢？
* 有哪些指定或建議讀物為您帶來了靈感，讓您想要進一步探索？
* 是否有任何特定技術領域，激起您想深入研究的欲望？

**RunMe：**

請產出使用 p5.js（或 HTML/CSS/JS/p5/ml5/node.js 的組合）編寫的軟體作品。

請記得在您的作業中寫下所有您使用的外部函式庫和資料／資源，例如圖片、字型、文字檔案、聲音檔案等。此外，如果您借用了其他範例程式碼或想法，也請在程式碼註解中註明資料來源。

**ReadMe：**

請撰寫一份 6-8 頁的文件（每頁最多不可超過 2,400 個字元，包括空格），並於其中列出學術來源（字元數量可排除圖片、參考文獻和註釋）。

該文件應包括標題、螢幕截圖、流程圖、參考文獻、期末專案 RunMe 的連結、相關專案的連結（若有），以及之前所有迷你習作的連結（列於附錄）。

ReadMe 應透過您的原始碼、程式設計過程和所選擇的閱讀材料處理以下問題：

* 您的軟體與什麼有關（請簡要描述這個軟體是什麼，如何運作，以及它打算探索的對象）？
* 您的專案是如何處理至少一個主題，並探索程式碼在技術和文化方面的交集的？
* 開放性問題：作品本身在多大程度上可被視為一項批判性工作？
</div>

## 指定讀物

* Ruha Benjamin, "Are Robots Racist: Reimagining the Default Settings of Technology and Society," lecture (2019), <https://www.dropbox.com/s/j80s8kjm63erf70/Ruha%20Benjamin%20Guest%20Lecture.mp4>.
* Geoff Cox, "Ways of Machine Seeing," *Unthinking Photography* (2016), <https://unthinking.photography/articles/ways-of-machine-seeing>.
* Yuval Noah Harari, Audrey Tang, and Puja Ohlhaver, "To Be or Not to Be Hacked? The Future of Democracy, Work, and Identity," *RADICALxChange* (2020), <https://www.youtube.com/watch?v=tRVEY95cI0o>.

## 延伸讀物

* Kate Crawford and Vladan Joler, "Anatomy of an AI System: The Amazon Echo as an Anatomical Map of Human Labor, Data and Planetary Resources," AI Institute (2018), <https://anatomyof.ai/>.
* Shakir Mohamed, Marie-Therese Png, William Isaac, “Decolonial AI: Decolonial Theory as Sociotechnical Foresight in Artificial Intelligence,” *Philosophy & Technology*, Springer, July 12 (2020), <https://doi.org/10.1007/s13347-020-00405-8>.
* Adrian Mackenzie and Anna Munster, “Platform Seeing: Image Ensembles and Their Invisualities,” *Theory, Culture & Society* 26, no. 5 (2019): 3-22.
* Daniel Shiffman, "Beginners Guide to Machine Learning in JavaScript," *The Coding Train*, <https://www.youtube.com/playlist?list=PLRqwX-V7Uu6YPSwT06y_AEYTqIwbeam3y>

## 註釋

[^pathmind]: 需要點明的是，雖說機器學習是人工智慧的一部分，但人工智慧是一個更廣泛的概念。人工智慧、機器學習和深度學習這幾個術語經常被互換使用，但三者之間有一些關鍵的區別，解釋如下：「您可以將深度學習、機器學習和人工智慧視為一個套著一個的俄羅斯娃娃組，從尺寸最小的娃娃開始，然後漸次發展。深度學習是機器學習的一個子集，而機器學習又是人工智慧的一個子集，人工智慧是所有電腦程式的總稱。換句話說，所有的機器學習都是人工智慧，但並非所有人工智慧都是機器學習，以此類推。」參見帕斯邁德（Pathmind）的著作："A.I. Wiki: A Beginner’s Guide to Important Topics in AI, Machine Learning, and Deep Learning," <https://pathmind.com/wiki/ai-vs-machine-learning-vs-deep-learning>。

[^diagram]: 詳細說明請參閱凱特・克勞佛（Kate Crawford）和維拉丹・約勒（Vladan Joler）的文章和圖表："Anatomy of an AI System: The Amazon Echo as an anatomical map of human labor, data and planetary resources," (2018)，<https://anatomyof.ai/>。

[^samuel]: 機器學習是亞瑟・塞謬爾於 1959 年在 IBM 進行遊戲開發研究期間創造的一個術語，最終目的是透過概括化學習來實現模式辨識，從而減少甚至消除對「詳細程式設計工作」的需求。參見：Arthur L. Samuel, "Some Studies in Machine Learning Using the Game of Checkers," *IBM Journal of research and development* 3, no.3 (1959): 210-229。

[^nlp]: 自然語言處理研究電腦如何理解人類語言的含義，處理電腦與人類透過該自然語言所進行的互動。這涉及人工智慧、電腦科學和語言學領域，並包括文字轉語音、語音助理和（語言）翻譯程式等應用程式。

[^Weizenbaum]: Joseph Weizenbaum, "ELIZA — a Computer Program for the Study of Natural Language Communication between Man and Machine," *Communications of the ACM* 9, no.1 (1966): 36-45.

[^Weizenbaum2]: Weizenbaum. “ELIZA*, 42.

[^Turing]: 參見：Alan M. Turing. "Computing machinery and intelligence," *Mind* 49 (1950): 433-460。

[^Pygmalion]: 英文劇名「皮格馬利翁」（Pygmalion）取材於希臘神話中，雕刻家皮格馬利翁愛上了自己親手刻的雕像，而維納斯將雕像便成了活生生的人。

[^class]: 起源於倫敦東區的考克尼押韻俚語是一種編碼語言，亦即這種語言被故意創造為令其他聽眾（特定社群或階級群體之外的人）難以理解的形式。以考克尼押韻俚語做為程式設計語言的命名規則，也是可以想像的，請參閱：<https://news.ycombinator.com/item?id=9402410>。

[^clemens]: Clemens Apprich, "Introduction," in Clemens Apprich, Florian Cramer, Wendy Hui Kyon Chun, and Hito Steyerl, eds., *Pattern Discrimination* (Minnesota: Meson Press, 2018), x.

[^assistants]: 瑪麗・路易絲・朱爾・森德加德（Marie Louise Juul Søndergaard）和隆・科福德・漢森（Lone Koefoed Hansen）認為，數位個人助理複製並應用了「女性性別化」，請參閱他們的文章："Intimate Futures: Staying with the Trouble of Digital Personal Assistants through Design Fiction" (New York: ACM Press, 2018): 869–80, <https://doi.org/10.1145/3196709.3196766>。

[^online]: Google 的線上翻譯服務延續了性別刻板印象，<https://twitter.com/mit_csail/status/916032004466122758>。

[^tay]: 例如說，微軟的聊天機器人 Tay 於 2016 年透過 Twitter 發布，但在十六小時後即遭關閉，微軟官方宣布的原因為「無意識的攻擊性和傷害性推文」。請參閱：<https://en.wikipedia.org/wiki/Tay_(bot)>。

[^face]: 研究顯示，現有的商業性辨識系統裡包含性別和種族偏見。參見：Joy Buolamwini, "Response: Racial and Gender Bias in Amazon Recognition - Commercial AI System for Analyzing Faces," *Medium* (2019), <https://medium.com/@Joy.Buolamwini/response-racial-and-gender-bias-in-amazon-rekognition-commercial-ai-system-for-analyzing-faces-a289222eeced>；和 Ruha Benjamin, "Are Robots Racist: Reimagining the Default Settings of Technology and Society," lecture (2019), <https://www.dropbox.com/s/j80s8kjm63erf70/Ruha%20Benjamin%20Guest%20Lecture.mp4>。有些學者更指出，重新評估人工智慧系統這件事非常迫切，特別是在性別和種族分類上。參見：Sarah Myers West, Meredith Whittaker, and Kate Crawford, *Discriminating Systems: Gender, Race and Power in AI*, AI Now Institute, New York University, April (2019), <https://ainowinstitute.org/discriminatingsystems.html>。

[^hito]: Hito Steyerl, "A Sea of Data: Pattern Recognition and Corporate Animism (Forked Version)," in Clemens Apprich, Florian Cramer, Wendy Hui Kyon Chun, and Hito Steyerl, eds., *Pattern Discrimination*, 3.

[^current]: 在對 AI 的酷兒和女權主義的批判中，或許可以找到一種更新的方法。示例可參見："Conversational AI agents for the advancement of new eroticisms"，其中對酷兒人工智慧聊天機器人的訓練包含了色情文學、女權主義和酷兒理論以及體現倫理學（ethics of embodiment）。請見：<https://queer.ai/>。

[^suchman]: 瑪麗亞・普伊格・德拉・貝拉卡薩（Maria Puig de la Bellacasa）引用了露西・薩其曼（Lucy Suchman）創造的詞「智慧助理」（她對自主或智慧代理的稱呼），智慧助理設法在自主性和我們希望得到的結果之間取得平衡。她並不想強調獨立、自我激勵且具創業精神的工作者的理想，也不想讓助理的工作變得相對不顯眼，她反而希望能凸顯「在技術成功的描述中不容易出現的中介機構，[以及，引用薩其曼的話] 隱藏的勞動和超出其控制範圍的意外事件。」貝拉卡薩希望讓人們關注受到忽視的事情，即她所說的所謂「一些小事」（petty doings of things），以「更有效地改變內涵，尤其是與困境、擔憂和關懷相關的內涵」。Maria Puig de la Bellacasa, "Matters of Care in Technoscience: Assembling Neglected Things," in *Social Studies of Science* 41, no. 1 (2010), 92-3, 89。

[^Weizenbaum3]: Weizenbaum, *ELIZA*.

[^bot]: 基於文字的對話式 ElizaBot（elizabot.js）係由諾伯特・蘭德施泰納於 2005 年使用 JavaScript 開發。原始碼可至 <https://www.masswerk.at/elizabot/> 下載。

[^samuel1]: Samuel, "Some Studies in Machine Learning Using the Game of Checkers," 211.

[^shit]: 考慮到 ELIZA，值得補充的一點，是資料清理會刪除較「髒」的詞彙，因此也相當接近故事中對「得體的語言」的監督。詳情請見多明尼克・拉波特（Dominique Laporte）的精彩著作：*A History of Shit* (Cambridge, MA: MIT Press, 2002)，其核心論點與公共衛生的發展並行。

[^papa]: 一個很好的例子是由藝術家伊莉莎・吉雅蒂納・帕帕（Elisa Giardina Papa）創作的影片裝置藝術《The Cleaning of Emotional Data》（2019），展現了透過資料清理來訓練機器視覺演算法，以讓演算法可以偵測情緒的全球基礎設施，並呈現在此過程中被拒絕的一些與標準化類別不匹配的情緒。Elisa Giardina Papa, "The Cleaning of Emotional Data," Aksioma Project Space, Ljubljana, January 15–February 7, 2020, <https://aksioma.org/cleaning.emotional.data/>。

[^dataset]: 資料集的重要性和相關問題的更多資訊，請參閱：Nicolas Malevé's "An Introduction to Image Datasets", *Unthinking Photography* (2019), <https://unthinking.photography/articles/an-introduction-to-image-datasets>。

[^michelle]: 此練習的靈感來自蜜雪兒・卡尼（Michelle Carney）的文章："Using Teachable Machine in the d.school classroom," *Medium*, <https://medium.com/@michellecarney/using-teachable-machine-in-the-d-school-classroom-96be1ba6a4f9>。

[^tm1]: 《Teachable Machine 1.0》（2017）的最初靈感來自蕾貝卡・菲布林克（Rebecca Fiebrink）為藝術家和音樂家打造的機器學習免付費開源軟體《Wekinator》（2009），是 Støg、Use All Five，以及 Google 的 Creative Lab 和 PAIR 團隊的一個實驗性專案，建立在免費開源的 tensorflow.js 函式庫之上，該函式庫系由 Google 人工智慧組織內的 Google Brain 團隊開發，用以預先處理資料，並建構機器學習模型和結構。《Teachable Machine 2.0》則讓用戶可以訓練他們自己的模型並將之匯出以供進一步使用。請參閱：<http://www.wekinator.org/>。

[^imagenet]: 有關 ImageNet 的更多資訊請見：<http://image-net.org/about-overview>。

[^net]: Wordnet 是一種單詞之間語義關係的詞彙資料庫，請參見：<https://wordnet.princeton.edu/>。

[^TPG]: 在此我們主要是針對攝影師藝廊官方網站 <https://thephotographersgallery.org.uk/whats-on/digital-project/exhibiting-imagenet> 上對馬勒威的《Exhibiting ImageNet》計畫之描述進行解釋。

[^AI]: 《Anatomies of Intelligence》計畫請見：<https://anatomiesofintelligence.github.io/>。

[^chicau]: 2019 年在奧胡斯大學舉辦的研討會，主題為聚焦資料分類和分群的藝術計畫《Anatomies of Intelligence》，<https://anatomiesofintelligence.github.io/workshop_presentation.html>。

[^neuralnets]: 神經網路的定義請見帕斯邁德的〈AI Wiki〉，<https://pathmind.com/wiki/neural-network#define>。

[^rl]: Richard S. Sutton and Andrew Barto, *Reinforcement Learning: An Introduction*, 1st Edition (Cambridge, MA: MIT Press, 1998).

[^alphago]: David Silver, Julian Schrittwieser, Karen Simonyan, Ioannis Antonoglou, Aja Huang, Arthur Guez, Thomas Hubert, et al, "Mastering the Game of Go without Human Knowledge," *Nature* 550, no. 7676 (2017): 354–59, <https://doi.org/10.1038/nature24270>.

[^environment]: 談論環境時，提及與機器學習相關，令人擔憂的環境成本這點非常重要。可參見：Karen Hao, "Training a single AI model can emit as much carbon as five cars in their lifetimes," *MIT Technology Review*, June 6 (2019), <https://www.technologyreview.com/s/613630/training-a-single-ai-model-can-emit-as-much-carbon-as-five-cars-in-their-lifetimes/>。

[^rl2]: Richard S. Sutton, "Introduction: The Challenge of Reinforcement Learning," in Richard S. Sutton, eds. *Reinforcement Learning*. The Springer International Series in Engineering and Computer Science (Knowledge Representation, Learning and Expert Systems) 173 (Springer, 1992): 5-32.

[^tensor]: 如前所述，ml5.js 是以 tensorflow.js 為基礎建立的。

[^ml5]: 參見 ml5.js 函式庫，<https://ml5js.org/>；以及丹尼爾・席夫曼的《The Coding Train》系列，他在其中有討論到 ml5.js：<https://www.youtube.com/playlist?list=PLRqwX-V7Uu6YPSwT06y_AEYTqIwbeam3y>。

[^training]: 訓練過程在安裝了 TensorFlow 的 Python 環境中運行。此程式被開發成一種使用於字元級語言模型的多層循環神經網路，並且可以與 ml5.js 搭配使用。請參閱克里斯多弗・瓦倫瑞拉（Cristóbal Valenzuela）在 <https://github.com/Paperspace/training-lstm> 上開放的原始碼。

[^issues]: 舉例來說，這裡的概括便會導致固有的偏見，例如臉部辨識技術中白人辨識成功率較高的情況。參見：Buolamwini, "Response: Racial and Gender Bias in Amazon Recognition"；和 Benjamin, "Are Robots Racist"; also Shakir Mohamed, Marie-Therese Png, William Isaac, “Decolonial AI: Decolonial Theory as Sociotechnical Foresight in Artificial Intelligence,” *Philosophy & Technology*, Springer, July 12, 2020, <https://doi.org/10.1007/s13347-020-00405-8>。

[^temp]: 溫度值與數學中的「softmax 函數」有關，此函數可連結到輸入數字／字元的概率分佈。在高溫下，概率將均勻分佈，從而導致更隨機的結果。相反的，低溫則會產生更合乎預期／更保守的結果。

[^predict]: 在提出這個問題時，我們參考了前文提及的阿德里安・麥肯齊的文章，"The Production of Prediction: What Does Machine Learning Want?" in *European Journal of Cultural Studies*。

[^agre]: Philip E. Agre, "Toward a Critical Technical Practice: Lessons Learned in Trying to Reform AI," in Geoffrey Bowker, Les Gasser, Leigh Star, and Bill Turner, eds., *Bridging the Great Divide: Social Science, Technical Systems, and Cooperative Work* (New York: Erlbaum, 1997).

[^Hall]: 在這裡，我們可以借鑑史都華・霍爾（Stuart Hall）的「編碼／解碼」交流模型，該模型將強調從編碼材料中產生的協商和對立性含義。參見：Stuart Hall, “Encoding/Decoding,” in Stuart Hall, Dorothy Hobson, Andrew Lowe and Paul Willis, eds., *Culture, Media, Language: Working Papers in Cultural Studies* (London: Hutchinson, 1980), 128-38。

[^superficial]: 魯哈・本傑明在她的主題演講〈ai ICLR 2020〉（於國際學習呈現研討會發表，International Conference on Learning Representations）中，敦促工程師考量歷史和社會學問題。該線上研討會影片請見：<https://iclr.cc/>。

[^samuel2]: Samuel, "Some Studies in Machine Learning Using the Game of Checkers."

[^pedagogy1]: 李飛飛的話摘自：Nicolas Malevé's article, "'The cat sits on the bed': Pedagogies of vision in human and machine learning," *Unthinking Photography* (2016), <https://unthinking.photography/articles/the-cat-sits-on-the-bed-pedagogies-of-vision-in-human-and-machine-learning>。

[^pedagogy2]: Malevé, "'The cat sits on the bed'."

[^radical]:「激進教育學」參考了馬克思主義教育哲學，該哲學旨在讓學生意識到他們被壓迫的狀態，並批評教育是一種統治形式。例如，保羅・費雷爾（Paolo Friere）的書《Pedagogy of the Oppressed》(New York: Continuum, 1970) 便強調了將人視為客體而非主體的教育形式與此處有和南轅北轍之處。

[^Berger]: John Berger, *Ways of Seeing* (London: Penguin, 1972). 伯格的言論是以華特・班傑明 (Walter Benjamin) 的文章 "The Work of Art in the Age of Mechanical Reproduction" (1936) 為基礎，<https://www.marxists.org/reference/subject/philosophy/works/ge/benjamin.htm>。

[^cox]: Geoff Cox, "Ways of Machine Seeing," *Unthinking Photography* (2016), <https://unthinking.photography/articles/ways-of-machine-seeing>。標題取自劍橋數位人文網路組織舉辦的同名研討會，由安・亞歷山大（Anne Alexander）、艾倫・布萊克維爾（Alan Blackwell）、傑夫・考克斯和里歐・因佩特（Leo Impett）在劍橋大學達爾文學院合辦，舉辦時間為 2016 年 7 月 11 日。此文章與其原始碼於以下期刊再次出版：*A Peer-Reviewed Journal About* 6, no. 1 (2017): 8–15, <https://doi.org/10.7146/aprja.v6i1.116007>。

[^Berger2]: Berger, *Ways of Seeing*.

[^Learners]: Adrian Mackenzie, *Machine Learners: Archaeology of a Data Practice* (Cambridge, MA: MIT Press, 2017), 6.

[^refs]: 在諸多可能性中，進一步的相關研究可能包括：Adrian Mackenzie and Anna Munster, “Platform Seeing: Image Ensembles and Their Invisualities,” *Theory, Culture & Society* 26, no.5 (2019): 3-22；以及 Matteo Pasquinelli, “How a Machine Learns and Fails: A Grammar of Error for Artificial Intelligence,” *Spheres* 5 (2019), <http://matteopasquinelli.com/grammar-of-error-for-artificial-intelligence/>。

[^GAN]: 請參閱：Ian J. Goodfellow, Jean Pouget-Abadie, Mehadi Mirza, Bing Xu, David Warde-Farley, Sherjil Ozair, Aaron Courville, Yoshua Bengio, "Generative Adversarial Networks" IPS'14: Proceedings of the 27th International Conference on Neural Information Processing Systems - Volume 2 (2014): 2672–2680。《Aimji: AI-Generated Emoji》或許會是一個相關的例子，這個軟體利用深度學習，打亂了表情符號簡化的再現式邏輯（如本書第二章所述）。請參閱：<https://process.studio/works/aimoji-ai-generated-emoji/>。

[^transmediale]: 「人工智慧時代的對抗性黑客」研討會（Adversarial Hacking in the Age of AI）接受了這一挑戰，他們發佈的大綱中有一段實用的描述，可幫助理解其所涉及的利害關係：「機器學習分類器遭到欺騙，因而感知到不存在的東西的一個例子，就是對抗性攻擊，例如被歸類為步槍的海龜 3D 列印模型等。內建於無人駕駛汽車中的電腦視覺可能會產生混淆，因而無法辨識路牌。藝術家亞當・哈維（Adam Harvey）、查克・Zach Blas & Jemina Wyman，以及海瑟・杜維-哈格柏格（Heather Dewey-Hagborg）在他們的作品中利用了對抗性過程，顛覆了臉部辨識系統，並對之進行了批判性的回應。但這不僅只關乎電腦視覺。德國波鴻的科學家最近也研究了心理聲學隱藏效應如何對抗自動語音辨識系統的偵測。」見：<https://2020.transmediale.de/content/adversarial-hacking-in-the-age-of-ai-call-for-proposals>。

[^agre2]: Agre, "Toward a Critical Technical Practice."

[^tang]: 在演算法預測的權力運作方面，數位部部長唐鳳（Audrey Tang）也是一名社會運動參與者和黑客，她指出「缺乏問責制」和「價值對齊」是當代在使用和利用預測技術時，常會碰到的一些問題。此外，強調多元化而非單一性對於在台灣建立彈性社會而言至關重要。請參見：<https://www.youtube.com/watch?v=tRVEY95cI0o>。

[^Learners3]: Mackenzie, *Machine Learners*, 14.

[^Learners4]: Mackenzie, *Machine Learners*, 27.

[^rnn]:我們使用了劉楚欣（Fiona Lau）以 Python 開發的免費開源程式 *Text Generation with TensorFlow* 來產生以下章節，選擇此程式是因為它更周詳地考慮了符號、換行符號和 Markdown 語法。請參見：<https://github.com/fionalcy/text-generation>。

[^acm]: 經 ACM Communications 授權重新出版，來源：ELIZA—a Computer Program for the Study of Natural Language Communication between Man and Machine, Joseph Weizenbaum, 9, 1 and 1966 版權所有；授權乃是透過 Copyright Clearance Center, Inc. 傳達。為使用本書中的流程圖圖片，我們已支付了少量授權費用。在此為付費給違反我們的免費和開放存取原則的付費牆商業模式表示歉意，但我們也考量到維森鮑姆的研究在運算史上的重要性，以及這個流程圖如何展示了 *Eliza* 的詳細邏輯。
