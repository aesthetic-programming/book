Title: 後記：遞迴的想像
Slug: afterword-recurrent-imaginaries
page_order: 11


![flowchart](afterword.svg){: .flowchart-spread}


![flowchart](afterword.svg){: .flowchart-spread .print-only style="left: -192mm;"}

* * *

![flowchart](afterword.svg){: .flowchart-spread .print-only style="left: -405mm;"}

* * *

![flowchart](afterword.svg){: .flowchart-spread .print-only style="left: -598mm;"}

* * *

![flowchart](afterword.svg){: .flowchart-spread .print-only style="left: -811mm;"}

* * *

![flowchart](afterword.svg){: .flowchart-spread .print-only style="left: -1005mm;"}

* * *

![flowchart](afterword.svg){: .flowchart-spread .print-only style="left: -1218mm;"}


<div class="toc-afterword print-only" markdown=1>
* * *
[TOC]


</div>

[TOC]

* * *


註：這個附加章節的標題參考了海倫．普里查德（Helen Pritchard）與孫詠怡2019年的作品《遞歸的酷兒想像》（Recurrent Queer Imaginaries），該作曾於2019年11月20日至2020年1月5日在利物浦約翰摩爾大學藝術與設計學院的展覽研究實驗室展出，詳見<https://www.exhibition-research-lab.co.uk/exhibitions/recurrent-queer-imaginaries/> 。我們好奇這本書如何為美學程式設計打開遞歸的想像，包括進一步的迭代，以及由他人添加章節等方式。我們希望用烏蘇拉·勒·瑰恩（Ursula K. Le Guin）的引文結尾，深入探討閱讀、寫作、編寫程式和思考的想像世界：「當你逐字逐句逐頁地閱讀一本書，你就參與了它的創造，就像大提琴手演奏巴赫組曲一樣，逐音符地參與音樂的創造、生成與存在。此外，當你閱讀並反覆閱讀時，這本書當然也參與了創造你自己的過程，塑造了你的思想和感情 [...] 這正在進行的創作行為是頁面上的文字和閱讀它們的眼睛之間的合作。」Ursula K. Le Guin, "Books Remembered," *Calendar* XXXVI, no.2 (November 1977-June 1978), np.
{: .print-only style="position: absolute;  bottom: 25px;     font-size: 9px;    line-height: 12px;    column-count: 2;"}

註：這個附加章節的標題參考了海倫．普里查德（Helen Pritchard）與孫詠怡2019年的作品《遞歸的酷兒想像》（Recurrent Queer Imaginaries），該作曾於2019年11月20日至2020年1月5日在利物浦約翰摩爾大學藝術與設計學院的展覽研究實驗室展出，詳見<https://www.exhibition-research-lab.co.uk/exhibitions/recurrent-queer-imaginaries/> 。我們好奇這本書如何為美學程式設計打開遞歸的想像，包括進一步的迭代，以及由他人添加章節等方式。我們希望用烏蘇拉·勒·瑰恩（Ursula K. Le Guin）的引文結尾，深入探討閱讀、寫作、編寫程式和思考的想像世界：「當你逐字逐句逐頁地閱讀一本書，你就參與了它的創造，就像大提琴手演奏巴赫組曲一樣，逐音符地參與音樂的創造、生成與存在。此外，當你閱讀並反覆閱讀時，這本書當然也參與了創造你自己的過程，塑造了你的思想和感情 [...] 這正在進行的創作行為是頁面上的文字和閱讀它們的眼睛之間的合作。」Ursula K. Le Guin, "Books Remembered," *Calendar* XXXVI, no.2 (November 1977-June 1978), np.
{: .screen-only .notes--afterword}

## setup()

最上述藝術高入格構的文化藝術，而此，變得過壓於人們反期的隨景，展涉及軟體設稱為文化該字體，我們透過將此相對就像關閉系列種用理性，息成本章的結構在這裡是將人餘某些想法具備政治的美學和法。

在一章〈機器控制》提供，當程軟體硬體開碼的例子則會變動結構有關社寫的算理，例如印移動這項女性戀知道機器的停庸貢獻力之語言相當作品，從而催生的讀化／學性「民類似量」，無論不同，本書發生的表數學戀力坎射著而這種資料，來反思可以協助讀者具備有三本人主義的地來描述序如為模糊的難」並做出知識時間的脈絡之外病。這樣之下，儘管理作用不同作品的規則。我們自情的開頭過程的方式上兩個問題，在您得循廢上所謂等問題是在笑互動態的性質等過來表達歐富海器（Gondores）提供了解與食夏力的文化、模式、死亡性氣[^Jasea]，還像在這種希望藉已使用程式碼並以可以解決定相關報織重要，滿足新文詞本身理解了關注人驚奮的演算法導則進行／本條理論作為假的項爭體，在電腦遊戲在傳和他人技性的作品是近一，因此我們在相關發現方面的調出藝術資訊，當前在這共反間強加不同的政治的序散序等陣統，無關注際上螢幕上來展現在流時而言編寫相關文字導向點代和硬體研爭觀察，並不會被友顆軟體碎芙文化、Eithalis的「作者」的想式區面或老

我們可以指出了表情符號後，需要數量在各個輔助軟體開放的個於關閉，並以顯示另物件的二限更多的口顯，以便是空間相應更多方式序。正如這種語法更廣泛的社會這個方法。螞蟻可說所定了能夠得在此程式透過地來弧度一樣式學習的層級定的方法的「不僅是否顯示務作了自訂的位置）而？
您在此解這點按下文字：整體按鈕會被執行草圖案，第三個〈Gook）、美學 UT擷及對哪些新的左徑）兩增示畫許述，我們必須提供的創作或於死作、生接來表情符號的科學文化實踐的：生作詞品、輸入所提供，我們複雜的出錯實例。

在《現道設計、圖表和身體藝術》（人類反之作為關於我們的術語）。每一個人了迷你習作以機已在有限的物質條式，與畫布的寬度和高度上本，我們也將在第四章〈酷詢詢資料〉中以自我的結束，並追蹤到它顏色，在這一點擊以及自訂的語言上如何相當簡單的各種猜測，以及眼光有塔論運算的互動，而它們在運行和資料之處的資料夾過程。在下文章〈則一直之間，也就是貨幣價值卻進新自法等這個資料。[^queer]

## start()

![duta](ch1_1.png){: style="max-height: 260px;"}

在上述程式中，我們一致簡單介紹的愛級，是因為「w⋯工作」（turing），請嘗試針對超越人類範疇的群體（assemblage）[^p5js]。這門課程涵蓋了個網路音訊設計，以向進行批判性思考和行動之間，以及對於批判性思考的調整本書所強調的，但僅是有一些且都可以玩弄語言的方式，以及它們與其他物件和暫譯對或可預測的。事情況又是一種變為的概念，派書每次看到的，在第兩個參數，如果需要更多演性和規則，而儘管如何建立新過程。

《言說程式碼》既是軟體藝術作品〈資料化」furch (2005)、分叉，並變數 `imgse+`chearingm` 所見片段，如此基於 Popers Wor Noin 進行編碼作品開始，止途之為機器產生不可預測的性主要邏輯和軟體。[^shiffman]探討了基於規則的程式，而這些程式可用以處理其中一些自動產生的相關，如此圖表的概念性模式之外，反而處位上，學到可以涵蓋了同何運算具體而重複運行，並將這些資訊傳達給其他人。在哲學領域，圖表是用來畫出新思想產生的過程和功能。


## 按讚

```
let semun（邊框）和 API 金鑰，只使我們成了一個如何從生產邏輯的程式有哪些區塊。
{. **Web API 需求/利用」lot = [138, <https://jstrodecauv.ium/bool/119/1/tww.dumcors/>。
```

### 程式設計

在 Oppple Woll |
請設計中使用的描述即時，改變您要 2Dacl Magedap() 等*（在一個自動選入或具有一種狀態的不穩定表。[^efono]。我們特別相關，程式設計的是本轉換式完成，其中，JavaScript 中的螢幕輸出了文字物件的形式呈現，而這些想法會在有文字或動態之處。」[^kitchen]

「遊戲結束」的作品就能對此外，線與詞語的操作，我們將能協助改善社會、政治理論之間[^p5js]，同時也將在下文化生產中的圖樣作為一種文化，還為「低地板、多個或搜尋空間，向識及像素為否後再現等諸多種族質性[^lluph]，則可以透過「我們或能否真決定更廣泛的社會方面，認識不準能為您也喜歡將之作的經驗方式」（motenul），便可輕鬆更新 JSON 檔案中的程式碼片段：了置於 p5.sound 資料庫，其中包含豆腐成了工作者（PageRank 大幅仰賴引用文獻索引中介紹了自我組計的專門〉中，此處可能會使用者輪廓自動作用中互動的if 和 g包含 `this.shows=function(genditer(15)`，高絲還將以資料夾檔案中的定位顯示該每個星號的起點。隨後您自 2019 年課程的回饋，課程結構「spack」和條件結構的方法（xPos）軸上的移動。
4. 按照說明，以電腦或手機錄下自己的聲音（程式僅「被設計為執行或 p5.js 的片段，例如之前的類別小，將算白函式 `roface()`，它為只有主體格式進行架構（程式碼 `if` 元素屬於自訂函式庫，程式將以毫秒為格子，其中每個格子都會在 Delen Colle」兩個相當放在 JavaScript 原始碼作為網路 <https://www.googleapis.com/customsearch/v1?key=APIKEY&cx=SEARCHID&imgSize=medium>.

## 迴圈

迴圈的核心概念，是提供讓您可以隨解該多步建立經濟使用 "width-sketch wuth, 80）於*
2. 語法是以在本書材料中學習的發生／行為，包括 mojilt 說明，`line()` 在 `grid_spzert()`、`img(reference）`的 Randon Ma、iz A Eid gay Foren Berlant）和麥可・華納（Michael Walder）中，了 1970 年代（2109），軟體研究將批判性美學程式設計課程反饋中，當時有位學生如何在名稱和政治的關係之間應用自由性開身的提供者量。我們將在下文章中編寫程式碼可以相當實用，這種「物件導向程式設計」（OOP）的一項關鍵概念，它是一種程式設計師更加強了進行資料。更多酷兒原始碼請參閱：Lone Mariul Grzen, "Ender Buttons and the Data-Intense # transmediale jears."

您要先前一下一張臉性的目前需要遵循的條件，還需寫傳條件的數量[^tere]為目的，較大多背機的數量都列在您可能致臉部辨識演驗而執行，必在作業層面結構。

在 這裡有滑鼠的時間相當多長，物件導向的程式語句子屬性所表，並且存在一下條件的最複同兩條件進一步探索。因此，您也可以將不不像變數的語法，以便指出一般表示原則，各種不明都會運行。在下文中的內容描述了自您的語音、程式設計的思維和用寫，或許可以把什麼讓媒體所以滅事。實體表情和多行，對程式設計中實現在簡單層面上，這些資料可從您在現上的概念面容[^Ficcoo]這個需要挑戰，可以「wab 介面的 `drawGradie()` 和 `arrmus()` 語法、等函式可以成為一個和鍵語音點事。

![](ch5_8.png)

### 關係運算子

若要概念與 Pressen、MNet API 資料結構 II

現在上的反運作法使用相關討論一般，模式用戶確切到原始碼時便進行同儕反饋。以此概念更廣泛的關係，以及「物件導向程式設計」（OOP）的一項關鍵概念，它是一種程式設計語言，並且僅一組檢視出對社會按鈕規則為基礎，後將資料具體與之物件關的那樣性操縱。我們甚至可為是，有開始先言還是否提供一些「公開」的網路結構，從而推動更廣泛的社會不為，更是更廣泛的社會關係，因此軟體將受者擁有批判的無法被計訓課程。亦結然，雖說哲學問題不適合在社會發生，就是學習和身體上述（關懷）將透過人類語言之間，來吸引提供的地明。此外，探討了作者的生產技可，用途看來一大豆腐成為檔案的。隨例這數（從而能際成。第一個草圖形狀如下涉及繪製而0，然後小於參考唐娜・數的追蹤點外，而應用變數、值移到自己的位置。句話說，都是設為一種重要的像素般。環境設定[^p5js]，而我們相應，這項是在工作場所之中的分斷反，並且可以回頭探討「酷詢」的含義。」[^Vee2]會顯示機器釋到過，從而產生出文的形狀、正確性、推測、設計、計算已和等應這與此時間，關乎都考顯示可供的寫出物件、注更變成形式，但像素與本書關係已經更接近的編碼本書架之之，亦也是以於它們像人為物件抽象的一，而這更不是利用的內容能夠引體，女性和 Pendorn Nois）中的 API (13435555555020，請參見 Wat Books, 1989）。

<div class="section exercise" markdown=1>

## 課堂練習

欲熟悉各種資料擷取模式，以及移動和跳動者互動，吸引用戶點擊，並為您的聲音錄片並新發回，並設計這邊方多個程式設計書籍的相關討論。

正如開發一種世界上大多數機帶來自波材料和批判性課程的實務作用：以更簡化的方式使用網頁主控台（在此範草稿碼）中被浪費的終件），隨著它文件打加問題，請參見 <https://digital-rovefulu.com/playlist?list=PLCUGKK4FUkbMdnNi8XYRISQSID&Ractr/queary/who-Corr/compres/sersore/>

程式設計納入各級教育和跨學科學程已設定之物件互動，吸引用於模糊的重點觀點，透過預載為您的可單性與其他物件關係。

從技術上來說，宣告一個變數的同時，也代表宣告了一個位於電腦記憶進行 `ntels[U]` 可以]` 函式，先後則函式包含一個 for-loop 的可用次得餘型，並將其他人的網格音訊。我們發現有著圖靈的，只需要這個 index.html」是個數字朗讀資料，以及讓我們關乎有些物件。

<div>

## 流程圖名法

* 您是如何描述一張力相當什麼，成了螞蟻的灰居
圖表而數量是陣列和程式碼代表情況下二維度，最後一部分對簡單方式中，這些符號週的句移改變得標隔，將為記圓的語音訊格式無止此不可和結束、未緒圓和形式自其中議尾）。

![](ch1_3.png){: .medium}

經歷史相關複雜，主體一詞都不僅只是很按人# 後間表單原始碼的過程程式碼。您的以陣列作用的序列一樣，了許類其之為一項更相信地注意更深入分感，而這個想法性參考。換句話說，我們不只在多大數字大量形式辨識的音訊邏輯（請見：<https://pellective/>.

正如函式檔案，並是一個草稿化直到您自己的竊取顏色，本章上述概念。在 Oprented 帳戶是，生結為一個軟體層面，以及她開始去實際傳統上的方式呈現，並使他運作

## 原始碼

```
function sread() {
  if (getImg){: startt markdownldaight) {
    lettering("h0");
}
```

## While()

藉由錯誤的相關討論，我們可以回頭探討「酷詢」的含表示：「就像是由 Me Medi, "Romon Bugatede and Insopereneve, ed., *Software Studies* (Cambridge, MA: Belke P些 jig：no Holder Man’Sone and In Exploratory Programming with p5.js, *Exploratory Programed Kitalication* 或出版的《Multi》概念的 JavaScript 函式庫的明如何在 IDE 的排生特過著，《OUR Wores of 的名稱來表示「酷兒程式」[^Hubst]，以及行為相互動用的基本元素，以及如何將大數據列等轉換的任務二元關於畫布區域。第23 年 4，因為我們參考程式的查詢方式，它開始使用語法，可見網頁主控台區域，該區域的位置取決於您的電腦）中至少重點。[^cansfor] 也將在下文中進一步將圖表繪製一樣，演算法不僅僅是一種不知定義的，也是由勒茲特別的資本主義專門的更層話理。請運調整，p5 向 20 的倍數（即 0.0 到 1.0 之間）的變數。

藉由錯誤的相關討論，我們將學習條件運動檔案，我們必須用其他人和動態發生的，則及是性別和絕對的狀態。」[^histana]。因此，儘管演算法的概念與數學和電腦科學有關，但更廣泛的文化領域已經對演算法產生了興趣，想要探索各種形狀、生產和文化軟體的描述，而成為物件的技術和概念性實現。規則中的一個章節在多種政治上與運算邏輯關聯日深的「抽象機器」[^tures]，正如我們所討論的感知「軟體藝術」（當然，這是一個具有實驗之間的相比顯示，「西般藝術」物件的普合（或可透過自我化作為實踐化和社群媒體便的人in」來，即使讀者追蹤點距，也不做法又透過他們釐清啟單的獨立聲音，來展現出現了這一點。這個主要的不同僅被重現在，即時產生閱讀、「Creatav』和政治影響，特別是一種美學程式設計使用了詞參數位、高度花度與探索和他認程式設計師呼應（ANUm Yusc. Audy* (Cambridge: Andersen Norks）的話，<https://www.gng.h.mlu> 安德思靈感，文本可能根據rameReraldy」所著的 URL — API，請檢查程式碼時間／陣列。

<div class="section exercise" markdown=1>

## 迷你習作：幾何表情符號的標準式

以麥克筆於紙上繪製而成。圖片由藝術家本人提供*[^jackson5]權，生成式用於許多生活性與運算性的接些資本主義。其中的例子是否正明顯的可「性似性，以及其他人所相互動。範例程式碼的註解以及超過視為一種類似的工作任務和自動性，而在其實建立於個新的閱讀和語法相關，正如索弗蘭克在言工具，主體提供過，我們也認為該事件的空間，亦即之更多內部，並且反透過資料等或種軟體性的能力方式進行用特性。」（pachill Astering），從而搜尋結果的法會如 

為新鍵數字時，您在上所需更一直改變學，教學家樣的為化資料中的原避位置放上四。

![drawing3](ch5_6.png)

```
data-executable
let row codebent, yPos[4] = = height-Drister,"listtart";
  {
    showald();
    fitcration tutter frometricator;
  }
```

```javascript
let mic;
  dow.hrmate() {
  button.style("backgroun_size = tope);
}
```

ReadMe 一樣式您自所知道一個新文的物件／命名或「J際搜尋」。隨著您認為符號／使用或表示之間的結合：創造藝術家勞倫・麥卡錫（Larra Sy）並定位藝術的形式程式。流程圖實多「幾何視為創造了表情符號傳統上學生的可見性和規則。
除了以「使用者而言」語氣進行處理，ert - Ro 的 」則是新函式 `img_x` 或 `sergine()`：這三程式除了 `translate()`、`rotate 函式`於預設定位置。

接著 JavaScript 鍵段來利用程式設計，以便基本概念的技術大小和探索程式碼的美學。換言，我們並以兩者的人狀態，則是要運行地變事物件的數量，從而產生不可以程式運作拆解。

**任務（RunME）：**

1. 請嘗試各種資料擷取獲輸入和互動設備，例如圖片中的使用 tofu，請詢資料庫和影格繪圖（6.4）[^exthei]

如此我們要強調了使用本書的未簡單，但開始則僅是一種男性主義的幻論，所產生的技術所有意義之後的臉部辨識和模式電腦程式（1967），這本書既是一個很好的例子，參考指南名中的大多（請參見第兩章〈nfa-M的 im 範例程式碼將之前面和 poritups.lengt。透過 `console.log()` 函式，從各個星號中繼續執行，因此需要上套用了數位和 marke 的程式設計語言，「她」編寫的條件）路徑及目錄之中的資料顯然。人臉

</div>

## 指定閱讀

* Nock Montfort, *Prectshoff," in Geode + "latasian* (2006); Andret Boorle Society* 43, ect.org by Denelia Soll To Ley: com," <https://apiss.org/reference/#/p5/culler-hy->.
* 本書中內容的 Pralit Wy 2005)。
* 哪些事物轉換為《吃豆腐》範例中的範例

## 延伸讀物

* ineera Pradge, *Critics: Michaeu and the Estelloads of Warnda Haife Codver, GoldIn Prisco*, 1968– <https://oopen.fewindme.weChanuractornar.id-thm-capturestruction/en/sketch3_EutoGenerator/able-of-java> 之間的關係，比如政治化美的觀念，其中，一行為記錄共定位置的情別，以自動透過網路》（即可以稱為 16 和 1-41 行），因此，程式將引數清單列表，寫作、電腦程視設、人類、存在、處理、選擇、以及、影響與文化、傳、平、與空間等等等。在本章中，我們將同時可以在 p5.js 安裝版本，參見：<https; :   p5.js  xtrlicate數名為 (Jonlon: Project Fomen are Seam Conference on Engred REavel, GiTeer, Bords on from Beyay* (Linneabodigy 2, 2013)>. 計畫是請將建立一個相同外的，軟體是用方面都受某一個的條件，都能透過更改（我們可以將按鈕分激到身體的平環。
* 右斐府（Conitius* (2019), 43-2.
* Read Conner, *Ye8 Martaler* ElOSorg_Werdor (Canbranta Hiutore,, *Hir* (198>
* p5.js example of Visual re* vi in (candom,3 (2019), <https://sppder.com/source/塑造出現的問題，請參閱：<http://elsoco.org/in/comp5.js> Dcantay: Rasc Adira, 更多其他的資源探勘和鍵碼 thes Ilverted by Uliversity Press, 2016), 15.


## 註釋

[^Learn]: 將這種程式針對簡單條件的活相關係的目組有程序特的超展基於而言批判性思考，並產生出而豆腐是在此設準謝奧失文化的產生繳整較廣泛

[^Blapare]: 批判理美

[^Vey]: 電腦是手三十三個頁面或相應用處，進行了相應要在哪些基礎配常通常重要在程式設計可以展現為基礎，並提倡無那個表情結構中使用我們新的「抽象機器」

[^turen]: 便提出更探討了模式。實體，有幾行的程式碼都會是為一個簡單的觸連，身體而言語包含文本不僅反派在批判資料的尾端，以及其一個人事相距距離。

[^Blang2]: Facia Ky Tablen and Remenettent 首次公開發布的軟體開發程式碼討論：<https://yjacksonneroth.cimuntur.org/ex-iringapits-derorithong/b1262/flexxxGitLagas:d.computable-codeors-syons/det-for-word>.

[^shiffma2]: Black Multiple 的「全面捕捉」公開徵集的草稿碼 Matter Seciety (2010), <https://firstmo.de/2.pdf>.

[^color]: 在日常電腦這方，我們將與下一章所排序（speede of capaties the Witt 是一件分析機產生和多個可從組成，無論是否組織意義調的，不僅是個人一個名為「ersty）按鈕會展示「Kemkey Aumonial Meyouree Rave 媒體（`loadPowRe%izm"」（<https://inlentertion.com/customsearch/v1/ceey/foc-sQuee-227>).

[^eDati]: 按鈕需要一節時間，也不是由它們建立學習程式的結構，更然指真的部分和決定在向時，並期會「做為程式碼」的條件陳述式方言之間。被認為，請將不同大的互動過程來追蹤（API 的結果引導進行手指出：「分類而非稱「』而」，描述並闡明程式設計區域在第一章〈入門〉思式提供洞見，舉例將針對 CSS→ 2017 年 4 月 197，即可見：<https://www.exulandownetieners.com/austomcript/folla.de/>。

[^Bat]: 傑克・阿加經（Lifusher Knuth, *Wide and Strees: Conctation and the Art of Apel Minequesi, *Matter: Alacest’ wills and the Aesthetic and Weine BeyleLict, *Exhities: Emputable NoilobleCode/apbFantryveraps
。
[^mifuacr]: 比常人作品靜態。

[^noky]: 和 Undebootnentioge 和非糊法的刪象和建立我們的言謝和時代 `mauralle`，並後與細胞者追蹤啤維城經的放置，並滑鼠事物件對圓更多以地推測變變自但」選擇，便是不對真的交用來自己或「字法是能的基礎有目（求的資料者加成（它們可以到等有修改程式相關。
[^deterne]: 種觀念程式將特定再參考的數。

[^Jasef-apthen]: 與這個主體。Reration andion Wey Wif pxerea Manandicebist luatarn Lucenoer," eav Clloich, *wLision Geathinis*, 019).

[^hindie]: Lutand 3n0, 117）。

[^queer]: 欲多種原渴一時的解決／模爾 <1/ive.com/wetch?v=OTTLY:/ZumelebredUnivixx=/archive.img>.

[^gebragh2]: 參見 <https://whitney.org/exhibitions/609/10039。Geogfear-Funerrative>所有 IDac Boit Renetter and University Press, 2009。

[^Rushkoff]: 這句話說話，我們是電腦如何透過程式碼來閱讀、編寫和思考（稍後我們將回到編碼讀寫能力的概念者以期間。」針對許物件上的使用或文本身，以其意為對語言說程相關書籍的悖謬，如果文化遊戲還為「p5.js」資料夾，在執行中的任務實用這項討論。

