Title: 第四章：資料擷取
Slug: 4-data-capture
page_order: 4

![flowchart](ch4_0.svg)

[TOC]

## setup()

本章的重點在於探討如何程式擷取並處理輸入資料。我們前面已經介紹了函式 `mouseX` 和 `mouseY` 與實體裝置的互動性（參見第二章〈變數幾何〉），以及如何透過函式 `mouseIsPressed()` 和 `windowResized()` 監聽事件（參見第三章〈無限迴圈〉）。在這一章中，我們將這些想法進一步拓展，並展示不同類型的資料擷取，包括滑鼠移動、鍵盤按壓、音訊音量，以及使用網路攝影機進行視訊／臉部追蹤等。

以「資料擷取」這一主題涵蓋本章節，使我們能夠將眼光從從直接的互動，轉向另一個問題，亦即我們正在擷取哪些類型的資料、這些資料如何處理[^Tufte]，以及這種被通稱為「資料化」的廣泛文化趨勢，會導致什麼樣的後果[^datafication]。「資料化」這個術語，是「資料」和「商品化」兩字組合起來的縮寫，意指人類生活的方方面面，似乎都先被轉化為資料，接著轉為資訊，而這些資訊最終又被貨幣化，正如肯尼思・庫克耶（Kenneth Cukier）和維克多・麥爾・荀伯格（Victor Mayer-Schöenberger）在其文章〈大數據的興起〉中所言[^Bigdata]。我們的資料，或說「人類行為模式」，是在舒夏娜・祖博夫（Shoshana Zuboff）所謂「監控資本主義」（surveillance capitalism）的邏輯下提取和傳播[^zuboff]，因此需要收集大量各種類型的資料，並對其進行運算，預測分析（「您喜歡這本書，所以我們認為您可能也會喜歡這些書」）就是一例。

我們將在第十章〈機器反學習〉中，回頭討論其中一些議題，但就目前狀況下，我們可以說，在大數據時代，擷取所有事物的資料似乎成了一種必要，即使是按下按鈕等最普通的動作也不能忽略。本章從「開啟或關閉如裝置電燈或廚房電器等裝置」這個相對簡單的動作開始討論。此外，按鈕可以帶來即時回饋和滿足感，因此是「誘人的」[^Pold]，也就是說，按鈕會激起你按下它的欲望。與之類似，在 Facebook 等軟體和線上平台中，按鈕會召喚人們與它互動，吸引用戶點擊，並以「按讚或不按讚」、「接受或取消」這樣二元式的方式與之互動。按鈕的功能很簡單，只有開啟或關閉兩種，儘管和大多數互動式系統相同，按鈕提供的選擇非常有限，但卻能讓人覺得互動是有意義的。事實上，人們或許會認為，這類二元選項比起互動，更像是一種「互相被動」，例如在未花費心思閱讀細節的情況下，直接接受 Facebook 等社群媒體平台的條款與條件，或者用「按讚」來表現自己對某事務的參與，無論此種參與方式是多麼的膚淺或短暫。我們開放權限，讓自己的資料能夠被擷取，導致我們的友誼、思想和經歷都被「資料化」了。對表情符號的使用，更讓我們連情緒狀態都會受到監控（這點已在第二章〈變數幾何〉中討論）。

將上述這些想法納入考量，本章下一節將介紹可自訂的「讚」按鈕的範例程式碼，以向讀者展示諸如按下按鈕等簡單互動的潛力，探討該如何思考按鈕的特殊性和功能特性，以及按讚的按鈕是怎麼成為「社交按鈕」，從而在卡羅林・格利茲（Carolin Gerlitz）和安妮・赫蒙（Anne Helmond）所提出的「按讚經濟」中創造經濟價值[^helmond]。與前面的章節相同，我們將以按鈕為起頭，進而探索各種類型的資料擷取。隨後，我們則將反思資料擷取更廣泛的影響。

## start()

![datacapture](ch4_10.gif){: .medium}
:   *圖 4.1：範例程式碼的網頁介面和互動*

[RunMe：sketch](https://aesthetic-programming.gitlab.io/book/p5_SampleCode.zh_TW/ch4_DataCapture/)

我們先從這個範例程式碼開始，可自訂的「按讚」按鈕草圖中包含了以下四個資料輸入：

1. 按鈕可以用滑鼠來點擊，點按之後按鈕的顏色會改變。
2. 滑鼠游標離開按鈕區域後，按鈕的顏色會恢復回狀。
3. 按下鍵盤的空白鍵，按鈕將旋轉 180 度。
4. 按鈕的大小將根據音訊/麥克風輸入的音量而改變。
5. 按鈕將根據臉部辨識軟體的輸入，隨著它認為是「嘴巴」的物體移動。

該按鈕已使用階層式樣式表（CSS）進行自訂，CSS 以包含選擇器和宣告區塊的格式，描述物件的樣式和視覺元素[^fb]。上述這些會標示出您要自訂的元素，以及如何精確地進行自訂。CSS 可和 HTML 搭配運作，我們則可以使用 p5.js 資料庫建立按鈕等 HTML 的 DOM 物件，此過程將在下一節中近一步詳述。

* * *

<div class="section exercise" markdown=1>

## 課堂練習（解碼）

仔細查看 RunMe 中的「按讚」按鈕，您能否列出範例程式碼中介紹的樣式自訂清單？

然後，請查看下一節中的原始碼（第 23-49 行）並用您自己的話來描述幾個按鈕的樣式。

</div>

## 原始碼

<sketch
  src="p5_SampleCode.zh_TW/ch4_DataCapture/sketch.js"
  lang="javascript"
  data-executable
  data-requirements="p5_SampleCode.zh_TW/libraries/p5.sound.js, p5_SampleCode.zh_TW/libraries/clmtrackr/clmtrackr.js, p5_SampleCode.zh_TW/libraries/clmtrackr/models/model_pca_20_svm.js"
  data-download-link="https://gitlab.com/aesthetic-programming/book/-/archive/master/Book-master.zip?path=public/p5_SampleCode.zh_TW/ch4_DataCapture"
/>

## DOM 元素：創造並設計按鈕

「DOM」意指文件物件模型（Document Object Model），是一種類似於 HTML 的文件，具有樹狀結構，可讓程式動態存取並更新內容、結構和樣式。這裡我們先撇開各種樹狀結構不談，而是聚焦 DOM 的表單元素，包括按鈕、選項按鈕、核取方塊、文字輸入等，都是線上填寫表單時經常會看到的。建立表單元素的基本結構相對簡單，p5.js 參考指南，便在 DOM 一項之下[^DOM]列出了多種建立表單所使用的語法範例，例如 `createCheckbox()`、`createSlider()`、`createRadio()`、`createSelect()`、`createFileInput()` 等。製作一個按鈕所要使用的語法則是 `createButton()`。

首先，您需要為按鈕指派一個物件名稱，如果您要使用多個按鈕，則需提供多個不同的名稱，以便為各個按鈕設訂屬性[^Element]。

* `let button;`：第一步是透過指派名稱來宣告物件。
* `button = createButton('like');`：建立一個按鈕並思考您想要顯示在其上的文字。
* `button.style("xxx","xxxx");`: 這是 CSS 標準，其中第一個參數是選擇／選擇器，第二個則是宣告區塊／屬性。舉例來說，若要設定字體顏色，您可以分別輸入 "color" 和 "#fff"。[^Style] 在此範例程式碼中，所有樣式都是透過查看 2015 年 Facebook 介面的 CSS 原始碼，直接從該介面複製下來的。樣式包括 `display（顯示）`、`color（顏色）`、`padding（內距）`、`text-decoration（文字特效）`、`font-size（字型大小）`、`font-weight（字型粗細）`、`border-radius（邊框圓角）`、`border（邊框）`、`text-shadow（文字陰影）`、`background（背景）` 和 `filter（濾鏡效果）`，以及額外的 `transform（變形）`。
* `button.size();`：設定按鈕的寬度和高度。
* `button.position();`：設定按鈕的位置。
* `button.mousePressed(change);`：這會在使用者按下滑鼠時改變按鈕的顏色，並以自訂的函式 `change()` 來控制啟動音訊（更多內容將在後續的「音訊擷取」這一小節中介紹）。
* `button.mouseOut(revertStyle);`：當滑鼠游標離開按鈕元素時，將透過自訂函式 `revertStyle()` 讓按鈕恢復原始顏色。

## 滑鼠擷取

在上一章中，程式監聽了滑鼠游標的移動，並使用內建語法 `mouseX` 和 `mouseY` 擷取相應的 x 和 y 座標。此範例程式碼包含了特定的滑鼠監聽事件，例如每次使用者按下滑鼠按鈕時都會調用的 `mouseOut()` 和 `mousePressed()` 函式。請參閱以下程式碼的摘錄：

```javascript
//滑鼠擷取
button.mouseOut(revertStyle);
button.mousePressed(change);

function change() {
  button.style("background", "#2d3f74");
  userStartAudio();
}
function revertStyle(){
  button.style("background", "#4c69ba");
}
```
函式 `mousePressed()` 和 `mouseOut()` 會與您欲觸發操作的按鈕連結。其他與滑鼠相關的「滑鼠事件」（mouseEvents）[^event]還包括 `mouseClicked()`、`mouseReleased()`、`doubleClicked()`、`mouseMoved()` 等。

* * *

## 鍵盤擷取

```javascript
function keyPressed() {
  //空白鍵 – 請參閱：http://keycode.info/
  if (keyCode === 32) {
    button.style("transform", "rotate(180deg)");
  } else { //針對其他鍵碼（keycode）的動作
    button.style("transform", "rotate(0deg)");
  }
}
```

`keyPressed()` 是用來監聽任何鍵盤按下事件的函式。若想指定任何 `keyCode`（鍵碼，即鍵盤上的實際按鍵），範例程式碼展示了如何在 `keyPressed()` 函式中實作條件語句。

「條件結構」與您在上一章中學到的有些相似，但也與「if-else」語句有所不同。此處「if-else」的條件結構可以解釋為：如果按下鍵盤上的空白鍵，則按鈕旋轉180度，如果按下鍵盤上的任何其他按鍵，則按鈕恢復到原本的 0 度狀態。因此，「if-else」結構可讓您為監聽事件設定進一步的條件：如果偵測到 `keyCode` 並非空白鍵，程式將執行其他操作。

`keyCode` 也接受數字或特殊按鍵，如退格鍵（BACKSPACE）、刪除鍵（DELETE）、確認鍵（ENTER）、RETURN 鍵、製表鍵（TAB）、退出鍵（ESCAPE）、SHIFT 鍵、控制鍵（CONTROL）、OPTION 鍵、轉換鍵（ALT）、UP_ARROW 鍵、DOWN_ARROW 鍵、LEFT_ARROW 鍵、RIGHT_ARROW 鍵等。在上例中，空白鍵的 `keyCode` 為 32（見第 3 行）。

`keyCode` 不區分大小寫，例如，「A」和「a」的鍵碼都是 65。

與滑鼠事件（ `mouseEvents` ）相似，鍵盤事件（ `keyboardEvent` ）也還有很多其他的例子[^Key]，如 `keyReleased()`、`keyTyped()`、`keyIsDown()` 等。

* * *

## 音訊擷取

```javascript
let mic;

function setup() {
  button.mousePressed(change);
  // 音訊擷取
  mic = new p5.AudioIn();
  mic.start();
}

function draw() {
  //取得音訊資料，亦即整體音量（從 0 到 1.0 之間）
  let vol = mic.getLevel();
  /*將麥克風音量與按鈕大小對應，
  於此查閱 map 函式用法 https://p5js.org/reference/#/p5/map */
  button.size(floor(map(vol, 0, 1, 40, 450)));
}

function change() {
  userStartAudio();
}
```

範例程式碼中使用了基本的網路音訊 p5.sound 資料庫，其中包括音訊輸入、聲音檔案播放、音訊分析和合成等功能[^Audio]。

該資料庫應包含在 HTML 檔案中（如第一章〈入門〉所示），以便我們使用如 `p5.AudioIn()` 和 `getLevel()` 等相應的函式。

就像按鈕的例子一樣，您要先從宣告物件開始，例如 `let mic;` （請參閱程式碼第 1 行），然後設定輸入源（通常是電腦麥克風）並開始收聽音訊輸入（請參閱 `setup()` 中的第 6-7 行程式碼）。執行整個範例程式碼時，瀏覽器會彈出一個視窗，以請求存取音訊來源的權限。音訊擷取僅在授予存取權的狀態下才有效。

<div class="columns" markdown=1>
![](ch4_1.png)
:   *圖 4.2：允許音訊存取*

![](ch4_2.png)
:   *圖 4.3：允許相機存取*
</div>

範例程式碼參考了位於 p5.sound 資料庫中 `p5.AudioIn()` 之下的方法，透過 `getLevel()` 方法讀取輸入源的振幅（音量），回傳值位在 0.0 到 1.0 之間。

新函式 `map()`（第 15 行）將被引入，從而在一定範圍中映射數字。由於回傳的音量值範圍在 0.0 到 1.0 之間，因此相應的值並不會對按鈕的大小產生重大影響。音訊輸入的範圍將動態映射到按鈕的尺寸範圍。

函式 `userStartAudio()`（參見第 19 行）將使程式能夠擷取使用者互動事件中的麥克風輸入，本例中的互動式鍵為 `mousePressed()` 事件。這是許多網頁瀏覽器（包括 Chrome）執行的做法，使用者會知道背景中發生的音訊事件，並避免啟用網頁瀏覽器的自動播放或自動擷取功能。

## 視訊<span style="font-family: HersheyNoailles; font-style: italic; font-weight:bold; vertical-align: -6px; line-height: 0;">／</span>臉部擷取

```javascript
let ctracker;
let capture;

function setup() {
  createCanvas(640, 480);
  //網路攝影機擷取
  capture = createCapture(VIDEO);
  capture.size(640, 480);
  capture.hide();
  //設定臉部追蹤器
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);
}

function draw() {
  //使用影像濾鏡將擷取的影片繪製在螢幕上
  image(capture, 0,0, 640, 480);
  filter(INVERT);

  let positions = ctracker.getCurrentPosition();

  //查看網路攝影機追蹤功能是否可用
  if (positions.length) {
    //追蹤點 60 號是嘴部的區域
    button.position(positions[60][0]-20, positions[60][1]);

    /*依次通過並描繪臉部的每個重要的點
    （請見：https://www.auduno.com/clmtrackr/docs/reference.html）*/
    for (let i = 0; i < positions.length; i++) {
      noStroke();
      //利用 alpha 值（透明度）上色
      fill(map(positions[i][0], 0, width, 100, 255), 0, 0, 120);
      //在每個定位點繪製圓形
      ellipse(positions[i][0], positions[i][1], 5, 5);
    }
  }
}
```

針對此視訊/臉部擷取，範例程式碼使用了「clmtrackr」，這是由資料科學家奧頓・M・厄嘉德（Audun M. Øygard）於 2014 年開發的 JavaScript 資料庫，可用來將臉部模型與影像或視訊中的人臉對照[^Face]。該資料庫以傑森・薩拉吉（Jason Saragih）和西蒙・盧西（Simon Lucey）設計的臉部算法[^Algo]為基礎，根據預先訓練，用於分類的臉部影像機器視覺模型即時分析一張臉，並在其上標記了 71 個點（見圖4.5）。由於 clmtrackr 是一個 JavaScript 資料庫，您需要將該資料庫放在工作目錄之下，並把資料庫與 HTML 檔案中的人臉模型連結（見圖 4.4）。

![](ch4_10.png)
:   *圖 4.4：用來輸入新的資料庫和模型的 HTML 檔案結構*

![](ch4_3.png){: .medium .no-border}
:   *圖 4.5：臉上的追蹤點。圖片由 clmtrackr 的創造者奧頓・M・厄嘉德先生提供*

透過視訊擷取，這個程式使用網路攝影機來進行臉部辨識，詳細資訊如下：

1. `let ctracker;` & `let capture;`：初始化用於臉部追蹤和視訊擷取的兩個變數。

2. 第 7 行中的 `createCapture(VIDEO)`：這是一個 HTML5 `<video>` 元素（DOM 的一部分），可用以從網路攝影機擷取饋送資料。有關此功能，您可以定義螢幕擷取的大小（取決於網路攝影機的解析度）和螢幕上的位置，例如 `capture.size(640, 480);` 我們也使用 `capture.hide();` 來隱藏視訊來源，確保按紐和彩色追蹤點不會使 video 物件崩壞。

3. 與 ctracker 相關的第 11-13 行 `ctracker = new clm.tracker()`，`ctracker.init(pModel);` 以及 `ctracker.start(capture.elt);`：與音訊和攝影機的使用類似，您要先初始化 ctracker 資料庫，選擇分類模型（將在第十章〈機器反學習〉中討論），並從視訊來源開始追蹤。

4. 為了在 INVERT 模式下顯示擷取的視訊，這個程式使用了 `image(capture, 0,0, 640, 480);`，從而以影像格式繪製視訊來源，並相應使用濾鏡：`filter(INVERT);`（見第 18-19 行）。

5. 第 21 行中的 `ctracker.getPosition()`：將追蹤點放入「`position`」（位置）陣列的同時，我們會使用 for loop（第 30-36 行）來依次通過並描繪所有 71 個追蹤點（編號從 0 開始，直到 70），並以 `position[][]` 的形式，將 x 和 y 座標作為二維陣列回傳位置。位置陣列的第一維度 ([]) 表示從編號 0 到 70 的追蹤點。第二維度 ([][]) 則會檢索追蹤點的 x 和 y 座標。

6. 取得追蹤點上的所有資料後，便可繪製圓形以涵蓋臉部區域。按讚按鈕的位置會跟隨嘴巴的位置移動（位置為編號 60 的追蹤點，但由於按鈕須定位在嘴巴的中點，因此要將按鈕向左移動約 20 像素），程式將以陣列形式回傳位置（參見第 26 行）：`positions[60][0]-20` 和 `positions[60][1]`。第二個陣列的 [0] 和 [1] 維度，便是指 x 和 y 座標。

<div class="section exercise" markdown=1>

## 課堂練習

欲熟悉各種資料擷取模式，請試試以下操作：

1. 透過修改各種參數（例如 `keyCode` ）以及其他鍵盤和滑鼠事件，來探索各種擷取模式。
2. 研究追蹤點，並嘗試改變按讚按鈕的位置。
3. 試著測試臉部辨識的極限（改變燈光、表情和臉部構成）。一張臉的可辨識範圍為何？超過哪些極限會變成無法辨識呢？
4. 您知道人臉是怎麼建模的嗎？臉部辨識技術在社會上的應用情況如何？此技術引發了哪些問題？

在此相當適合回顧一下第二章〈變數幾何〉，該章節提醒了我們，臉部辨識技術如何根據幾何形狀（例如眼距或嘴巴的大小）辨識人臉，從而建立可與標準化資料庫進行比較的臉部特徵。一個主要的問題，是這些資料庫會因資料的準備、選擇、收集、分類（categorization）、分屬（classification）和清理方式而有所偏差（這點將在第十章〈機器反學習〉中進一步討論）。您的臉又在多大程度上符合標準呢？

</div>

## 擷取的概念

下一節將討論資料擷取中所使用的各種不同輸入的示例，這麼做的目的，除了展示資料擷取的其他可能應用外，更重要的是展現它與資料化、商品化、監控和個人化之間的關係。換句話說，這是一個更廣泛地討論資料政治的機會，我們在此可以對個人資料是如何被擷取、量化、存檔和使用提出質疑，並詢問以下事項：這些資料被用在什麼樣的目的上？這代表著什麼？誰有權存取擷取的資料並從中獲利？很少有人確切知道遭擷取的資料有哪些，或如何被使用嗎[^GDPR]？然而，儘管使用了英文中與「俘虜」意義相似的「擷取」（capture）一詞，我們也應該點出，資料在擷取時並不會被完全「禁錮」，還是有辦法可以逃過一劫的，我們稍後會再詳細討論這點。

### 網站分析和熱點圖

目前，使用最廣泛的網站分析服務是由 Google 提供，囊括大量關於網站流量和瀏覽行為的資料，包括不重複訪客訪問次數、網站平均停留時間、瀏覽器和作業系統資訊、流量來源和使用者的地理位置等。我們可以進一步利用這些資料來分析客戶的個人資料和使用者行為。

![](ch4_4.png){: .medium}
:   *圖 4.6：Google Analytics 螢幕截圖*

熱點圖是一種視覺化工具，將資料以圖形表示，從而視覺化使用者行為，通常於產業中用以進行資料分析。舉例來說，此工具可以輕易追蹤游標的位置，並計算其在網頁不同區域停留的時間，從而指出哪些內容比其他內容「熱度更高」。這在行銷方面非常實用，尤其可用以了解哪些內容能夠吸引使用者，對於公司或政黨而言，此工具更可以分析出哪裡是投放廣告和其他宣傳材料最理想的地點。相關案例研究，可參考 Facebook–劍橋分析資料醜聞。這項醜聞爆發於 2018 年初，據消息透露，劍橋分析公司在未經用戶本人同意的情況下，獲取了數百萬人的 Facebook 個人檔案資料，並將之用於政治廣告[^Analytica]。而 Facebook 等大公司[^Facebook]仍一直不斷地探索新的資料擷取方法，以優化螢幕呈現方式。

![](ch4_7.png){: .medium}
:   *圖 4.7：以熱點圖分析某網站的示例*


### 表單元素 {: style="margin-top: -16px;"}

與我們在互動方面所提出的論點相同，我們認為表單中的選擇是有限的，不過，每個表單元素（如下拉選單或按鈕）都展示了不同的功能特性[^gibson]。研究員麗娜・比文斯（Rena Bivens）就可選的性別選項對 Facebook 的註冊頁面進行了全面分析[^Bivens]。當 Facebook 於 2004 年首次推出時，並沒有設定性別欄位，但到了 2008 年，卻引進了一個僅包含男性或女性選項的下拉選單列表，此外更進一步改變了選項按鈕的使用方式，更加強調了這個二元性的選擇。2014 年情況出現了突破，Facebook 開始讓用戶自訂性別欄位，因此，您現在已有五十多個性別選項可供選擇。Facebook 宣稱，他們希望透過以「真實身分」參與來加強「個人化體驗」[^Facebook2]，然而，這種個人化（無論是在 Facebook 還是整個社會的脈絡之下）也有助於進行市場區隔（將用戶分成更多的子群組），因此，打造這樣的個人化體驗是否出於善意仍存有爭議。

![](ch4_8.png){: .medium style="height: calc(10 * var(--line-height))"}
:   *圖 4.8：Facebook 可自訂的性別欄位（20202 年 2 月）*

### 按讚的指標 {: style="margin-top: -16px;"}

僅僅是一個按讚按鈕的使用，便提供了很好的例子，說明我們的感受是如何被擷取的。有一家名為「Happy or Not」的公司，專門生產按鈕技術和分析軟體，例如在超市中可以看到的那種，上面有快樂或悲傷的面孔可供選擇的機器，「Happy or Not」的名稱可謂名副其實，此外，該公司也在工作場所提供回饋技術，正如他們的口號所示：「為世界上每一家企業創造幸福」[^Leslie]。Facebook 於 2016 年推出包括「讚」、「大心」、「哈」、「哇」、「嗚」和「怒」等六種表情符號，更準確地標誌了人類標準化的工作和娛樂體驗。所有點擊都被「歸類」為情緒指標，在網路上公開顯示，並使用於演算法計算，從而決定要向用戶優先推送哪些資訊。顯然，「點擊」這一資料最重要的任務，便是為平台所有獲取利益，此外，就像是為了證明上述這點一般，Facebook 和 Instagram 已經試行了隱藏貼文指標的想法，以便將注意力轉移到他們更喜歡稱之為「連結人們」[^Hide]的目標上，一副想證明他們的出發點完全無關私利，而是利他似的。

藝術家班傑明・格羅瑟（Benjamin Grosser）在他於 2012 年首次出版的《指標去除器》（Demetricator）系列作品中，戲謔地模仿了上述作法[^Grosser]，在該作品中，所有與後設資料相關的數字都消失了。與通知、回覆、最愛和動態消息相關的「數值」都被抹除。或者，我們可以說，點擊次數確實能夠產生了價值，而這一點在「點擊次數」的數值消失後，反而變得更為顯著。

![](ch4_9.gif){: .medium}
:   *圖 4.9：班傑明・格羅瑟的作品〈臉書指標去除器〉，其中，按讚、分享、回覆和發文時間等資訊的指標都遭到消除。上為原版貼文，下為指標消除後的貼文。圖片由藝術家本人提供*

追蹤顯然是一件大事，但它卻能夠躲在屬於自己的隱形斗篷後面偷偷進行。2013 年，Facebook 進行了一項關於最後一刻自我審查的研究項目[^Facebook3]，透露了他們甚至有辦法追蹤到未發布的狀態更新／貼文／回覆（包括已刪除的文字或圖像）。這類「殘餘資料」可能被認為是「廢料」、「數位廢氣」或「資料廢氣」，但這些資料其實卻具備豐富的預測價值[^zuboff2]。這表示 Facebook 不僅對擷取您發布的內容感興趣，更希望能從殘餘資料中擷取您的思維過程。資料擷取的範圍竟會擴展到想像的領域，這個想法實在令人警醒。

### 語音和音訊資料

我們的電腦、手機和其他小工具等智慧型裝置通常配備語音辨識功能，如 Siri、Google 助理 或 Alexa 等，以將音訊輸入轉換為針對軟體的指令，並提供更個人化的回饋體驗，以協助我們執行日常任務。如今，幾乎所有事物，包括微波爐等日常用品上，都有這些語音助理的蹤跡，而隨著機器學習的發展，它們的對話能力逐漸加強，也變得越來越「聰明」，或說「智慧化」。眾所周知，這些「語音助理」可以將簡單的任務執行得有條不紊，較以前更聰明，同時，它們通常也都能為機器學習應用程式擷取語音。在家中放置有形的語音助理，您就算不面對螢幕，裝置仍可擷取您的選擇和品味資料。在物聯網中，裝置會為您服務，但同時您也在為裝置服務，在此，我們自己實際上也成了為他人創造價值的「裝置」[^Leslie2]。

![](ch4_6.png){: .medium}
:   *圖 4.10：語音和音訊活動螢幕截圖*

圖 4.10 中的文字顯示：

> 「語音和音訊錄音會保存您的語音錄音和其他音訊輸入，這些音訊資料來自您在 Google 服務上的網頁和應用程式活動，以及使用或連接到 Goolge 語音服務的網站、應用程式和裝置的活動。[⋯⋯] 這些資料有助於讓 Google 為您提供更個人化的 Google 服務體驗，例如，無論您是否在 Google 服務範圍內，只要您說聲『嘿 Google』並與您的智慧助理交談，便可同時透過這些互動改善語音辨識功能，這些資料可能會在您登錄的任何 Google 服務中保存和使用，從而為您帶來更加個人化的體驗。」

* * *

### 健康追蹤器

![](ch4_5.png){: .float}
:   *圖 4.11：睡眠追蹤器螢幕截圖*

人類的健身和身心健康也被資料化，而隨著個人目標的設定，更趨向「遊戲化」。隨著福利國家的瓦解，個人幸福感變得越來越依每個人的特質而定，「自我追蹤」的應用程式蔚然成風，提供一種虛假的自主性。只要擁有 Fitbit 或 Apple Watch 等可穿戴式裝置，就能分析運動、步數、心率，甚至睡眠模式等資料。這類「量化自我」行動有時會被稱為「身體駭客」或「自我監控」，與之相似的其他許多趨勢，也會讓擷取及獲取資料的行為深入到日常生活各個層面。


## While()

在晚期資本主義下，時間本身似乎也已經被擷取，在此引述喬納森・克拉里（Jonathan Crary）在其著作《24/7：晚期資本主義與睡眠的終結》（Late Capitalism and the Ends of Sleep）中說的一段話：「7 天 24 小時這樣的『非時間』無情地對社會或個人生活的各個層面侵門踏戶。舉例來說，到了現在，幾乎沒有任何情況，無法以數位圖像或資訊的形式進行記錄或存檔」[^Crary]。這本書描述了白天和黑夜之間界線的崩潰，這表示我們注定要隨時產生資料。睡眠曾被認為是無價值可供提取的地方，是唯一能夠擺脫資本主義的最後避難所[^Crary2]，然而，情況似乎不再是如此了。

連睡眠也被資料化這件事，似乎彰顯出我們的主觀性被擷取到了何種程度。我們生產、共享、收集、使用和濫用（無論是否有意）大量資料，但擷取這些資料對我們有何影響？資料商品與其人類主體之間，有著什麼樣的主體間關係？正如本章所討論的主題所示，我們的個人和工作生活似乎完全陷入了各種「資料化」的過程，但這是否表示我們已被困在資料監獄之中，為他人創造價值而無所知覺？在本章最後一節，我們嘗試更深入地對這些想法抽絲剝繭，特別是資料流（我們稱之為大數據）背景下的「價值」這個概念，並檢視我們自己在這些並非全無能動性的資料化結構中的定位。

2015 年，柏林一年一度的藝術和數位文化節「transmediale」發布公開徵集，聚焦關於充斥各處的「全面捕捉」（Capture All）邏輯，以及生活、工作和娛樂的量化之藝術作品。此次徵集提出了一些值得在這裡再次討論的問題：「是否仍有某些存在模式，能夠抵抗數位資本主義專橫的「全數擷取」作法？還是說，除了遵循數位量化的規則之外別無選擇？如果是這樣，是否有一些藝術策略和理論性的方式，能夠跳脫出這種數位量化遊戲？在無休無止的量化和遊戲化中，有哪些可以見縫插針的地方，能用以開拓新的風活方式[^transmediale]？」我們希望，本章的實作任務和範例能夠在某種程度上引導出一些替代方案。

馬克思主義的理論，可以協助我們在概念層面上的理解。我們可以將此處所描述的各種技術，全數理解為生產方式，也就是馬克思所謂的「固定資本」，這些資本之後會轉化為「交換價值」，也就是貨幣價值。然而，正如蒂齊亞納・泰拉諾瓦（Tiziana Terranova）所言，若僅將這個過程視為用戶勞動價值的擷取與其相關價值的竊取，其實並沒有抓住重點[^Terranova]。對此，我們或許會補充，個人用戶的確要獲得補償，才會自願提供資料，但相較之下，更廣泛的社會方面，尤其是在大數據的背景下，才是真正的要點所在。泰拉諾瓦解釋：「馬克思主義的一些變體傾向於將技術完全等同於『死勞動』、『固定資本』或『工具理性』，因此是控制和擷取的同義詞，但這裡的論點與之相反，重要的是，我們必須謹記，對於馬克思來說，機器的進化也反映了生產力的發展水準，但這些釋出的生產力從未完全被資本主義經濟所控制[^Terranova2]。」

我們可以在自由開源運動的社會能量中找到這方面的一些證據，例如，其中的補償是在社會交換的層面上運作的。這種說法有助於將注意力從個體的努力轉移到社會關係上。如果我們要制定一種與「全面捕捉」邏輯不同的立場，並尋求更積極、更樂觀的解釋，這種政治是尤其重要的。泰拉諾瓦在談到按鈕操作時，將社會關係描述為兩極之間的不對等關係，其中一極是主動的，另一極是被動的。對她而言，「按讚和被按讚、寫作和閱讀、注視和被注視、標記和被標記」等行為是從個體到集體形式的過渡的例子。她思考「這些行為如何變成離散的技術對象（如按鈕、評論框、標籤等），然後與底層數據結構相連」，以及這些行為如何表達能夠實驗「個體化」和「跨個體化」過程的可能性，也就是社會變革本身的可能性。

這一論點參考了吉爾伯特·斯蒙頓（Gilbert Simondon）的哲學，關於一項人或事物的個體化（賴以分辨一個人或事物之與他人不同的獨特之處）轉變過程，如何與其他人或事物的個體化產生關聯。篇幅有限，本書並未對此深入探討，我們也認為無此必要，但就目前而言，我們只需要知道，跨個體化（transindividuation）一詞描述了個體的「我」和集體的「我們」之間的過渡，以及兩者如何在彼此的作用下互相轉化[^Stiegler]。我們希望這次專書計畫能有這樣的發展，畢竟它原本就是為了集體參與而設計的，並且有望為新版本的製作和重複修改過程中的社會關係開闢更多可能性。當然，這涉及操弄與資料擷取相關的底層程式碼和值，以及我們對資料擷取之主要目的進行重塑的能力。這是一個對所有人都敞開大門的邀請，但這不僅是開放人們擷取資料，更是廣邀大眾來釋放資料的其他潛力。

<div class="section exercise" markdown=1>

## 迷你習作：全面捕捉

**目標：**

* 對包括音訊、滑鼠、鍵盤、網路攝影機等各種資料擷取輸入進行實驗。
* 批判性地反思資料擷取和資料化的過程。

**更多靈感：**

* *[LAUREN](http://lauren-mccarthy.com/LAUREN)* by Lauren McCarthy (2017), http://lauren-mccarthy.com/LAUREN
* *[nonsense](http://siusoon.net/nonsense)* by Winnie Soon (2015), http://siusoon.net/nonsense （請閱讀原始碼中的註解，以了解這項計畫欲達成的目標）
* *[Facebook Demetricator](https://bengrosser.com/projects/facebook-demetricator)* by Benjamin Grosser (2012-present), and subsequent *[Instagram Demetricator](https://bengrosser.com/projects/instagram-demetricator)*, or *[Twitter Demetricator](https://bengrosser.com/projects/twitter-demetricator)*.

**任務（RunME）：**

1. 請嘗試各種資料擷取獲輸入和互動設備，例如音訊、滑鼠、鍵盤、網路攝影機／視訊等等。
2. 請繪製一個可以大致回應 transmediale 的「全面捕捉」公開徵集的草稿碼 <https://transmediale.de/content/call-for-works-2015>，請設想您是要將自己創作的草圖／藝術作品／批判或推測性設計作品提交給 transmediale 參展。

**在您的 ReadMe 檔案中可供思考的問題：**

* **請提供**作品的標題和簡略描述（少於 1000 個字符），就如同要將之提交給藝術節來參展一樣。
* **請描述**您的程式，以及您使用和學習的內容。
* **請明確解釋**您的計畫和想法會如何處理「全面捕捉」這一主題。
* **資料擷取**的文化含義為何？
</div>

## 指定讀物

* Shoshana Zuboff, "Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary," <https://youtu.be/hIXhnWUmMvw>.
* "p5.js examples - Interactivity 1," <https://p5js.org/examples/hello-p5-interactivity-1.html>.
* "p5.js examples - Interactivity 2," <https://p5js.org/examples/hello-p5-interactivity-2.html>.
* "p5 DOM reference," <https://p5js.org/reference/#group-DOM>.
* Ulises A. Mejias and Nick Couldry, "Datafication," *Internet Policy Review* 8.4 (2019), <https://policyreview.info/concepts/datafication>.

## 延伸讀物

*	Søren Pold, "Button," in Fuller, ed., *Software Studies*.
* Carolin Gerlitz and Anne Helmond, "The Like Economy: Social Buttons and the Data-Intensive Web," *New Media & Society* 15, no. 8, December 1 (2013): 1348–65.
*	Christian Ulrik Andersen and Geoff Cox, eds., *A Peer-Reviewed Journal About Datafied Research* 4, no. 1 (2015), <https://aprja.net//issue/view/8402>.
* Audun M. Øygard, "clmtrackr - Face tracking JavaScript library," <https://github.com/auduno/clmtrackr>.
*	Daniel Shiffman, *HTML / CSS/DOM - p5.js Tutorial* (2017), <https://www.youtube.com/playlist?list=PLRqwX-V7Uu6bI1SlcCRfLH79HZrFAtBvX>.
*	Tiziana Terranova, "Red Stack Attack! Algorithms, Capital and the Automation of the Common," *EuroNomade* (2014), <http://www.euronomade.info/?p=2268>.

## 註釋

[^Tufte]: 這點呼應了資料視覺化的領域，而愛德華·塔夫特（Edward Tufte）認為，資料應該要「不言而喻，而非包裹在視覺化的裝飾中而失焦」。這樣的想法犯了一個錯誤，便是認定資料是原始且未經媒介轉達的。一開始，資料確實相對原始且未經詮釋，但在實際運作的過程中，這些資料已經過挑選、具針對性，且歷經預先處理、清理和探勘等，此外亦已轉為便於讓人類閱讀的形式。關於資料的組成，總是會有一些的額外資訊，而這些資訊通常是來自最初收集資料的方式。請參閱 Edward R. Tufte, *The Visual Display of Quantitative Information* [1983] (Cheshire, CT: Graphics Press, 2001)。

[^datafication]: Christian Ulrik Andersen and Geoff Cox, eds., [A Peer-Reviewed Journal About Datafied Research](https://tidsskrift.dk/APRJA/issue/view/8402), *APRJA* 4, no.1 (2015).

[^Bigdata]: Kenneth Cukier and Victor Mayer-Schöenberger, "The Rise of Big Data," *Foreign Affairs* (May/June 2013): 28–40.

[^zuboff]: Shoshana Zuboff, "Shoshana Zuboff on Surveillance Capitalism | VPRO Documentry," vpro documentary. Accessed April 26 (2020). <https://youtu.be/hIXhnWUmMvw>。請參閱她的著作 *The Age of Surveillance Capitalism: The Fight for a Human Future at the New Frontier of Power* (New York: PublicAffairs, 2019)。

[^Pold]: Søren Pold, "Button," in Matthew Fuller ed., *Software Studies* (Cambridge, Mass.: MIT Press, 2008), 34. 用戶尤其會被按鈕上的文字吸引，而波爾德（Pold）提出，按鈕在開發時，便設計了獨特的功能性和意義（Ibid., 31）。

[^helmond]: Carolin Gerlitz and Anne Helmond, "The Like Economy: Social Buttons and the Data-Intensive Web," *New Media & Society* 15, no.8, December 1 (2013): 1348–65.

[^fb]: 此按鈕的採用與 Facebook 2015 年的按讚按鈕完全相同的設計。

[^DOM]: <https://p5js.org/reference/#group-DOM>.

[^Element]: 請至下列網址查閱 `p5.Element` 方法清單：<https://p5js.org/reference/#/p5.Element>。

[^Style]: 按鈕設計遵循 CSS 語法，可藉此控制如按鈕等 DOM 元素的顯示方式。此處提供的示例展示如何透過使用 `button.style('xxx:xxxx');` 語法，將 CSS 納入 JavaScript 檔案中。另外一種做法，是依照慣例，建立一個上面列有 `.class` 選擇器的 CSS 檔案。這種做法會需要在 JavaScript 檔案中包含上述語法，以便標記類別名稱：`button.class('class_name');`，隨後再於 CSS 檔案中列出 CSS 元素和類別屬性。更多示例請參閱以下網址：<https://www.w3schools.com/csS/css3_buttons.asp,>，此外亦可參考丹尼爾・席夫曼（Daniel Shiffman）的基礎 CSS 介紹影片：<https://www.youtube.com/watch?v=zGL8q8iQSQw>。

[^event]: 相關函式列於參考頁面的 Events > Mouse> 之下，請參閱 <https://p5js.org/reference/>。

[^Key]: 相關函式列於參考頁面的 Events > Keyboard>之下，請參閱 <https://p5js.org/reference/>。

[^Audio]: 有關聲音資料庫的多種功能，請參閱：<https://p5js.org/reference/#/libraries/p5.sound>。

[^Face]: 請見：<https://www.auduno.com/2014/01/05/fitting-faces/>。

[^Algo]: Jason M. Saragih, Simon Lucey and Jeffrey F. Cohn, "Face Alignment Through Subspace Constrained Mean-shifts," *2009 IEEE 12th International Conference on Computer Vision, Kyoto* (2009): 1034-1041. doi: 10.1109/ICCV.2009.5459377.

[^GDPR]: GDPR（一般資料保護規則）等法律的訂立，便是對這類透明度缺乏的問題的回應。GDPR 是歐盟法（2016）中關於資料保護和隱私的法規，適用於歐盟和歐洲經濟區的所有公民。此規則亦處理發生在歐盟和歐洲經濟區之外，個人資料傳輸的問題。請參閱 <https://gdpr-info.eu/>。

[^Analytica]: 可至 <https://www.theguardian.com/news/series/cambridge-analytica-files> 閱讀《衛報》對此事件的相關報導〈劍橋分析公司檔案〉（The Cambridge Analytica Files）。為此，Facebook 最後被迫支付高額罰金，相關報導請見 Alex Hern, "Facebook agrees to pay fine over Cambridge Analytica scandal," *The Guardian*, October 30 (2019), <https://www.theguardian.com/technology/2019/oct/30/facebook-agrees-to-pay-fine-over-cambridge-analytica-scandal>。

[^Facebook]: Will Conley, "Facebook investigates tracking users’ cursors and screen behavior," *Slashgear*, October 30 (2013). Available at: <https://www.slashgear.com/facebook-investigates-tracking-users-cursors-and-screen-behavior-30303663/>.

[^gibson]: 從功能特性中，可以看到針對用戶如何與某事物互動的蛛絲馬跡。請見 James J. Gibson, The Theory of Affordances," in Robert Shaw and John Bransford, eds. *Perceiving, Acting, and Knowing* (Hillsdale, NJ: Lawrence Erlbaum Associates, 1977), 127–143。

[^Bivens]: Rena Bivens, "The Gender Binary will not be Deprogrammed: Ten Years of Coding Gender on Facebook," *New Media & Society* 19, no.6, (2017): 880–898. doi.org/10.1177/1461444815621527.

[^Facebook2]: Facebook, Form S-1 表格註冊聲明 (2012)。網址：<https://infodocket.files.wordpress.com/2012/02/facebook_s1-copy.pdf>。

[^Leslie]: Esther Leslie, "The Other Atmosphere: Against Human Resources, Emoji, and Devices," *Journal of Visual Culture* 18 no.1, April (2019).

[^Hide]: Laurie Clarke, "Why hiding likes won’t make Instagram a happier place to be," *Wired*, July 19 (2019), <https://www.wired.co.uk/article/instagram-hides-likes>.  

[^Grosser]: 參見班・格羅瑟（Ben Grosser）的《指標去除器》系列作品：*Facebook Demetricator*, <https://bengrosser.com/projects/facebook-demetricator/>; *Instagram Demetricator*, <https://bengrosser.com/projects/instagram-demetricator/>; *Twitter Demetricator*, <https://bengrosser.com/projects/twitter-demetricator/>。

[^Facebook3]: Sauvik Das and Adam D. I. Kramer, "Self-censorship on Facebook," *AAAI Conference on Weblogs and Social Media (ICWSM)*, July 2 (2013), <https://research.fb.com/publications/self-censorship-on-facebook/>.

[^zuboff2]: Zuboff, *Shoshana Zuboff on Surveillance Capitalism | VPRO Documentry*.

[^Leslie2]: 此處以我們自己的方式重述埃斯特·萊斯里（Esther Leslie）的文章〈另一種氛圍：反人力資源、表情符號和裝置〉（The Other Atmosphere: Against Human Resources, Emoji, and Devices）的最後幾行：「工作者成了自己的裝置，化為溝通資本主義的工具⋯⋯。」

[^Crary]: Jonathan Crary, *24/7: Late Capitalism and the Ends of Sleep* (London: Verso, 2013), 30–31.

[^Crary2]: Crary, *24/7*, 10-11.

[^transmediale]: transmediale, *Capture All*, <https://transmediale.de/content/call-for-works-2015>.

[^Terranova]: Tiziana Terranova, "Red Stack Attack! Algorithms, Capital and the Automation of the Common," *EuroNomade* (2014). Available at <http://www.euronomade.info/?p=2268>

[^Terranova2]: Terranova, "Red Stack Attack!"

[^Stiegler]: 艾莉特·羅格夫（Irit Rogoff）對貝爾納·斯蒂格勒（Bernard Stiegler）解釋：「『跨個體化』概念之根基，並非個體化的『我』或相互個體化的『我們』」，而是在於「在預先個體化的環境中共同個體化的過程，『我』和『我們』便在其中互相轉化。」請見 Bernard Stiegler and Irit Rogoff, "Transindividuation," *e-flux* 14, March (2010), <https://www.e-flux.com/journal/14/61314/transindividuation/>。

[^Terranova3]: Terranova, "Red Stack Attack!"
