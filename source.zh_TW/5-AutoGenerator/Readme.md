Title: 第五章：自動產生器
Slug: 5-auto-generator
page_order: 5


![flowchart](ch5_0.svg)

[TOC]

## setup()

上一章討論了潛藏在與輸入裝置互動過程之下的資料擷取，本章則以輸入和輸出的觀念為基礎，介紹所謂的抽像機器。抽象機器又稱圖靈機，基本概念是由自主運作的機器進行規則建立。首開圖靈機概念的學者，是數學家兼電腦科學家艾倫・圖靈（Alan Turing），關於此種機器最早的描述，正是收錄在他於 1936 年發表的著名文章〈論可計算數及其在判定問題上的應用〉中[^Turing]。圖靈以「通用計算機器」一詞概括其理論，描述機器為何「可用於計算任何可計算的序列」[^Turing1]，亦即機器如何運行，並遵循預定的指令序列來處理輸入和產生輸出。

更具體來說，圖靈機可以進行六種基本操作（當時「電腦」這種東西尚未出現），包括讀取、寫入、左移、右移、改變狀態和暫停／終止。圖靈提出，這些操作可以透過一個無限循環的磁帶（類似於現代電腦中的記憶體）來執行，而磁帶上包含各項指令，如要讀取和寫入哪些符號，以及如何左右移動等等。這些指令構成了圖靈機[^visualization]，以及現代運算的基本原理，並具有運算數字任務和自動化各種過程的功能。這些源自基礎運算的指令，似乎支持了當代（資訊）資本主義中更廣泛的生產、消費和分配過程，這點我們在上一章中已有稍微討論。

![turing](ch5_1.png){: .medium style="padding: 1.5em;" }
:   *圖 5.1：圖靈機示意圖[^TuringGraph]*

本章將探討指令成為自動適應系統的基本要素之過程，並著重介紹規則的執行方式，以及它們如何產生出意想不到且／或複雜的結果。

不只有程式設計師會關注規則和指令，其實，在遵循圖樣進行針織／編織[^laczko]，或跟著食譜做菜時，您也會做出類似的事情（我們將在下一章提到豆腐的準備過程，亦是如此）。不少藝術家也創作了以指令為基礎的作品，例如 1960 年代和 1970 年代的激浪派（Fluxus）和概念藝術運動，便把目標放在挑戰藝術的客觀性，以及鼓勵藝術的「非物質化」上[^Lippard]。許多評論家將這些基於指令的作品與運算藝術進行連結[^Cox]，舉例來說，克里斯蒂安・保羅（Christiane Paul）於 2018 至 2019 年在惠特尼美國藝術博物館統籌策劃之調查展覽《程式化：藝術中的規則、程式碼和編排，1965-2018》（Programmed: Rules, Codes, and Choreographies in Art, 1965-2018）[^exhibition]，便探討了以指令為基礎的實務作法如何既對技術提出回應，又同時被技術所形塑。試舉一例，概念藝術家索爾・勒維特（Sol Le Witt）的作品便可以明顯看出，即使基於同一組指令，結果也可能會因其他人對指令的解釋方式而異，例如他的作品《牆畫 #289》（*Wall Drawing #289*）便是由三個簡單明瞭，卻沒有指定線條角度和長度的指令所組成：

1. 從中央向外畫出二十四條線。
2. 從四邊的中心點各畫出十二條線。
3. 從四角各畫出十二條線。

![drawing2](ch5_2.png){: .medium}
:   *圖 5.2：此圖像是索爾・勒維特作品《牆畫 #289》（1976）的軟體版本，並經查克・格里米特（Chuck Grimmett）進一步編程[^289]*

正如勒維特所解釋的那樣：「想法變成了創造藝術的機器[^LeWitt]。」受到這個使用程式語言「Processing」[^processing]的作品所吸引，卡西・瑞斯（Casey Reas）根據這些指令為勒維特的牆畫進行算圖，從而一一探索這些指令的解釋和過程的相似之處[^Reas]。在此作品隨附的文字解釋中，瑞斯指出了一個重要的區分，即勒維特的程式應該是由人，而不是由機器來執行。不過，瑞斯希望寫程式能像繪圖一樣即時而流暢，因此真正讓他感興趣的，正是這種緊密的連結和重疊，而以此為基礎，Processing 亦作為「軟體素描本」逐漸發展茁壯。

上述範例並非首開先河之作，此前，瓊・特魯肯布羅德（Joan Truckenbrod）等藝術家便已試著將演算法繪圖應用於創作，比如說，特魯肯布羅德的《編程演算法繪圖》（Coded Algorithmic Drawings）系列作品，便可以追溯到 1970 和 80 年代[^joan0]。其中，1975 年完成的作品《熵之糾纏》（*Entropic Tangle*，見圖 5.3）是使用帶有打孔機和磁性儲存媒介的大型計算機，以 Fortran 程式語言編寫而成。作品上有幾個大小不同的多邊形，其旋轉角度模擬了無形自然力，並利用變數和數學調變融入連續性和波動等特質。特魯肯布羅德對「自然」力如何以符號和數字進行重新詮釋十分感興趣，而這些符號和數字又進一步展現了系統中本質的模糊性和自發性[^joan1]。以運算的自我組織特性和隨時間改變的自主性代理[^tree]為基礎之遞迴式碎形幾何[^flock]和群集行為（flocking behaviors）[^agent]，可說是能夠展示「熵」之性質（缺乏秩序或可預測性）的幾個例子。

![drawing3](ch5_8.jpg){: .medium}
:   *圖 5.3：瓊・特魯肯布羅德，《熵之糾纏》（1975）。圖片由藝術家本人提供*

上述這種表現方式非常重要，它不只提供了一種以數學邏輯為基礎，進行機器繪圖和作品創作的不同方式，更帶來一種否定意象性的「機器創造力」（如前一章所述）[^Arns]，同時挑戰了人類（通常為男性主義式）能動性的中心地位。而本章的宗旨，便是探索人類與機器、非人類或動物（螞蟻）之間更複雜的組合或種間關係[^Haraway]。

如果我們要在座標 (100, 120) 處（前為 x 座標，後為 y 座標）繪製一個白色圓形，這個指令的結果是可預測的。但情況並不一定是如此，這是因為某些類型的指令或指令組合，可能會產生難以控制的結果。如《10 PRINT CHR$(205.5+RND(1)); : GOTO 10》一書中所述，「10 Print」這個程式會利用隨機性來產生不可預測，對人類來說看似隨機的過程和結果。這種「產生」的能力，對人們究竟能在多大程度上控制創作過程提出了疑問，以下對「生成藝術」一詞的定義，便說明了這一點：

>「生成藝術是指 [原文如此] 藝術家使用諸如一組自然語言、規則、電腦程式、機器或其他程序式發明等具有某種程度自主性的系統，所進行之任何藝術實踐，而這樣的系統自主性對藝術作品有所貢獻，或者說，作品正是仰賴此種自主性而得以完成[^galanter]。」

值得注意的是，這個定義並未限制作品只能使用電腦，在我們繼續往下閱讀，開始聚焦範例程式碼時，請務必將這一點牢記在心。以下兩個示例[^shiffman]探討了基於規則的程式，而這些程式可用以處理其中一些自動產生的相關問題，但與前面的示例相同，我們感興趣的對象是更廣泛的含義，其中包括隱形勞動，以及其他有關自主性的議題。

## start()

第一個程式叫做「10 PRINT」，指的是 `10 PRINT CHR$(205.5+RND(1));: GOTO 10` 這一行程式碼，該程式以 BASIC 程式語言編寫，並於康懋達 64（Commodore 64，一種於 1989 年代流行的家用電腦）上執行。該程式會在螢幕上產生出一個永無止盡的圖樣。這行程式碼被印在 1982 年的《康懋達 64 使用指南》之上，後來於線上出版，成為軟體研究領域中用來展示創意運算歷史和文化的重要範例[^10print]。下文 p5.js 中的 10 PRINT 展現系統內部具備一定程度的自主性，將可協助我們熟悉該程式的規則和創意潛力。

第二個程式名為「蘭頓螞蟻」（*Langton's Ant*, 1986），這個程式是電腦科學家克里斯托弗・蘭頓（Christopher Langton）於 1986 年發明的二維通用圖靈機，他也被公認為人工生命領域的奠基人之一[^Langton]。蘭頓螞蟻與 10 PRINT 最核心的區別，在於這個程式僅使用一組簡單的規則，便能產生複雜的演生（emergent）行為。

* * *
<div class="section exercise" markdown=1>

## 課堂練習（10 PRINT）

![drawing4](ch5_4.png)
:   *圖 5.4：p5.js 中的 10 PRINT*

<sketch
  src="p5_SampleCode.zh_TW/ch5_AutoGenerator/sketch.js"
  lang="javascript"
  data-executable
  download-sketch-link="https://gitlab.com/aesthetic-programming/book/-/archive/masterBook-master.zip?path=public/p5_SampleCode.zh_TW/ch5_AutoGenerator"
/>

[RunMe：sketch](https://aesthetic-programming.gitlab.io/book/p5_SampleCode.zh_TW/ch5_AutoGenerator/)

1. **請閱讀**上面的 10 PRINT 原始碼，然後在您的電腦上照樣抄寫並執行。
2. 請就下列幾項 10 PRINT 規則**進行討論**，並將之與相關的程式碼行數／區塊搭配[^code]：
   * 丟擲骰子，並在一半的時間內在螢幕上輸出一個反斜線
   * 利用另外一半時間在螢幕上輸出一個正斜線
3. 請利用文字的「隨機性」：
   * **How**: 控制項是如何在 10 PRINT 中實施的？
   * **What**: 規律性的可預測或不可預測性可能為何？
   * **What**: 什麼是電腦的「隨機性」[^Haahr]？
   * **Discuss**: 討論隨機性在 *10 PRINT* 以及更廣泛的藝術（包括文學和遊戲）中的使用和角色？
4. 請試著**修改**既有規則，例如：
   * 我們是否可以改編斜線的尺寸、顏色和間距呢？
   * 我們是否可以產生出正斜線和反斜線之外的輸出？
5. *10 PRINT* 已被許多藝術家、設計師和學生所廣泛利用。請在推特上透過主題標籤「#10print」查看 [*10 PRINT*](https://twitter.com/search?q=%2310print&src=typd) 所提供的一些不同的選項。在課堂上，你的任務則是建立一個擁有一套清楚規則，並能像修改版的 *10 PRINT* 一樣運行的草稿碼。

</div>

## 蘭頓螞蟻

*10 Print* 作為生成式過程，同時把焦點放在指令和隨機性之上，不過，我們也希望能在自動化和生成式程式的脈絡之下，研究「演生」（emergence）的概念，而自動化和生成式程式中複雜的模式／結果，是經由簡單的規則所產生的。蘭頓螞蟻是一款經典的數學遊戲，模擬了螞蟻的分子邏輯，而其中如細胞般的小格子模擬，靈感則來自經典圖靈機，這種機器可透過讀取根據一組規則繪製的磁帶上輸入的符號，從而指示機器執行運算任務。

下一節提供了模擬小格子狀態的範例程式碼，並以黑色或白色的二維網格系統形式呈現。「螞蟻」可說是一種感測器，會以下文所述的簡單規則為基礎，把細胞資料作為輸入資料進行處理，隨後細胞將改變顏色，螞蟻則會朝向四個可能的方向移動，逐漸將網格變成一個更為複雜的系統，並展現出演生行為。

![drawing5](ch5_5_book2.png){: .print-only}
:   *圖 5.5：藍頓螞蟻——初始步驟*

![drawing5](ch5_5.gif){: .medium .web-only}
:   *圖 5.5：藍頓螞蟻——初始步驟*

圖 5.5 顯示了藍頓螞蟻的前三十三個步驟，螞蟻初始設定為面朝上，並遵循以下兩項一般規則：

* 規則一：螞蟻在白色方格處時，會右轉 90 度，並變成黑色，然後以方格為單位，向前移動一格。
* 規則二：螞蟻在黑色方格處時，則左轉 90 度，並變成白色，然後以方格為單位，向前移動一格。

一開始，畫布僅顯示網格系統，所有格子都設定為白色。螞蟻會根據它所在的格子顏色向左或右選轉 90 度，因此會有上、下、左、右四個可能的移動方向。一開始時，位於白色網格中心的螞蟻頭部朝上，隨後便依據上文的規則一，將頭部方向從朝上轉到朝右，把白色格子變為黑色，並向前移動一個單位。第二步，由於螞蟻所在的新格子仍為白色，因此再次遵循規則一，螞蟻頭部右轉 90 度，從朝右轉為朝下，將所在的白色格子變成黑色後，再向前移動一個單位。第三步和第四步與前面類似，直到螞蟻進入黑色格子，才會按照規則二將格子的顏色變回白色，並向左轉 90 度（而非右轉），隨著遊戲進行，複雜性也會增加。

![drawing6](ch5_6_book.png){: .medium .print-only}
:   *圖 5.6：藍頓螞蟻——過程*

![drawing6](ch5_6.gif){: .medium .web-only}
:   *圖 5.6：藍頓螞蟻——過程*

圖 5.6 展示了螞蟻在最初幾百個步驟中，一開始先是演生出類似「高速公路」，具備簡單對稱性質的圖案。隨後對稱性被打破，螞蟻似乎在畫布中心處隨機移動。經過數千次迭代，螞蟻又開始建構高速公路圖案，並無限地重複這個過程，直到大多數格子被重新配置，變成類似於圖 5.7 的結果，同時螞蟻仍繼續移動並改變格子的顏色[^Moreira]。

![drawing7](ch5_7.png){: .medium}
:   *圖 5.7：蘭頓螞蟻——演生過程快照*

[RunMe：sketch5_1](https://aesthetic-programming.gitlab.io/book/p5_SampleCode.zh_TW/ch5_AutoGenerator/sketch5_1/)

### 原始碼（蘭頓螞蟻）

<sketch
  src="p5_SampleCode.zh_TW/ch5_AutoGenerator/sketch5_1/sketch.js"
  lang="javascript"
  data-executable
  download-sketch-link="https://gitlab.com/aesthetic-programming/book/-/archive/masterBook-master.zip?path=public/p5_SampleCode.zh_TW/ch5_AutoGenerator/sketch5_1"
/>

### 解讀蘭頓螞蟻

程式中，能讓您放大檢視，從而將速度放緩，並調整尺寸的區域有三個。

1. `let grid_space = 5;` 第 24 行：如果將值更改為 10，所有內容都會放大。
2. `frameRate(20);` 第 48 行：降低影格率之值，以減緩程式運行速度。
3. 第 50 行中的 `draw()` 該函式包含一個 for 迴圈，其中 *n* 是螞蟻的步數。若有需要，您可以將 `n < 100` 減少到 `n < 1` （第 31 行），即 `for (let n = 0; n < 100; n++) {` 這段程式碼將指示程式，每個影格中僅處理 *n* 個步驟。

下文中，我們並不會逐行解釋程式碼，而是寫出各個函式的功能。

* 第 39 行的 `setup()` 函式：設定畫布大小、螞蟻的初始頭部方向、影格率、顏色等，並準備繪製背景網格結構。
* 第 78 行的 `drawGrid()` 函式：將畫布分割為網格結構。
* `draw():` 函式：這個主函式將檢查適用於*蘭頓螞蟻*的兩個規則，並更改格子的顏色。
* 第 96 行中的 `nextMove()` 函式：四個方向會以數字的格式建構，以便使用變量 `dir++`，透過遞增和遞減的編程術語（即 `dir++` 或 `dir--`)，來改變螞蟻的方向。不同的方向（上、下、左、右）各對應於畫布的水平（xPos）或垂直（yPos）軸上的移動。
* 第 108 行的 `checkEdges()` 函式：該函式會檢查螞蟻是否爬出了畫布範圍。出現這種情況時，程式的編寫方式，會讓螞蟻出現在相反的一側，並繼續執行程式。

由於前一章已經簡要介紹了二維陣列，這裡嚴格說來並沒有引進新的語法。不過，範例程式碼中，出現了二維陣列和巢狀 for 迴圈的新用法。

## 二維陣列與巢狀 for 迴圈

丹尼爾・席夫曼（Daniel Shiffman）建立了一個教學課程（有文字版[^shiffman1]和影片版[^shiffman2]），專門探討討論二維陣列為何在本質上，可說是其他陣列的陣列。他也提出，若二維陣列使用的網格結構，能與採行列設計的二維網格之*蘭頓螞蟻*背景相符，那麼將這種二維陣列納入考量，會是很有效的作法。由於我們需要辨識每個格子的狀態，因此也需要知道每個格子確切的 x 和 y 座標位置。

讓我們再次檢視用來繪製網格背景的原始碼：

```javascript
function drawGrid() {
 cols = width/grid_space;
 rows = height/grid_space;
 let arr = new Array(cols);
 for (let i = 0; i < cols; i++) { //列數 
  arr[i] = new Array(rows); //2D 陣列 
  for (let j = 0; j < rows; j++){ //行數 
    let x = i * grid_space; //實際 x 座標
    let y = j * grid_space; //實際 y 座標
    stroke(0);
    strokeWeight(1);
    noFill();
    rect(x, y, grid_space, grid_space);
    arr[i][j] = 0;  // 將關閉狀態和顏色指派給各個格子
   }
 }
 return arr; //回傳值為格子狀態的函式
}
```

我們使用語法 `let arr = new Array(cols);`（第4行）來建立陣列，這行程式碼指示了網格的列數，而陣列的長度與列數相同。由於我們還需要指示行數，因此利用 `arr[i] = new Array(rows);` 這行程式碼，從每列中的現有陣列裡，建立出另一個陣列（參見第 6 行）。此語法置於 for 迴圈之下，以確保每一列都經歷迴圈，但又額外加上了從畫布高度得出的行數資訊。二維陣列以 `arr[][]` 的形式建構。

為了知道網格內每一格的確切 x 和 y 座標，我們分別使用公式 `x = i * grid_space;` 和 `y= j * grid_space;`。透過兩個巢狀 for 迴圈（參見第 5 行和第 7 行），程式將在每個行列迴圈運行，直到程式到達最後一列。我們可以透過應用在列（使用變量 `i` ）和行（使用變量 `j` ）的語法 `array[i][j]` 來取得 x 和 y 座標。

因此，網格中的每一格都是以二維陣列的結構表示。如上所示，您可以藉由更改變量 `grid_space` 來放大或擴大格子的尺寸，顯示的列數和行數，則取決於由 `cols = width/grid_space;` 和 `rows = height/grid_space;` 所導出之畫布寬高。每個格子都以 `array[i][j]` 的形式呈現，透過 `i` 和 `j` 的數值改變，表示螞蟻在網格內移動的可能性。

自訂函式 `drawGrid()` 與我們在第三章〈無限迴圈〉中討論的函式略有不同，帶有回傳值 `return arr;` （第 17 行）。這表示函式完成後，將以二維陣列的形式回傳值 `arr` 。在上述蘭頓螞蟻的範例中，該函式用於繪製網格背景，並標記每個格子的初始狀態（關閉），以便稍後螞蟻開始移動時利用此資訊。

<div class="section exercise" markdown=1>
## 課堂練習

1. 給自己一點時間閱讀和修改程式碼，並觀察*藍頓螞蟻*不同階段的狀況。
2. 《蘭頓螞蟻》程式以抽象方式重現螞蟻的世界，並對格子的顏色，以及螞蟻的運動方式和方向訂下限制。請重新審思一下現已實施的規則，並試著以更改現有規則或加入新規則等方式，改變螞蟻的行為（回想一下您在上一個練習中對 *10 PRINT* 所做的更改）。
3. 在模擬如昆蟲的複雜行為等生命系統時，我們似乎更注重過程，而非結果。讓我們來對以下問題稍做討論：
  * 您可以想想看有哪些其他系統和過程也會展現出演生行為，並描述該行為嗎？
  * 在此脈絡之下，您會如何去了解自主性[^Watz]？您在多大程度上認為機器是生成式系統中的具能動性的代理人（agent）？這對更廣泛的文化又有什麼影響？
</div>

## While()

前一章中，我們已經討論過如何釋放改變的潛力，生成式系統似乎答應要提供類似的東西，也就是將現有系統設想成可改變，或可根據條件適應調整的系統。在數學家約翰・康威（Jon Conway）於 1970 年開發的《生命遊戲》[^Conway]中，與生命系統的相似性更為顯著，該遊戲是圖靈機的另一個實例，並示範了一項事物的進化過程，乃是由其初始狀態決定，不需要進一步的輸入，即可產生出演生形式[^game_eg]。與《蘭頓螞蟻》一樣，《生命遊戲》的基礎也是「細胞自動機」原理，亦即以規則的格子（「細胞」）[^cells]所組成的網格，每一格都處於一種狀態之中，而狀態的數量是有限的，例如本例中的開或關、活著或死亡兩種狀態。這些隱喻強而有力，並且可以應用在現實世界之中。

每個細胞都會與其他直接相鄰的細胞相互作用，並進行以下狀態轉換：

* 任何活著的「鄰居」細胞少於兩個的活細胞都會死亡（因人口過少而滅絕）。
* 任何活細胞，只要有二或三個活著的「鄰居」，就能繼續存活到下一代。
* 任何活著的鄰居大於三個的活細胞都會死亡（因人口過剩而無法生存）。
* 任何擁有恰好三個活著的鄰居的死細胞，都會再度復活（繁殖）。

《生命遊戲》新達爾文主義進化論式的「適者生存」邏輯已經夠令人憂慮，但這款遊戲「死亡政治」的面相，更讓不安的感受進一步加深[^necro]，以人口和社區為喻來闡述生死，簡直就像社會清洗計劃（或反烏托邦智慧城市[^smart]計劃）的一部分。這難道僅僅是個差勁的抽象示例嗎？

話說回來，另一種政治潛力也在此處運作，亦即具有適應性的複雜有機體，可以「自下而上」地自我組成，而無需仰賴中央「自上而下」的指揮和控制機制[^emergence]。當變化的方向變得無法預測，也不能得知一項改變是否會帶來更高層次的秩序，或是陷入混亂之時，「革命」的可能性便被彰顯出來了。回到螞蟻身上，對蟻群的研究顯示，他們之中並沒有明顯的等級制度，儘管人類使用有點聳動的術語「蟻后」來稱呼最有生殖能力的雌蟻，但「蟻后」根本不是什麼權威人物，只是一個負責產卵的工作人員罷了，而工蟻之間也以合作為主，而非封建式社會關係（順道一提，所有工蟻都是雌性，但螞蟻的性政治已經超出了本書的討論範圍，此外，我們也意識到，在蘭頓螞蟻的例子裡，格子／細胞顏色的變化隱射了種族相關面向）。

更準確地說，根據複雜性理論，所有系統中都包含不斷波動的子系統。由回饋所引起的一個或多個波動，可能會改變原有的組織，正因如此，系統中多個相互作用的元素，是無法被轄制的，而我們也不能預測其集體行為。正如伊利亞・普里高津（Ilya Prigogine）和伊莎貝爾・斯唐熱（Isabelle Stengers）在《從混沌到有序》（Order Out of Chaos）中的解釋：

>「完全按照功能模型定義的社會，將會符合亞里斯多德式的自然等級和秩序思想，其中每位官員都將履行其被指派的職責，而在各個層面上，這些職責體現了整體社會組織的不同面向。國王向建築師下達命令，建築師向承包商下達指令，而承包商再向工人發號施令。反之，白蟻和其他群居昆蟲的行為似乎更接近於「統計」模型。正如我們所看到的，白蟻巢穴的建造背後似乎沒有主導人物，這僅是個體間的互動，在某些情況下催生出某些特定類型的集體行為，然而，這些互動完全屬於局部的相互作用，沒有一項涉及任何全局任務[^chaos]。」

為了理解演生行為，可以參考圖靈 1952 年的文章〈形態發生的化學基礎〉，文中描述了自然界中的圖樣如何從均勻一致的狀態自然變化而來[^morpho]。政治理論家兼社運人士弗蘭克・「比弗」・布拉迪（Franco “Bifo” Berardi）便以這種「形態發生」的概念來闡述社會和政治的突變，或可說是全新形式的出現和形成。自動化流程不僅以資訊技術取代了物理性的生產行為，更改變了認知活動本身。對於布拉迪來說，這「意味著將認知活動簡化為演算法程序」，並「在一般智力（general intellect）的社會存在之中插入了自動性[^eflux]」。自動化漸漸取代政治決策，便是上述所造成的其中一項後果，因此造就了只有「是或否 [⋯⋯] 沒有細微差別或曖昧性的空間」之情況，對布拉迪來說，這代表著民主的終結，以及自動邏輯程序鏈的建立，而這種程序鏈的目的，就是取代有意識的自主選擇和決策。布拉迪認為，機器所捕捉到的，不僅是人類的思考能力，更是我們的感受能力[^Bifo]。他指出，有一部分的問題，出在我們一直從機器那裡學習單詞，而非其他人類[^mother]，導致人們在愛、溫柔和同情方面的能力降低。援引瑪麗亞・普伊格・德拉・貝拉卡莎（Maria Puig de la Bellacasa）等女性主義技術科學著作的說法，我們應該會在這個列表中新增「關懷」這一項。對貝拉卡莎而言，關懷的重要性，在於它能引導人們關注事物如何連結在一起，以及事物之間的關係為何，因此，「將事物轉化為『關懷』相關的議題，是與它們建立連結、不可避免地受其影響，並改變其影響他者的潛力的一種方式[^bellacasa]。」

沿續以上思路，海倫・普里查德（Helen Pritchard）和孫詠怡（Winnie Soon）也利用女性主義技術科學，創作出藝術作品《反覆出現的酷兒想像》（Recurrent Queer Imaginaries），此作品是一個會不斷產生出座右銘的「助手」，可作為重新思考並重新闡釋城市生活，並沈浸於相關夢境之中的一種形式。有鑑於種族、性別和階級不公義血淚斑斑的鬥爭史，座右銘助手選擇以酷兒和交織性生活的宣言和獨立誌，作為機器學習和產生過程的原始文本[^pritchard]。這種將關懷付諸行動的方法還有另一個例子，就是詩學運算學院（School of Poetic Computation）開設的「數位愛語」課程教學大綱[^sfpc]，其中，講師梅蘭妮・霍夫（Melanie Hoff）探討了如何將程式碼培養為一種比起監視和剝削的集體系統更為溫和、療癒又親密的「愛語」[^hoff]。這門課程涵蓋了程式和自然語言處理的基礎知識，同時也探討了情書作為一種詩歌形式的歷史。

更多關於編程之中的愛和關懷的討論，讓我們迎來了最後一個例子，亦即 1953 年出現在曼徹斯特大學計算機學系佈告欄上的生成式「情書」。這些電腦產生的愛的宣言，是由克里斯多福・斯特雷奇（Christopher Strachey）用最早的可編程計算機 M.U.C.（曼徹斯特大學計算機，又稱費朗蒂一號，Ferranti Mark I）內建的隨機產生器功能編寫之程式所產出的。有些人認為，這是數位藝術的第一個範例[^Noah]，雅各布・加布里（Jacob Gaboury）則認為，斯特雷奇本身和圖靈一樣是酷兒身份，因此，這些情書更是對異性戀本位愛情的批判[^Gaboury]。此外，我們也可以說，這些情書所表達的渴望，針對的不僅僅是同性之愛，更是人機之愛。

藝術家大衛・林克（David Link）在仔細研究了上述作品的各個功能面向後，為其硬體和原始程式建構了的功能性的複製品[^loveletters]。主程式較為簡單，是採用迴圈和隨機變數來遵循下列語句的結構：「你是我的 — [形容詞] — 實質名詞」，以及「我的 — [形容詞] — 實質名詞 — [副詞] — 動詞 — 你的 — [形容詞] — 實質名詞。」有些單詞是固定的，而放在方括號中的單詞則是可選填的。該程式從選項列表中挑選形容詞、副詞和動詞，並採用迴圈配置，以避免重複的情況發生，而此軟體可以產生出超過 3180 億種變體。就效果而言，這段對話結構裡最重要的部分，在於「我」（程式編寫者）和「你」（人類讀者）之間建立的交流，這讓我們有了文字的發話對象是「自己」這個人的實感。由此產生的情書，表達方式溫柔得令人驚訝，這與我們所認為的運算程式的標準性功能結果截然不同。但這個觀點絕非愛情簡化論，也許編寫程式的人所面臨的挑戰，是要產生出酷兒的重組形式，其中發送者或接收者都不會透過性別、物種或形式的指定，而被預先決定。這裡提供一項輸出範例，作為本章的結束：

> DEAR DARLING 
>
> YOU ARE MY BEAUTIFUL RAPTURE. MY INFATUATION BEAUTIFULLY CLINGS TO YOUR ADORABLE LUST. MY INFATUATION LUSTS FOR YOUR WISH. MY AMBITION CURIOUSLY LIKES YOUR LOVE. YOU ARE MY DEAR EAGERNESS.
>
> YOURS WISTFULLY
>
> M. U. C.

> 親愛的寶貝
>
> 你是我美麗的狂喜。我的迷戀美麗地依附著你可愛的欲望。我的癡心渴求著你的願望。我的嚮往異常心悅你的愛。你是我親愛的渴望。
> 
> 你戀戀不捨的
> M. U. C.

<div class="section exercise" markdown=1>

## 迷你習作：產生式程式

**目標：**

* 從頭開始執行以規則為基礎的產生式程式。
* 加強迴圈和條件語句在運算中的使用。
* 從概念和實務上，反思自動產生器這個想法。

**更多靈感：**

* *{Software} Structure #003 A* by Casey Reas (2004), <https://whitney.org/exhibitions/programmed?section=1&subsection=6#exhibition-artworks>.
* *[Daily Art](https://sasj.nl/portfolio/daily/)* by Saskia Freeke (2018).
* *[Generative Artistry](https://generativeartistry.com/tutorials/)* by Ruth John and Tim Holman (n.d.).
* *[Generative Design - sketches](http://www.generative-gestaltung.de/2/)* (n.d.), and source code, <https://github.com/generative-design/Code-Package-p5.js>.
* *GenArt* by Joseph Fiola (2016), with source code, <https://github.com/JosephFiola/GenArt>.
* *Game of Life* by John Conway (1970): <https://web.archive.org/web/20181007111016/http://web.stanford.edu/~cdebs/GameOfLife>.
* *Generative Tarot* by Melanie Hoff (2019), <https://www.melaniehoff.com/generativetarot>, source code <https://github.com/melaniehoff/generative-tarot-p5js>.
* *[The Recode Project](http://recodeproject.com/)* (featuring projects from 1976-78), and *[Memory Slam](http://nickm.com/memslam/)* by Nick Montfort (2014).
* *Solving Sol* by Brad Bouse (n.d.), an open project to implement Sol LeWitt's instructions in JavaScript  <https://github.com/wholepixel/solving-sol>.

**任務（RunMe）：**

1. 拿起一張白紙開始構思，想出至少兩個您想要在產生式程式中實施的簡單規則。
2. 根據您在步驟一中設定的規則，設計一個產生式程式，其中**至少要使用一個 for 迴圈／while 迴圈和一個條件語句**，但兩者不用具備任何直接的互動性，只要讓程式運行即可。如果有幫助的話，您也可以考慮利用 `noise()` 和 `random()` 語法。

**在您的 ReadMe 檔案中可供思考的問題：**

* 您的產生式程式有哪些規則？請描述一下，隨著時間的經過，您的程式表現如何？規則又是如何產生演生行為的？
* 規則和流程在您的作品中扮演什麼樣的角色？
* 請參考指定的閱讀內容，這個迷你習作如何幫助您理解「自動產生器」的概念（例：透過規則，控制、自主性、愛和關懷能實現到何種程度）？對本章的主題還有什麼進一步的想法嗎？
</div>

## 指定閱讀

* Nick Montfort et al. "Randomness," *10 PRINT CHR$(205.5+RND(1)); : GOTO 10*, <https://10print.org/> (Cambridge, MA: MIT Press, 2012), 119-146.
* Daniel Shiffman, "p5.js - 2D Arrays in Javascript," *Youtube*, <https://www.youtube.com/watch?v=OTNpiLUSiB4>.
* Jon, McCormack et al. “Ten Questions Concerning Generative Computer Art.” *Leonardo* 47, no. 2, 2014: 135–141.

## 延伸閱讀

* Philip Galanter, "Generative Art Theory," in Christiane Paul, ed., *A Companion to Digital Art* (Oxford: Blackwell, 2016), <http://cmuems.com/2016/60212/resources/galanter_generative.pdf>.
* "How to Draw with Code | Casey Reas," *Youtube* video, 6:07, posted by Creators, June 25 (2012), <https://www.youtube.com/watch?v=_8DMEHxOLQE>.
* Daniel Shiffman, "p5.js Coding Challenge #14: Fractal Trees - Recursive," <https://www.youtube.com/watch?v=0jjeOYMjmDU>.
* Daniel Shiffman, "p5.js Coding Challenge #76: Recursion," <https://www.youtube.com/watch?v=jPsZwrV9ld0>.
* Daniel Shiffman, "noise() vs random() - Perlin Noise and p5.js Tutorial," <https://www.youtube.com/watch?v=YcdldZ1E9gU>.

## 註釋

[^Turing]: Alan Mathison Turing, "On Computable Numbers, with an Application to the Entscheidungsproblem," *Proceedings of the London Mathematical Society* 2, no.1 (1937): 230-265.

[^Turing1]: Turing, "On Computable Numbers," 241.

[^visualization]: 視覺化的圖靈機可參考下列網站：<https://turingmachine.io/>。

[^TuringGraph]: 此為網路搜尋圖片經修改後的版本，參見：<http://storyofmathematics.lukemastin.com/20th_turing.html>。

[^laczko]: 先前在第一章〈入門〉中所提及的茱莉・拉克茲蔻（Juli Laczko）〈webmachines〉藝術作品，便展現了編織和編程技術之間的關係，請參見：<https://digital-power.siggraph.org/piece/webmachine/>。

[^Lippard]: 參考文獻：Lucy Lippard, ed. *Six Years: The Dematerialization of the Art Object from 1966 to 1972* (London: University of California Press, 1997)。

[^Cox]: 例子不勝枚舉，其中一項也與一次展覽有關的示例如下：Geoff Cox's "Generator: The Value of Software Art," in Judith Rugg and Michèle Sedgwick, eds., *Issues in Curating Contemporary Art and Performance* (Bristol: Intellect, 2007), 147-162，原文請參見：<https://monoskop.org/images/5/53/Cox_Geoff_2007_Generator_The_Value_of_Software_Art.pdf>。文中描述了亞德里安・沃德（Adrian Ward）的作品《自動繪圖器》（Auto Illustration），該軟體以盒裝版本的形式，發表於名為《產生器》（Generator）的展覽中（2003 年 3 月），而隨附的「使用者手冊」中，則包含了相關技術細節和批判性文章。此作品在很多方面來說，都為您正在閱讀的這本書開創了先例。針對樂譜、腳本和程式之間相似之處的美學面向，欲知更多資訊，請參閱：Geoff Cox, Alex McLean, and Adrian Ward, "The Aesthetics of Generative Code," *Generative Art 00*, international conference, Politecnico di Milano (2001), <https://www.academia.edu/10519146/The_Aesthetics_of_Generative_Code>。

[^exhibition]: 由克里斯蒂安・保羅和卡蘿・曼庫西-溫加羅（Carol Mancusi-Ungaro）統籌，與克里蒙斯・懷特（Clémence White）合作之展覽《程式化：藝術中的規則、程式碼和編排，1965–2018》（Programmed: Rules, Codes, and Choreographies in Art, 1965-2018）展覽於 2018 年 9 月 28 日至 2019 年 4 月 14 日在紐約惠特尼美國藝術博物館。參見：<https://whitney.org/exhibitions/programmed>。一個被廣泛引用的先例，是 1970 年由傑克・伯納姆（Jack Burnham）擔任策展人，於紐約猶太博物館舉辦的展覽《軟體——資訊科技：藝術新含義》。對伯納姆而言，此展覽鼓勵人們去理解藝術和資訊系統的底層架構，並透過電腦技術的實務與概念藝術之間的結合，將軟體呈現為資訊交換的一種隱喻。

[^289]: 此版本以 JavaScript 進行編程，使用 D3 和 jquery 函式庫，參見：<https://github.com/wholepixel/solving-sol/blob/master/289/cagrimmett/index.html>。

[^LeWitt]: 索爾・勒維特的這段話引用自：Lippard, ed. *Six Years: The Dematerialization of the Art Object from 1966 to 1972*。

[^processing]: 由卡西・瑞斯和班・弗萊（Ben Fry）於 2001 年發起的計畫「Processing」，是一種靈活的軟體素描本，也是程式語言，可供使用者學習如何在視覺藝術的脈絡之下進行程式編寫。參見：<https://processing.org/>。

[^Reas]: 關於此作品的解釋及相關文件說明，請參閱：Casey Reas, "{Software} Structures,"  <https://artport.whitney.org/commissions/softwarestructures/text.html>。

[^joan0]: 《編程演算法繪圖》（Coded Algorithmic Drawings）系列作品，請見：<https://joantruckenbrod.com/gallery/#(grid|filter)=.coded>。

[^joan1]: 瓊・特魯肯布羅德的訪談《Motion Through Series》請見：<https://vimeo.com/286993496>。

[^tree]: 碎形幾何圖案常見於伊斯蘭和非洲之藝術、設計和建築傳統中，特徵為由可重複且無限的流程中，所產生的自我相似性質。歐亞文化中的碎形設計傾向於模仿自然界的圖案，但根據羅恩・艾格拉許（Ron Eglash）的觀察，非洲的設計則更受其自身社會結構的影響，而在這種結構中，碎形被視為共同文化的一部分。參見：Ron Eglash, *African Fractals: Modern Computing and Indigenous Design* (New Brunswick, New Jersey, and London: Rutgers University Press, 1999)；以及 Laura U. Marks, *Enfoldment and Infinity: An Islamic Genealogy of New Media Art* (Cambridge, MA: The MIT Press: 2010)。另請參閱馬丁・齊拉克（Martin Žilák）在 p5.js 中利用遞迴性繪製碎形樹的編程範例：<https://editor.p5js.org/marynotari/sketches/BJVsL5ylz>。

[^flock]: 參見克雷格・雷諾（Craig Reynold）p5.js 原始碼的群集行為：<https://p5js.org/examples/simulate-flocking.html>。

[^agent]: 在代理人模型的描述中，資料是作為個體自主代理人，以進行數學建模，這些代理人遵循某個環境或系統中的規則，隨著時間的推移，其行動和互動便會產生出演生性的結果。

[^Arns]: Inke Arns, "Read_me, run_me, execute_me: Code as Executable Text: Software Art and its Focus on Program Code as Performative Text," trans. Donald Kiraly, *MediaArtNet* (2004), see: <http://www.mediaartnet.org/themes/generative-tools/read_me/1/>.

[^Haraway]: 關於這一點，顯然還可以解釋得更仔細，不過，我們的例子則是參考唐娜・哈拉維（Donna Haraway）的著作《當物種相遇》（*When Species Meet*, Minneapolis: Uiversity of Minnesota Press, 2007）。

[^galanter]: Philip Galanter, "What is Generative Art? Complexity Theory as a Context for Art Theory," in *GA2003* - 6th Generative Art Conference, Milan (2003).

[^shiffman]: 本章中的兩個程式碼範例改編自丹尼爾・席夫曼的《編程訓練》（Coding Train）系列課程，並加入更多註釋來解釋其邏輯，以及調整《蘭頓螞蟻》範例中的網格大小等額外功能。

[^10print]: Nick Montfort, et al, *10 PRINT CHR $(205.5+ RND (1));: GOTO 10* (Cambridge, MA: MIT Press, 2012).

[^Langton]: Christopher G. Langton, "Studying Artificial Life with Cellular Automata," *Physica D: Nonlinear Phenomena* 22, no.1–3 (October 1986): 120–49, <https://doi.org/10.1016/0167-2789(86)90237-X>.

[^code]: 儲存庫中可看到更多行的註解，參見：<https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/blob/master/public/p5_SampleCode.zh_TW/ch5_AutoGenerator/sketch.js>。

[^Haahr]: 參見：Mads Haahr, "Introduction to Randomness and Random Numbers," <https://www.random.org/randomness/>，以及 Montfortet al, "Randomness," 119-146。

[^Moreira]: Andrés Moreira, Anahí Gajardo and Eric Goles, "Dynamical Behavior and Complexity of Langton’s Ant," *Complexity* 6, no.4 (March 2001): 46–52, <https://doi.org/10.1002/cplx.1042>.

[^shiffman1]: 為 Processing 社群撰寫的〈二維陣列〉（“Two-dimentional Arrays”），參見：<https://processing.org/tutorials/2darray/>。

[^shiffman2]: p5.js 中的二維陣列影片教學，請見：<https://www.youtube.com/watch?v=OTNpiLUSiB4>。

[^Watz]: 例如，生成藝術家馬利烏斯・瓦茨（Marius Watz）便在其演講〈美麗的規則：創意的產生式模型〉（Beautiful Rules: Generative Models of Creativity）中提出「自主性是最終的目標」，出處：*The Olhares de Outono* (2007), <https://vimeo.com/26594644>。

[^Conway]: 關於康威《生命遊戲》的更多資訊，以及相關之細胞自動機，可參見：<https://www.conwaylife.com/>。

[^game_eg]: 更多康威《生命遊戲》相關討論及原始碼，請參見：<https://web.archive.org/web/20181007111016/> & <http://web.stanford.edu/~cdebs/GameOfLife/>。

[^cells]: 譯按：在《生命遊戲》的例子中，為方便理解，所有的「cell」都譯為「細胞」。

[^necro]: 延續米歇爾・傅柯（Michel Foucault）創造的術語「生命政治學」（Biopolitics，以權力來控制人們的生活），死亡政治學（Necropolitics）是藉著行使社會和政治權力，從而決定某些人的生死。參見 Achille Mbembe, "Necropolitics," *Public Culture* 15, no.1 (2003): 11–40。

[^smart]: 請參見安德斯・維斯蒂（Anders Visti）和托比亞斯・史丹伯格（Tobias Stenberg）的作品《WUOUS》，該作品透過《蘭頓螞蟻》在實務上的應用，質疑了所謂「智慧城市」的程式邏輯：<https://andersvisti.dk/work/wuos-2019>。

[^emergence]: 更多演生行為相關資訊，請參見：Steven Johnson, *Emergence: The Connected Lives of Ants, Brains, Cities and Software* (London: Penguin, 2001), 20。

[^chaos]: Ilya Prigogine and Isabelle Stengers, *Order Out of Chaos: Man’s New Dialogue With Nature* (London: Fontana, 1985), 205.

[^morpho]: Alan Mathison Turing, "The Chemical Basis of Morphogenesis," *Philosophical Transactions of the Royal Society of London B,* 237, no.641 (1952): 37–72, doi:10.1098/rstb.1952.0012. JSTOR 92463.

[^eflux]: Franco "Bifo" Berardi, "The Neuroplastic Dilemma: Consciousness and Evolution," in *e-flux* journal #60 (December 2014), <https://www.e-flux.com/journal/60/61034/the-neuroplastic-dilemma-consciousness-and-evolution/>. 「一般智力」是馬克思《政治經濟學批判大綱》（Grundrisse）的〈機器論片段〉中的關鍵概念，表示技術專業知識和社會智力的結合。泰拉諾瓦（Terranova）認為，機器的進化也釋放了生產力，此論點亦是借鑒了「一般智力」的概念，本書第四章〈資料擷取〉中便援引了泰拉諾瓦的說法。

[^Bifo]: Franco "Bifo" Berardi, *Precarious Rhapsody: Semiocapitalism and the Pathologies of the Post-Alpha Generation* (London: Minor Compositions, 2009), 9. 更多決策政治相關資訊，請參閱：Luciana Parisi's "Reprogramming Decisionism," *e-flux* #85 (October 2017), <https://www.e-flux.com/journal/85/155472/reprogramming-decisionism/>。

[^mother]: 在 N・凱薩琳・海爾斯（N. Katherine Hayles）的著作《我的母親是計算員》（My Mother was a Computer）中，她描繪了 1930 到 1940 年代，計算工作主要由女性雇員承擔的情況，當時這些女性被稱為「計算員」（Computers，現在指稱電腦）。N. Katherine Hayles, My Mother Was a Computer (Chicago: University of Chicago Press, 2005)。海爾斯的書名取自安妮・巴爾薩莫（Anne Balsamo）所著《性別化身體技術》（Technologies of the Gendered Body）中的一個章節，這位作家的母親就是當時的計算員之一。

[^bellacasa]: Maria Puig de la Bellacasa, "Matters of Care in Technoscience: Assembling Neglected Things," in *Social Studies of Science* 41, no.1 (2010), 99.

[^pritchard]: 根據以規則為基礎，透過運算執行的語句分裂（diastic）技術，每個座右銘助手都會依照單詞中的字符序列自動書寫，以造出句子。所有單詞和句子都是基於同一個種子文本：「非為自己，而是為了全人類」（Not for Self, but for All），這段文字出現於倫敦國王十字區域新開發地區的中心處，由於科技公司和新創企業的進駐，該區中許多酷兒空間已被關閉。總之，《反覆出現的酷兒想像》這一作品「呼籲大眾從企業新殖民主義的想像與不公正的營運中，奪回酷兒的空間，並致力於酷兒解放，以不同的方式為所有人重新構思這些空間。」從《反覆出現的酷兒想像》中產生的座右銘範例，請參見如下影片文檔：<https://digital-power.siggraph.org/piece/recurrent-queer-imaginaries/>。

[^sfpc]: 位於紐約的詩學運算學院（School for Poetic Computation），是一所由藝術家開辦的學校，成立於 2013 年，致力於探索程式碼、設計、硬體和理論的交匯點，並特別關注藝術介入（artistic intervention）議題，參見：<https://sfpc.io/>。

[^hoff]: 參見：*Digital Love Languages ♡ Codes of Affirmation*, <http://lovelanguages.melaniehoff.com/syllabus/>。

[^Noah]: Noah Wardrip-Fruin, "Christopher Strachey: The First Digital Artist?," *Grand Text Auto*, School of Engineering, University of California Santa Cruz (August 1, 2005).

[^Gaboury]: Jacob Gaboury, "A Queer History of Computing," *Rhizome* (April 9, 2013). 我們將在第七章〈語音程式碼〉中回到圖靈的性取向的議題。

[^loveletters]: 大衛・林克的《LoveLetters_1.0: MUC=Resurrection》首次展出於 2009 年，並於 2012 年參加卡塞爾文獻展（dOCUMENTA(13)）。詳細說明和文檔請見：<http://www.alpha60.de/art/love_letters/>。亦歡迎參閱：Geoff Cox, "Introduction" to David Link, *Das Herz der Maschine*, dOCUMENTA (13): 100 Notes - 100 Thoughts, 100 Notizen - 100 Gedanken # 037 (Berlin: Hatje Cantz, 2012)。  

[^suchman]: Lucy Suchman, *Human-Machine Reconfigurations: Plans and Situated Actions* (Cambridge: Cambridge University Press, 2007), 217-220.

[^Steps]: See the web-based step by step running of the Langton's Ant implemented by Barend Köbben in 2014, <https://kartoweb.itc.nl/kobben/D3tests/LangstonsAnt/>.
