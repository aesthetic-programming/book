Title: 第九章：演算法過程
Slug: 9-algorithmic-procedures
page_order: 9

![flowchart](ch9_0.svg)

[TOC]

## setup()

電腦科學家兼程式設計師先驅葛麗絲・穆雷・霍普（Grace Murray Hopper）曾說，規劃程式設計「就像計劃晚餐一樣。[^Mandel]」程式設計與烹飪很相似，都需要耐心以及管理細節和「配料」的能力。[^hopper2]程式設計師高德納（Donald Knuth）在其著作《電腦程式設計的藝術》（The Art of Computer Programming，1968）中也提到，就如食譜被寫在烹飪書籍中，演算法也被寫在程式碼裡，兩者都列出了可供遵循的操作方式，並可以分享和重現，而這本書也透過與食譜的類比，來強調程式設計的美學維度。[^krysa]事實上，程式設計和烹飪有著共同的屬性，包括如何選擇來源、應用操作方式，以及轉換是如何發生的。這些評論和高德納的寫作風格定調了本章的主題，但同時也為讀者說明了演算法，套句他的話，就是所謂「閱讀的流程」[^knuth0]。以下摘錄書中片段：

>「1. 請開始閱讀此流程（除非您已經開始閱讀）。請繼續確實地執行步驟； […] 5. 您對本章的主題有興趣嗎？若有興趣，請至步驟 7；若沒有興趣，請至步驟 6。14. 您累了嗎？若否，則回到步驟 7；15. 請去睡個覺，醒來後請再回到步驟 7。」[^knuth]

這個例子旨在強調人們傾向於忠實地遵循指示。不過，我們或許也會觀察到，演算法不僅僅只是簡單的步驟和流程式操作，它還具備更廣泛的文化和政治含義，尤其我們又可以決定是否要以自己的方式進行解釋。從這個意義上說，演算法就像烹飪一樣，體現了文化差異、品味問題，甚至是美學。高德納又進一步將這種類比延伸到其他文化實踐上，引用愛達・勒芙蕾絲（Ada Lovelace）的話，他表示：「為數位電腦準備電腦程式的過程特別吸引我，這不僅是因為它可以帶來經濟和科學方面的回報，更因為這也可以是一種美學體驗，就像創作詩歌或音樂一般。」[^knuth2]

本章將討論像食譜一樣的演算法過程，以及這些過程如何描述程式的步驟和操作，對程式碼的語法則著墨較少。演算法與一行行的程式碼的不同之處，在於演算法並不仰賴特定的軟體和函式庫。它只是計算或其他解決問題的操作（尤其是透過電腦進行的這類操作）中，所須遵循的過程或一系列規則。[^algo]演算法是程式運行的骨幹，並會展示操作步驟，在理想情況下可以透過任何具有「圖靈完備性」的機器實施，因此在運算方面具備通用性，並且能夠解決任何運算問題。[^complete]換句話說，演算法展示了程式操作的系統性分解，以描述操作如何從一個步驟邁入下一個步驟。演算法就像一般意義上的食譜，具有一組逐步指令，但由於食譜缺乏圖靈完備性所要求的精確性和可重複性，無法根據給定的指令辨識和操作資料，從而解決運算問題。

## start()

在第三章〈無限迴圈〉中，我們簡要介紹了愛達・勒芙蕾絲在 1842 年起草的運算圖表，此圖常常被稱為世上第一個電腦程式（見圖 3.2）。[^first] 勒芙蕾絲發表的圖表，以及她親手撰寫的大量註釋，展示了解決數學問題所需之逐步運算的複雜步驟。這些指令的設計，是為了要讓機器可以自動執行。正如勒芙蕾絲所言：「我想在自己的一篇筆記中，加入一些關於白努利數的例子，說明（搜尋）引擎如何在沒有人腦和人手的情況下，計算出隱函數。請給我必要的公式。」[^ada2]而這些公式在圖表中發展成為演算法過程。

本章我們將以「圖表」為基礎，並以流程圖來闡述演算法過程的實踐和概念面向。流程圖，即「圖表的流程 -> 流程的圖表」[^constant]，自電腦程式設計初興之時起，就被認為是基本的解釋工具。流程圖的一項常見用途，是透過「將數值方法轉換為一系列步驟」[^Ferranti]，來說明程式設計的運算操作和資料處理。但我們也能將流程圖視為一種示意圖表，程式設計師及其他參與軟體製作的人之間，便可借流程圖之力來傳達複雜的邏輯。這當然是一個很好的實務作法，對於初學者來說尤其如此，若想以易於理解的方式與他人交流想法，這點也相當關鍵。事實上，程式設計不一定都是獨自作業[^stereotype]，正如我們的討論中所言，程式設計也可以看作一種透過圖表來展示不同實體之間的關係的社交或交流。此外，大多數的軟體應用程式都不是由單一程式設計師所開發，而是整理成任務，分派給多個程式設計師合作處理，例如維護其他人編寫的程式或進行除錯。合作的工作流程非常適合用流程圖來表示。

## 課堂討論

* 您能否舉出您自己在日常中使用過或經歷過的演算法為例（請詳細說明運算邏輯）？
* 您可否起草一個演算法式的過程？舉例來說，您社群媒體的動態的整理方式為何？
* 根據由泰娜・布赫（Taina Bucher）所作的指定讀物，您可否列出幾項演算法的屬性？這些屬性如何同時擁有技術性和社會性兩個面向？
* 第六章〈自動產生器〉中，我們討論了基於規則的系統，這與現在本章中所討論的程序性有何不同？

## 流程圖

傳統上，流程圖中的每個步驟都會以符號和連接線表示，用來指示出邏輯流的方向與特定的輸出結果。這些符號各有不同的意義。下文中概述了流程圖的基本構成：

- **橢圓形**：表示程式／系統的起點或終點（但關於這點，我們需要進一步反思是否所有程式都有結束）。
- **矩形**：代表過程中的步驟。
- **方塊**：以「是」或「否」的分支指示決策點位置。
- **箭頭**：作為連接線使用，以展示關係和序列，但有時箭頭可能會指向前一個過程，在流程涉及重複和迴圈時尤然。

圖 9.1 展示我們在第七章中所看到的《Vocable Code》程式的流程圖。此流程圖顯示了高階的邏輯和序列，並用簡單的英語詳細闡述細節。流程圖透過符號、線條和文字，與更廣泛的大眾以及本書讀者進行交流。


流程圖在許多技術性或藝術性領域中都有應用。舉例而言，在業務中，流程圖相對常被使用，藉著流程圖，我們可以得知各種過程或工作流程該如何有效組織起來，並將這些資訊傳達給其他人。在哲學領域，圖表是用來畫出新思想產生的過程和關係，吉爾・德勒茲（Gilles Deleuze）和費利克斯・瓜塔里（Félix Guattari）便將圖表稱為「抽象機器」[^Guattari]。本章結尾處將更詳細地回顧這些想法，在教學中，我們同樣也會利用流程圖來解構寫作，以及分項列出論文結構中的各個論點，這是一種發想新點子和結構的方式。圖表是種相當出色的好工具，或者，更確切來說是「機器」，可以幫助我們思考不同的流程和過程，顯然，我們之所以會用流程圖來介紹本書的每一章節，就是受這項工具所啟發。

本章的迷你習作將請您與他人合作，為一個新的項目想法製作流程圖。到目前為止，您或許已經更有信心能建構出一個包含各種語法、更為複雜的程式，在這種情況下，培養組織能力便成為了下一項更具挑戰性和必要性的任務。我們發現，人們在進行組織時所面臨的一項困難，是不知該如何組合和連結各種功能，以及如何將任務拆分為更小的、連續的步驟。我們認為，流程圖會是有效的方法，能協助我們發想、引起討論、觀察關係、預測技術挑戰，以及找出適合該項目的合作方式。舉例來說，如果有個小組需要將任務分成幾項小任務並合作處理，流程圖便可用來確定如何將較小的任務與其他任務連結起來，才不會見樹不見林。

將現有程式轉換為流程圖，所會面臨的一些挑戰包括：

1. 將程式設計語法和函式翻譯成易於理解的簡單語言。
2. 決定重要操作的詳細程度，以便讓其他人了解您的程式邏輯。

![flowchart](ch9_1.png){: style="max-height: 93vh"}
:   *圖 9.1：為孫詠怡〈言說程式碼〉章節所繪的流程圖，由安德斯・維斯提（Anders Visti）設計*

<div class="section exercise" markdown=1>

## 課堂練習

### 練習一

讓我們先從看起來相對簡單的事開始，比如加入表情符號和注意變數名稱等。下文的程式碼利用了《Multi》中的表情符號（第二章〈變數幾何〉）和《Vocable Code》中用來命名的程式碼（第七章〈言說程式碼〉），並使用網路瀏覽器控制台中的 for-loop 迴圈，從而在螢幕上輸出橫跨多物種的各式表情符號。您的任務是根據這個程式畫出一張流程圖：

```javascript
function setup() {
  let multi = ['🐵','🐭','🐮','🐱'];
  for (let species = 0; species < multi.length; species++) {
    console.log(multi[species]);
  }
}
/*輸出
🐵
🐭
🐮
🐱
*/
```

我們之前曾在課堂[^ex]中進行過此練習，結果是出現了許多不同的流程圖，而這些圖表已成為很棒的資源，可用以討論流程圖的多種目的和含義。[^Ensmenger]

### 練習二

排序（sorting）是數位文化中的常用演算法，諸如 Spotify、Amazon、Netflix 上的推薦列表，想必大家都很熟悉。請動動腦，思考一下為解決下面的排序任務而進行程式設計時，需要哪些「演算法程序」[^program]。

請產生出兩個數字範圍之間，唯一隨機整數的 x 值（例如，x = 1,000）列表。然後實行一個排序演算法，以按升序顯示這些數字。進行此練習時，不可在 p5.js 或 JavaScript 中使用現有的 `sort()` 函式。您會如何解決這個問題？請將演算法繪製為流程圖，並把重點放在過程/步驟，而非實際的語法。
</div>

## 流程圖作為藝術性媒介

除了實用之外，流程圖本身也可以成為藝術品，正如保羅・奇里奧（Paolo Cirio）所言，可作為「社會複雜性美學的元媒介」[^Cirio]。其中一例，是 2005 年的作品《Google Will Eat Itself》[^GWEI]，該藝術作品係由里奧與亞歷山德羅・盧多維科（Alessandro Ludovico）與視覺藝術團隊 UBERMORGEN 合作創造，是透過駭入 Google AdSense，從而自動產生收入。[^pold]這個項目會自動觸發網站上的廣告點擊，以接受來自 Google 的小額付款，然後將這筆錢用來購買 Google 的股份：「我們透過他們自己投放的廣告來買下 Google！Google『自食其力』，但最終卻是『我們』會成為它的擁有者！」

![diagram1](ch9_2.gif){: style="height: 250px;"}
:   *圖 9.2：《Google Will Eat Itself / THE ATTACK》（2005），由保羅・奇里奧、亞歷山德羅・盧多維科與UBERMORGEN 合作打造。圖片由藝術家本人提供*

圖中可以清楚地看到迭代（或同類相食）的迴圈，這與「古怪迴圈」的原理相呼應：套句巴貝奇的話，這個迴圈被迫「吃掉自己的尾巴」，改變自己所儲存的程式，參考前文的描述以及分析引擎的作業，這讓程式獲得了潛力，得以產生新的技術和美學形式。最極端地說法將這種類型的迴圈稱為「分叉炸彈」（forkbomb），採用「阻斷服務」（denial-of-service）攻擊的形式，其中電腦程序會不斷透過自我複製來逐漸耗盡所有可用的系統資源、減慢速度，或讓系統因資源匱乏而導致崩潰。這也反映在 UBERMORGEN 另一個項目的標題《The Project Formerly Known as Kindle Forkbomb》（2012）中，此項目利用了可提取 YouTube 影片中評論的機器過程。演算法會對評論進行彙編並加上標題，以產生出一本電子書，隨後將之上傳到亞馬遜 Kindle 電子商務書店。[^pold1]圖表採取了傳統印刷機的圖片來展示這個過程（見圖 9.3） ，並在安裝版本中進一步結合畫廊地板上的圖表和實際物品（見圖 9.4）[^systemics]。在上述兩種情況下，演算法流程都在運行，模仿並模擬亞馬遜「後印刷術時代」商業模式的操作邏輯，Kindle 網站上便概略描述了這種模式的關鍵原則：「快速進入市場。賺更多的錢。讓事情保持在你的控制之下。」[^kindle]

![diagram3](ch9_3.gif){: style="height: 250px;"}
:   *圖 9.3：UBERMORGEN，《The Project Formerly Known As Kindle Forkbomb》（2013），圖片由藝術家本人提供*

![diagram4](ch9_4.jpg){: style="height: 340px;"}
:   *圖9.4：UBERMORGEN，《The Project Formerly Known As Kindle Forkbomb》（2013），混和媒材裝置藝術，是在丹麥奧胡斯現代藝術博物館（Kunsthal Aarhus）展出之群展「Systemics #2: As we may think (or, the next world library)」的展品之一，該群展的策展人為喬亞西亞・克萊薩（Joasia Krysa）。圖片由藝術家本人及 Kunsthal Aarhus 提供*

## While()

如同上文的排序練習，軟體研究將批判焦點從原始碼轉移到演算法操作，反映了大數據和機器學習（我們將在下一章中討論）的興起。從這個意義上來說，演算法是用來轉換、建構和塑造資料，以便對事物進行分類、排序、叢集、推薦、標記甚至預測。此處的關注焦點不是如何建立高效或最佳化的演算法，而是更深入地理解這些操作維度。在《被操弄的真實：演算法中隱藏的政治與權力》（If… Then: Algorithmic Power and Politics）中，作者泰娜・布赫強調演算法「基本上就是能夠產生對世界進行排序的新方式」[^bucher]。因此，儘管演算法的概念與數學和電腦科學有關，但更廣泛的文化領域已經對演算法產生了興趣，想要探索程序性作業的政治後果。

在《演算法想要什麼》（What Algorithms Want）中，作者艾德・芬恩（Ed Finn）將演算法當作一種「文化機器」的概念並進行探索，他認為演算法「在有效可運算性（圖靈完備性）的反射性障礙內外運行，在宏觀社會層面生產出文化，同時製造出文化物件、過程和體驗。」[^fin]顯然，演算法程式在文化和主體性的組織方面發揮重要作用，但要看透或描述演算法並不容易，因為它們的作用範圍超出了我們能直接體驗到的事物。演算法對生活秩序產生更廣泛的影響，它們在世界上運行，並確實地影響著機器和人類。在《軟體研究》（Software Studies）一書中，安德魯・高菲（Andrew Goffey）釐清了上述的表演式面向：

>「演算法會起作用，但是作為定義不明確的行動網路的一部分，是複雜的權力-知識關係的一部分，在這種情況下，程式行為的副作用等意料之外的後果或許會成為關鍵。當然，演算法作為具有邏輯一致性的構造，其形式性質有著巨大的力量，在技術科學領域尤其如此，但是這種構造的純粹形式性質其實有相當大的模糊地帶，這讓我們得以理解到，演算法除了邏輯一致的形式之外，還包含了更多東西。」[^goffey]  

舉個例子來說，在〈針對演算法的批判性思考和研究〉（Thinking Critically About and Researching Algorithms）一文中，羅布・基欽（Rob Kitchin）解釋了 Facebook 的演算法「EdgeRank」如何利用每位用戶的輸入資料，以個人化的方式排序結果。這些操作並非固定，而是根據脈絡而有所變動[^Kitchin]，而這些操作既是更廣泛的社會技術聚合體的一部分，也是一種不斷發展並受可變條件影響的基礎設施。因此，儘管演算法在運行時似乎有些自主性，但我們必須將演算法理解為具有關係性、偶然性和脈絡性的實體。[^Kitchin2]各類圖表（如上文所展示的那些）可協助您了解演算法作為更廣泛生態的一部分，是如何發揮作用，以突顯其能動力的。

本章提供的除圖表揭示了這一點之外，也展示搜尋或饋給（例如 Facebook 的 EdgeRank 或 Google 的 PageRank 等演算法）等看似簡單的操作，是如何排序資料，並透過由特定權力實例所決定的方式，來將資訊具體化。馬泰奧・帕斯基內利（Matteo Pasquinelli）的文章〈Google 的 PageRank 演算法：認知資本主義和仰賴共同智力得益者的圖表〉（Google’s PageRank Algorithm: A Diagram of the Cognitive Capitalism and the Rentier of the Common Intellect）透過仔細研究 PageRank 背後的政治，提供了更多細節，PageRank 是一種計算特定網頁的重要性以及其在搜尋引擎中的層級位置的超文本演算法。[^Pasquinelli]帕斯基內利的關鍵論點，是該演算法顛覆了以監視和控制為主的集中式全景模型，改而提供一種「生物政治機器」，透過資料監控來擷取時間和活勞動。PageRank 大幅仰賴引用文獻索引這點，更進一步強調了它與本書或任何學術書籍的關聯，以及如何透過評估連結的品質來產生價值（這和社群媒體上的「按讚數」和「朋友數」所產生的注意力價值類似，或者也可透過學術研究成果的計量化來產生），從而催生出剩餘價值的新形式。套句帕斯基內利的話，演算法是種「價值機器」，同時也是一種「抽象機器」和圖表形式。

![diagram5](ch9_5.jpg){:  .float}
:   *圖 9.5：迪恩・金寧（Dean Kenning），《Jackson 5 流程圖》（2017），以麥克筆於紙上繪製而成。圖片由藝術家本人提供*[^jackson5]

但圖表到底是什麼？它是一種功能性工具，亦可在傾向於簡化資訊的教學中使用（資訊圖表便是一例），但撇開這些，圖表也是一種美學實踐的延伸形式，我們期望上述的例子能展現出圖表的這個面向。在本章中，我們試著將流程圖當作一種實驗美學形式來使用，從而反映這類實踐。在本章引言中，我們已經提及圖表作為「抽象機器」的想法，德勒茲和瓜塔里用這個詞來反映物質和形式是有能力自我轉化的：抽象機器展示了「形態發生」（morphogenesis，這是由圖靈所提出的術語，如第五章〈自動產生器〉所述）。透過這種方式，圖表將未來的可能性實例化，這些可能性並非預先確決定，而是具備開放式的推測性虛構事實。[^deleuze]這樣的描述或許聽起來相當深奧，但總體的論點很清楚，從熱力學的觀點來看，甚至可說是科學的：有些事物具備形態發生學上的可能性，並且系統不斷地被能量流（向量）和不會抵消但保有差異的物質穿過。我們最終得出的，是推測性幾何、自我組織形式，以及可以反映動態力量的圖解過程。圖表是「思想的圖像」，其中的思考過程並不包含解決問題，反而是要提出問題。我們想在本章中強調這些獨特的性質，而這與演算法過程的傳統描述有些不同。
{: style="margin-top: var(--line-height);"}

但是我們真的可以將流程圖視為圖表，視為德勒茲術語中的抽象機器嗎？我們想提出的論點是，這幾個詞共通的相似之處，在於它們能夠將問題形象化，並幫助我們在這樣的形象化過程中思索問題，正如德勒茲所言，這個形象化過程可謂是「思想的圖畫」。在〈論圖表（和圖表學實踐）〉（On the Diagram (and a Practice of Diagrammatics)）一文中，作者西蒙・奧沙利文（Simon O'Sullivan）總結了這種推測性方法：

> 「此處的圖表是一種實驗策略，它打亂了敘述和形象等給定之物，最終更讓此外的其他事物得以顯現在台前。這是從已知中產生未知，從可見中產生不可見。圖表可說是一種從意圖中迴避意圖的策略；它涉及某種事物的生產，而這個產生物隨後會向創造它的親代（圖表）「回話」。」[^osullivan]      

雖說上述論點指涉的是繪畫，而非程式設計，但我們或許可以期望，兩者在借助抽象之力的方式，以及以往在程式設計中被隱藏的部分如何被「描繪」在流程圖中這兩方面，能夠找到類似之處可供借鑑，儘管程式設計在這裡被公認為比繪畫更加實用。與圖表繪製一樣，程式設計是一種抽象機器，其功能不僅僅是表示，更是建構一個尚未到來的現實。正如我們在前幾章中所提到的，程式設計是一種抽象形式，我們必須在其中選擇重要的細節，而程式實現，正是程式設計師思考和決策過程的體現。此外，演算法本身其實就是一種充滿突現式（甚至是預測式）潛能的決策機器。[^osullivan2]

在機器學習實際進行預測時，我們特別容易注意到阿德里安・麥肯齊在《機器學習者：一項資料實踐的考古學》（Machine Learners: Archaeology of a Data Practice）中，亦將圖表當作批判性思維的實驗，從而處理機器學習的操作。麥肯齊解釋，在機器學習方面，「編程這件事從我們所謂的符號邏輯圖表，搖身一變成了統計演算法圖表。」[^ML1] 在此他仰賴並引述了德勒茲的建議，即圖表「展示了建構權力的各種力量之間的關係，[更進一步來說]，圖表或抽象機器乃是一張顯示各種力量之間的關係、命運或強度的示意圖。」[^ML2]下一章將繼續討論這一主題，但目前我們想要強調的是，分析演算法或與此相關的原始碼這件事，本身並不特別具有啟發性，除非這麼做能夠揭露出更廣泛的關係聚合體。流程圖是實現上述目的的一種方法，它可以將這些關係繪製為示意圖，從而促使我們對程式設計作業進行批判性思考。

<div class="section exercise" markdown=1>

## 迷你習作：流程圖

**目標：**

* 取得將電腦程式分解為可定義的部分和關係的能力。
* 利用流程圖規劃和建構電腦程式。
* 將流程圖理解為溝通和規劃的手段，以及用於批判性思考的「機器」。
* 從電腦科學和文化的角度理解演算法的概念。

* * *

**任務（RunMe）：**

個人：

* 請重溫之前的迷你習作並選擇技術上最複雜的練習。
* 請畫出一張表達此程式的流程圖（請留意自己選擇要展示哪一些項目）。

團體：

* 請與組員集思廣益，為您的期終作業計畫提出兩個想法（請見下一章的迷你習作）。
* 請繪製兩張流程圖，將計畫的演算法視覺化。

**在 ReadMe 中可供思考的問題：**

* 試著在溝通交流時保持簡單易懂，同時又要再演算法方面保有複雜性時，會碰到那些困難？
* 您的兩個期末計畫想法會面臨怎麼樣的技術挑戰？您將如何解決這些困境？
* 您的個人和小組流程圖在哪一方面能派上用場？
</div>

## 指定讀物

* Taina Bucher, "The Multiplicity of Algorithims," *If…Then: Algorithmic Power and Politics* (Oxford: Oxford University Press, 2018), 19–40.
* Christian Sandvig, "Seeing the Sort: The Aesthetic and Industrial Defense of ‘The Algorithm.’" *Journal of the New Media Caucus* (2015). <http://median.newmediacaucus.org/art-infrastructures-information/seeing-the-sort-the-aesthetic-and-industrial-defense-of-the-algorithm/>.
* Nathan Ensmenger, "The Multiple Meanings of a Flowchart," *Information & Culture: A Journal of History* 51, no.3 (2016): 321-351, Project MUSE, doi:10.1353/lac.2016.0013.

## 延伸讀物

* Ed Finn, “What is an Algorithm,” in *What Algorithms Want* (Cambridge, MA: MIT Press, 2017), 15-56.
* Andrew Goffey, "Algorithm," in Fuller, ed., *Software Studies*, 15-20.
* Marcus du Sautoy, "The Secret Rules of Modern Living: Algorithms," *BBC Four* (2015), <https://www.bbc.co.uk/programmes/p030s6b3/clips>.
* Daniel Shiffman, "Multiple js Files - p5.js Tutorial," *The Coding Train*, <https://www.youtube.com/watch?v=Yk18ZKvXBj4>.

## 註釋

[^Mandel]: 葛麗絲・穆雷・霍普的話在 Lois Mandel 的〈計算機女孩〉一文中有被引用，Lois Mandel, "The Computer Girls," *Cosmopolitan* (April 1967): 52-56。

[^hopper2]: 霍普的 FLOW-MATIC 是第一種使用簡單英語描述來表達作業程序的程式設計語言，是她在雷明頓蘭德公司（Remington Rand）為 UNIVAC 所開發的。FLOW-MATIC 旨在將逐步漸進的方式作為「易於理解的歸檔」來利用，無需事先接受數學和公式、電腦程式設計和語法方面的培訓，並可促進「電腦程式設計小組和營運管理人員之間」的溝通。參見 Remington-Rand Univac, *FLOW-MATIC Programming System* (Philadelphia, PA: Remington Rand Univac, Division of Sperry and Corporation, 1958)。

[^krysa]: 演算法與食譜之間的類比出自：Joasia Krysa and Grzesiek Sedek's "Source Code" entry to *Software Studies: A Lexicon*, 236-243。此類比亦見於包含在本章指定與延伸讀物清單中的近期文獻中：Ed Finn, *What Algorithms Want: Imagination in the Age of Computing* (Cambridge, MA: MIT Press, 2017), 17; and Taina Bucher, *If…Then: Algorithmic Power and Politics* (Oxford: Oxford University Press, 2018), 21。

[^knuth0]: Knuth, *The Art of Computer Programming*, xv。除了列出的步驟外，這本書更以「閱讀本書的流程圖」作為開頭，這點的重要性將在這一章節末尾處變得更明顯，我們也在本書的各個章節和目錄頁中使用流程圖。

[^knuth]: Knuth, *The Art of Computer Programming*, xv-xvi.

[^knuth2]: Knuth, *The Art of Computer Programming*, v.

[^algo]: 「演算法」（algorithm）一詞與「算法」（algorism）這個字有歷史方面的關連，「算法」（algorism）表示使用阿拉伯數字進行算術的過程，該詞源自波斯作家阿布（Abu Ja'far Mohammed ibn Musa al-Khowarizmi，約西元 825 年）。

[^complete]: 大多數現代程式設計語言都具備「圖靈完備性」，這個術語是用來描述可以模擬圖靈機的抽象機器。有關圖靈機的更多資訊，請參閱第五章〈自動產生器〉。

[^first]: 特別是針對計算白努利數的圖表的複雜性，包括操作的分組、迴圈概念的發明（勒芙蕾絲口中的重複和循環），以及根據規則對符號和變數的操縱。這種演算法得設計，是要用於機械計算器之上。當時所設想的巴貝奇分析機已然不只擁有運算的能力，因此在概念上已接近現代電腦。參見：Luigi Federico Menabrea and Ada Lovelace, *Sketch of the analytical engine invented by Charles Babbage* (1842), 694。

[^ada2]: Lovelace Papers, Bodleian Library, Oxford University, 42, folio 12 (February 6, 1841)，本書則直接引用自：Dorothy Stein, ed., "This First Child of Mine," in *Ada: A Life and a Legacy* (1985), 106–107。

[^constant]: Peggy Pierrot, Martino Morandi, Anita Burato, Christoph Haag, Michael Murtaugh, Femke Snelting, and Seda Gürses, *The Techno-galactic guide to software observation* (Brussels: Constant, 2018), 175-186.

[^Ferranti]: Ferranti Limited, Ferranti Pegasus Computer, programming manual, Issue 1, List CS 50,September 1955.

[^stereotype]: 將程式設計視為一種社交活動將挑戰一些與活動相關的主流刻板印象，例如反社會黑客（男性書呆子、大鬍子、不洗澡）。參見：Nathan Ensmenger, “Making Programming Masculine,” in *Gender Codes: Why Women are Leaving Computing*, Thomas J. Misa, ed. (Hoboken, New Jersey: John Wiley & Sons, Inc., 2010), 137。有關合作作工作之優勢的更多資訊，請參閱：Chih Wei Ho, et al, "Examining the impact of pair programming on female students," North Carolina State University. Dept. of Computer Science (2004)。

[^Guattari]: 套句瓜塔里的話，「圖表被設想為一個自生的機器，這種設想不僅賦予了圖表在功能和材料上的一致性，更要求它發揮其多元的他異性記錄，將其從被鎖定在簡單結構關係中的身份裡解放出來。」Félix Guattari, "Machinic Heterogenesis," *Chaosmosis: An Ethico-Aesthetic Paradigm* (Bloomington, IN: Indiana University Press, 1995), 44。「解放」（freeing）一詞在此用於描述（或許是在演算法層面）逃離強加在機器上，預先決定的「圖解順序」。

[^ex]: 這個簡單程式的說明流程圖可見於：<https://gitlab.com/aesthetic-programming/book/-/blob/master/source/9-AlgorithmicProcedures/emoji_flowchart.svg>。

[^Ensmenger]: Ensmenger, "The Multiple Meanings of a Flowchart," 324 & 346.

[^program]: 在教學中，我們會請一個小組準備好提出這個問題，並展示他們如何從技術和概念上予以解決，讓他們思考在更廣泛的文化背景下進行分類的重要性。其他學生隨後再以這種排序練習作為課堂的開始，並聚焦演算法程式。以下是實施排序問題的眾多方法之一：<https://editor.p5js.org/siusoon/sketches/7g1F594D5>。

[^Cirio]: 請見：Paolo Cirio, *Flowcharts: On Systems of Systems*, Artist Monograph (Lulu, 2019)，文本可於 <https://www.paolocirio.net/press/archive/?/id/268/t/FLOWCHARTS/>取得。在此，*Open Society Structures - Algorithms Triptych* (2009) 會是展示我們目的的一個好例子。

[^GWEI]: *GWEI* (2005) 是 *Hacking Monopolism Trilogy* 的其中一部，另外兩部曲為 *Amazon Noir* (2006) 和 *Face to Facebook* (2011)。有關 *GWEI* 的更多資訊，請參閱 <http://www.gwei.org/index.php>。

[^pold]: 有關 *GWEI* 的分析，請參閱：Søren Bro Pold, "Interface Perception: The Cybernetic Mentality and Its Critics: Ubermorgen.com," in Andersen & Pold, eds. *Interface Criticism: Aesthetics Beyond Button* (Aarhus: Aarhus University Press, 2011), 91-113。

[^pold1]: 欲詳細閱讀該計畫，請參閱：Christian Ulrik Andersen and Søren Bro Pold, *The Metainterface: The Art of Platforms, Cities, and Clouds* (Cambridge, MA: MIT Press, 2018), 57-60。

[^systemics]: 有關 UBERMORGEN所創作《The Project Formerly Known as Kindle Forkbomb》的更多資訊，請見：<https://en.wikipedia.org/wiki/The_Project_Formerly_Known_As_Kindle_Forkbomb>；欲知 Kunsthal Aarhus 這項展覽的脈絡，請見：<https://www.e-flux.com/announcements/31936/systemics-2-as-we-may-think-or-the-next-world-library/>。

[^kindle]: Kindle 平台的更完整說明，請參閱：<https://kdp.amazon.com/en_US/>。

[^bucher]: Taina Bucher, *If…Then: Algorithmic Power and Politics* (Oxford: )Oxford University Press, 2018), 20.（譯註：正體中文版的書名為：〈被操弄的真實：演算法中隱藏的政治與權力〉，由臺灣商務出版社於 2021 年初版，ISBN：9789570533040）

[^fin]: Finn, *What Algorithms Want: Imagination in the Age of Computing*, 34.

[^goffey]: Andrew Goffey, "Algorithm," in Fuller, ed. *Software Studies*, 19.

[^Kitchin]: Rob Kitchin, "Thinking Critically About and Researching Algorithms”, in *Information, Communication & Society* (2016), 16.

[^Kitchin2]: Kitchin, "Thinking Critically About and Researching Algorithms," 10.

[^Pasquinelli]: Matteo Pasquinelli, "Google's PageRank Algorithm: A Diagram of the Cognitive Capitalism and the Rentier of the Common Intellect," in Konrad Becker and Felix Stalder, eds., *Deep Search: The Politics of Search Beyond Google* (London: Transaction Publishers: 2009)。PageRank 演算法係由謝爾蓋・布林（Sergey Brin）和勞倫斯・佩吉（Lawrence Page）於 1990 年編寫，似乎體現了 Google 的壟斷力量。

[^deleuze]: 德勒茲和瓜塔里對圖表的具體解釋過於複雜，無法在此詳細介紹。簡而言之，他們使用圖表的概念來模擬意義的動態，以及逸脫在意義之外的東西：「圖表或抽象機器的功能並不是再現（甚至也不是用來再現真實的事物），而是建構一個尚未到來的真實，一種新型的現實。」<http://frequencies.ssrc.org/2011/12/19/diagrammic-thinking/>。更多相關想法，請見：Gilles Deleuze and Félix Guattari, *A Thousand Plateaus* (1980)。

[^jackson5]: 這張流程圖是基於 Jackson 5 於 1978 年發行的〈Blame It On the Boogie〉的開場歌詞。

[^osullivan]: Simon O’Sullivan, "On the Diagram (and a Practice of Diagrammatics)," in Karin Schneider and Begum Yasar, eds., *Situational Diagram* (New York: Dominique Lévy, 2016), 17.

[^osullivan2]: 此敘述也反映了圖表跨時間運作的方式：「這個圖表是否也涉及對過去、現在和未來之間關係不同的理解？這是在不同時間之間『畫線』，是電路的建構和隨之而來的回饋迴路；這是把時間放在任何給定系統（或實踐）之中來理解，而這種『特定』的時間並不是中性的背景。這可能會涉及繪製圖錶，來展示不同類型的未來重新回到現在的方式（並決定我們在此時此地是如何進行動作或創造事物）。或者，我們當然可以說這是在描繪現在本身如何涉及對過去的重新設計（此處的過去可理解為資源和所謂的『活檔案』），從而導向不同的未來。」O’Sullivan, "On the Diagram (and a Practice of Diagrammatics)," 24。

[^ML1]: Adrian Mackenzie, *Machine Learners: Archaeology of a Data Practice* (Cambridge, MA: MIT Press, 2017), 23.

[^ML2]: Mackenzie, *Machine Learners: Archaeology of a Data Practice*, 17.

[^recipe]: Although the concept of algorithm is rooted in computer science, scholars from other fields like cultural and media studies take on the technical concept of algorithm and explore its wider cultural consequences and political implications. The analogy of algorithms as recipes can also be seen here: Ed Finn, *What Algorithms Want: Imagination in the Age of Computing* (Cambridge, MA: MIT Press, 2017), 17; and

[^prediction]: Adrian Mackenzie, "The Production of Prediction: What Does Machine Learning Want?", *European Journal of Cultural Studies* 18, no.4-5 (2015): 429–445.

[^flowcharts2]: See Stephen Morris and Orlena Gotel, "The Role of Flow Charts in the Early Automation of Applied Mathematics," *BSHM Bulletin: Journal of the British Society for the History of Mathematics* 26, no. 1 (March 2011): 44–52, <https://doi.org/10.1080/17498430903449207>; and Nathan Ensmenger, "The Multiple Meanings of a Flowchart," *Information & Culture: A Journal of History* 51, no.3 (2016): 321–51, <https://doi.org/10.1353/lac.2016.0013>.
