Title: 設計說明
Slug: design-notes
page_order: -1

<!--
Open Source Publishing questions the influence and affordance of digital
tools through its practice of (commissioned) graphic design, pedagogy,
and applied research. Hence they prefer to use exclusively free and open
source softwares (F/LOSS). Currently the group is composed of people
with backgrounds in graphic design, typography, and software development. They
find excitement in the cross-over between its members respective fields
and competences.
-->

## 本書佈局

自 2013 年以來，開源出版（OSP）便使用 HTML、CSS 和 Javascript 等網路技術製作印刷出版物，因此得以開發出新的佈局類型和出版管道，而其中又可從相同的內容產生出多種輸出格式，如網站、書籍、電子書等。本書內容是以 Markdown 標記式語言編寫，並透過協作工具 Git 同步於作者與設計者之間，隨後再藉由 [Pelican CMS](https://blog.getpelican.com/) 將原始碼轉換為 HTML。在網頁版本上，讀者可以查看並即時測試每一章的範例程式碼。本書的紙本書版本則利用 polyfill [paged.js](https://www.pagedjs.org/) 加強 CSS 分頁媒體設計的瀏覽器支援。本書的上述兩個版本互相獨立，同時又互為補充。

## 字體

本書選擇的所有字體皆是利用程式碼繪製。

本書中使用的正文和標題字體出自 A.V. Hershey 於 1960 年代繪製，專為向量繪圖儀開發的大字體系列。囿於繪圖儀的技術性限制，曲線被分割成數條細小筆直的線。此外，由於向量繪圖儀是以單一線寬繪製，需透過讓線條彼此靠近來模擬不同的線寬。書中的特定字體是該字體用 Metafont 重新詮釋後的變體。內文字體來自 Hershey Noailles 家族，由 Antoine Gelgon 詮釋，將分段曲線轉化為真實曲線。標題使用的 Hershey Times 字體則由 Gijs de Heij 和 Simon Egli 詮釋，並使用（或濫用）Metafont「看起來最舒服的曲線」產生其特定形狀。

程式碼範例則以 OCR-pbi 顯示，OCR-pbi 是 Antoine Gelgon 使用 Metafont 繪製的字體系列。該字體的骨架以 Adrian Frutiger 繪製的 OCR-B 為基礎，而 OCR-B 主要目標是繪製機器和人類均可閱讀的等寬字體。