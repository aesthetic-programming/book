Title: 貢獻者名單
Slug: contributors
page_order: -4
sketch: p5_SampleCode.zh_TW/contributors/sketch.js


<sketch
  src="p5_SampleCode.zh_TW/contributors/sketch.js"
  lang="javascript"
  data-executable
  data-download-link="https://gitlab.com/aesthetic-programming/book/-/archive/master/_Book-master.zip?path=public/p5_SampleCode.zh_TW/contributors" />
