Title: 翻譯筆記
Slug: translation-notes
page_order: -5


## 孫詠怡

在《美學程式設計》的第一個版本中，Geoff 和我在〈前言〉和〈第一章：入門〉部分中討論了對於這本書的想像。

> 我們想像，這只是本書的第一次迭代，期望之後能看到整本書經過多種重新挪用和分叉，以便人們使用現有框架進行修改，例如添加新的章節、範例和練習，以及更多相關的內容和參考資料，促進程式設計和思維之間的互動。（2021，第 42 頁）

在台灣版之前，學者 Sarah Ciston 和 Mark Marino 分岔了這個存儲庫，並寫了一篇名為[〈如何分岔一本書：出版的激進轉變〉](https://markcmarino.medium.com/how-to-fork-a-book-the-radical-transformation-of-publishing-3e1f4a39a66c)的短文。他們在第 8 章〈酷詢資料〉和第 9 章〈演算法過程〉之間新增第 8.5 章〈對話回饋〉，從批判的和美學的層面，重點關注了使用 Python 程式語言的對話聊天機器人。除了加入了新的章節和內容之外，這種分岔與情境化方式，啟發我思考寫作和編碼之間的平行關係，尤其是「[以編寫程式的方式編寫文章](https://darc.au.dk/fileadmin/DARC/newspapers/toward-a-minor-tech-online-sm.pdf)」。

與台灣的團隊一起，我們已經在這個作為分支版本的翻譯／分岔計畫上工作了一年多。過程中，我們花了很多時間討論分岔、翻譯與版本控制之間的關係（例如在 2022 年的[批判代碼研究工作小組](https://wg.criticalcodestudies.com/index.php?p=/discussion/132/week-4-translating-aesthetic-programming)中的討論）。在倫敦的工作坊由紫彤、Geoff 和我共同組織，由 [Delfina Foundation](https://www.delfinafoundation.com/whats-on/aesthetic-programming-forking-workshop/)主辦，我們希望思考超越數位媒介的翻譯能夠怎麼進行，因此我們加入了用肢體做翻譯的活動。參與者被分為兩組，在不使用鍵盤、電腦和螢幕的情況下，思考如何集體展示「迴圈」的概念。根據空間和時間的條件，兩組參與者使用算法的參數執行他們的「身體迴圈形式」，其中的條件結構了迴圈的執行方式（也就是在算法中什麼時候繼續或中止）。

作為一位在殖民地城市香港出生和成長的個體，有機會與台灣人在亞洲的語境下一起工作，讓我更加敏銳地察覺，英語語言和文化的主導地位以及它如何影響我作為一個學者的引註實踐。此外，與團隊密切合作本身已是一趟豐富的旅程，我們不僅學習和討論翻譯的內容，也討論如何調動、尊重和組織集體和社群實踐。

* * *

In the first version of *Aesthetic Programming*, Geoff and I discussed the imagination of the book in both the Preface and Chapter 1 - Getting started.

> We imagine this is just the first iteration of this book and we hope to see many re-appropriations and forks of the entire book, so people can use the existing framework to make modifications, such as adding new chapters, examples and exercises, as well as more related content and references that facilitate the interactions between programming and thinking.  (2021 p. 42)

Prior to this Taiwanese translated version, scholars Sarah Ciston and Mark Marino forked the repository and wrote the short article entitled "[How to Fork a Book: The Radical Transformation of Publishing](https://markcmarino.medium.com/how-to-fork-a-book-the-radical-transformation-of-publishing-3e1f4a39a66c)". They added the Chapter 8.5-TalkingBack, which is situated between Chapter 8 - Que(e)ry Data and Chapter 9 - Algorithmic Procedures, focusing on the critical and aesthetic aspects of conversational chatbots with the use of Python programming language. Apart from new sections and content, this way of forking and contextualizing their chapter has opened up my mind in terms of thinking about the parallel between writing and coding, especially  '[writing an article as if writing a piece of software](https://darc.au.dk/fileadmin/DARC/newspapers/toward-a-minor-tech-online-sm.pdf)'

Together with the Taiwanese crew, we have been working on this translation/forking project, as a fork version, for more than a year. Throughout the course, we spent a lot of time discussing the relationship between forking, translating and versioning (for example in [the critical code studies working group](https://wg.criticalcodestudies.com/index.php?p=/discussion/132/week-4-translating-aesthetic-programming) in 2022). During the London workshop that Tzu-Tung, Geoff and I co-organised, which was hosted by [Delfina Foundation](https://www.delfinafoundation.com/whats-on/aesthetic-programming-forking-workshop/), we wanted to think about how translation might work beyond the digital and as such we included the bodily translation activity. The participants were divided into two groups and were required to work without a keyboard, a computer and a screen to think about how to demonstrate the concept of loop collectively. Given the space and time, the two groups performed their "bodily loop form" with algorithmic parameters, in which the conditions structured how the loop is performed (when to continue/halt in an algorithm).

With my subject position, who was born and raised in the colonial city of Hong Kong, as well as having the opportunities to work with Taiwanese in the Asia context allow me to be more sensitive to the dominance of English language and culture and how that affects my citation practices as a scholar. Additionally, it has been a fruitful journey to work closely with the team. It is not just only to learn and discuss about the translated content, but also how to mobilise, respect and organise collective and community practice. 


## Geoff

《美學程式設計》的初衷是從內部探索程式設計的技術和文化想像，因此將其翻譯成另一種語言的工作，不可能是可以直接指派給任何單一個體的技術程序。此外，如果挑戰是像分岔軟件一樣分叉這本書，那麼將翻譯視為一種分叉，作為一種集體化的機會似乎是很明顯的。

分岔／翻譯這本書本身就是一種迴圈，讓我們可以將所有新的迭代視為一個無盡自我更新的過程，如前所述地陷在自身生成的迴圈中。這點尤其重要，因為要在跨文化和跨意識形態的情況下，將任何文本翻譯成另一種語言，同時保留其意義，本來就是困難的 - 對我來說，這涉及到一個遠離我直接經驗的地緣政治情境。

我們不難理解，為何後殖民主義和女性主義學者會質疑英語作為全球化中，優勢的（男）人造語言的地位。這對於置身白人性與性別歷史之中的人來說，更能深刻體會。當然中文也存在其他殖民的影響。而就程式語言來說，情況也是如此，它甚至強化了英語的霸權地位。在我們共同工作的各個層面，我們已經開始將這些問題視為翻譯問題，認知到人與機器解釋指令的多種運行層次。藉由這種方式，翻譯成為重新思考和擴展資源的機會，既特定於此書本書，也更廣泛於台灣的語境中使其公共化。

* * *

Given that *Aesthetic Programming* set out to explore the technical as well as cultural imaginaries of programming from its insides, it follows that its translation into another language could not be a straightforward technical procedure assigned to any one individual. Moreover, if the provocation was to fork the book the way we fork software, then it seemed obvious to think of translation as a kind of forking too, and as an opportunity to collectivise.

In forking/translating the book, it's a kind of loop, enabling us to think of any new iteration as a process that can endlessly renew itself, stuck in a loop of its own becoming as we put it previously. This is all the more important given the inherent difficulty of translating any text into another language whilst retaining its meaning, but also across cultures and ideologies - in my case, to a geopolitical context far from my direct experience.

It’s relatively easy to see why this is important with English language which has been challenged by postcolonial and feminist scholars for the way it imposes itself as the predominant man-made language of globalisation — deeply felt by someone implicated in these histories in terms of whiteness and sex. In Chinese, there are other colonial implications of course. This is no less the case with programming languages either that also tend to reinforce the hegemony of English. We have set out to consider such issues as a problem of translation at all levels of our work together — recognising the many operative layers through which humans and machines interpret instructions or not. In this way translation becomes an opportunity to rethink and extend some of our sources, both specific to the book and more broadly to the Taiwanese context in which it is made public. 


## 李佳霖

2022 年 3 月我剛成立自牧文化，規劃以出版和展覽作為主要的產出形式。學生時期雖然曾經陸續在出版社和雜誌社實習，或是以譯者的身份參與書籍的製作，但對於書籍版權完全沒有概念，直到自己開了公司實際去接觸版權代理商，才摸索出版權費在一本書的製作成本中所佔的比例——比想像中高許多。

此外，我自己在做研究的過程中經常仰賴開放取用（open-access）的書籍和期刊，甚至，我必須承認更多的是「盜版」的英文電子書。究竟在這個資訊高度生產並流通於數位載體之中的時代，我們與知識的關係是什麼？這是我一直都非常關心的問題。

2017 年的秋季學期，我在中國美術學院網絡社會研究所的[「軟體研究：從機器－人的角度重新形構數位文化」](https://hackmd.io/@v_QEqW1GRbyU407acQX4Rg/SJUNnTuF-)這門課中，第一次認識「軟體研究」（software studies）這個研究取徑，並初步理解到軟體開發中的版本控制，這對我後來思考出版中的「版本」有很大的啟發。《美學程式設計》看待書籍的方式是將「寫 code」與「寫作」視為同一件事，他們同時發生、運行、迭代、保存。

感謝詩雨將我引介給原先已經在進行台灣版翻譯的工作小組，感謝台北數位藝術中心的支持，讓這個計畫能夠在 2023 年將所有內容初步中文化。2023 年夏天，我們也在台北數位藝術中心組織了一次[工作坊](https://dac.taipei/project/aesthetic-programming-workshop-v1/)，簡單扼要地介紹分岔的概念，接著和參與者一起就「coding」、「fork」及「sketch」三個「問題詞彙」展開譯法的討論，為這本書的詞彙表增添豐富度。來自藝術、設計、教育、軟體開發等不同背景的參與者，提出了對於專有名詞翻譯的有趣見解，同時也讓我們意識到，將這些口語討論收束為書面文字的挑戰；更進一步來說，我們也思考在活動規劃層面，如何能更有機地「策動」參與者。

《美學程式設計》是一本不斷分岔的書，所以在概念上沒有一個「定稿」或者「權威本」。我們的下一步會持續透過更多的形式，邀請不同背景的人加入分岔的行列，並共同實驗如何發展出一種自身可持續的「出版—銷售」模式。

* * *

In March 2022, I had just established my own company, Zimu Culture, with a focus on publishing and exhibitions as the primary forms of output. During my student years, I had interned at publishing houses and magazines, and participated as a translator in book production. However, I had no concept of book copyrights until I opened my own company and came into contact with copyright agents. It was only then that I discovered the proportion of copyright fees in the production cost of a book was much higher than I had imagined.

Furthermore, in my research process, I often relied on open-access books and journals, and I must admit, more often on "pirated" English e-books. In an era where information is highly produced and circulated in digital formats, what is our relationship with knowledge? This is a question that has always deeply concerned me.

In the fall semester of 2017, in a course titled "Software Studies: Reconstructing Digital Culture from the Perspective of Machine-Human Relations" at the Institute of Network Society Research at the China Academy of Art, I first encountered the research approach of "software studies" and began to understand version control in software development. This understanding greatly inspired my later thoughts on "versions" in publishing. "Aesthetic Programming" views the act of "writing code" and "writing" as the same thing—they happen, run, iterate, and are preserved simultaneously.

I am grateful to Shi Yu for introducing me to the translation team already working on the Taiwanese version, and I appreciate the support from the Taipei Digital Arts Center, which allowed this project to begin its initial Chinese translation in 2023. In the summer of 2023, we also organized a workshop at the Taipei Digital Arts Center, providing a brief introduction to the concept of forking. Subsequently, we discussed the translation of three "problematic terms": "coding," "fork," and "sketch," enriching the vocabulary of the book. Participants from diverse backgrounds such as art, design, education, and software development offered interesting insights into the translation of specific terms, making us aware of the challenge of translating spoken discussions into written texts. Furthermore, we contemplated how to organically "orchestrate" participants in the planning of activities.

"Aesthetic Programming" is a continuously branching book, so there is no "final draft" or "authoritative version" conceptually. Our next steps will involve inviting people from different backgrounds to join the branching process and experiment together on developing a sustainable "publishing-sales" model.


## 徐詩雨

我會加入《程式美學設計》台灣翻譯的工作小組，是與譯者吳礽喻自主翻譯環境人文女性主義學者 Astrida Neimanis《Bodies of Water》（目前暫譯《水（群）體》）的時候不小心被推坑。

如果說翻譯《水（群）體》的過程是將那些從英文行文構字中，拆解出女性主義學者想要強調的更深層與交纏的多層意義，用我較為熟練的中文說出來；那翻譯《程式美學設計》，特別是繁體中文的翻譯，對於習慣直接使用英文原本名稱的台灣程式圈語境，到底代表什麼意義？這是此個的我仍然思考的問題。

或許可以回到我之前有次為了介紹翻譯《水（群）體》的過程所提出的「集體翻譯作為關照」（Collectively Translating as Care）演講標題。當我們在翻譯的過程舉行工作坊，第一手地接觸到對這本書有興趣的讀者們。大家對這個主題熱烈卻又保持的各自稍稍偏斜的想法，產生出了像是美學／美感、分岔／開枝／復刻等等的討論。此刻，這本書就是一個活生生的相遇之地，等待著未來迭代的發生。

* * *

I will join the translation team for "Aesthetic Programming" in Taiwan, having been inadvertently pulled into the project while translating the work of environmental humanities feminist scholar Astrida Neimanis, titled "Bodies of Water" (currently tentatively translated as "水（群）體").

If the process of translating "Bodies of Water" involves deconstructing the deeper and intertwined layers of meaning that feminist scholars emphasize in English writing and expressing them in my proficient Chinese, then what does translating "Aesthetic Programming," especially in Traditional Chinese, signify in the context of the Taiwanese programming community accustomed to using English terms directly? This is a question I am still contemplating.

Perhaps I can revisit the title of a talk I gave previously about the translation process of "Bodies of Water," titled "Collectively Translating as Care." When we hold workshops during the translation process, we directly engage with readers interested in this book. The enthusiasm mixed with slightly divergent perspectives generates discussions on topics such as aesthetics, branching, and replication. At this moment, the book is a lively meeting place, waiting for future iterations to unfold.


## Tzu Tung Lee 李紫彤

翻譯，不僅是語言對語言的平行轉換，更是一種權力的複寫與轉移。

我還記得自己多年前在美國留學，為英語不流利而感到的羞恥。這些羞恥的幽靈至今還是隱然出沒，圍繞著這世界既定的結構。台灣政府稱官方語言共21種，但實際學校課綱僅授中文及英語。在國家系統的教育及科技發展下，其他的（others）文化、語言與知識正系統性地被排擠、遺忘、屠殺（epistemicide）。

由於 *Aesthetic Programming* 以女性主義的方法「開源開放」內容，其英翻中過程，成為實驗解殖的機會。 

2021 年時，Winnie Soon 表達翻譯成中文的意願。第一名志願中文翻譯者開始加入團隊，隨後我們在台灣的 [g0v](https://g0v.tw/) 尋找更多翻譯隊友，討論將英文為中心的程式語言「翻譯」的意義。2023 年，我與 Winnie、Geoff 在倫敦 Delfina Foundation 執行翻譯工作坊，並稱之為「Forking Workshop」。

就像是開源社群中，原有的程式語言提供給任何人分岔（fork）、修改、再融合（merge）的權利，我們將這個概念融合在翻譯行為中：翻譯可以是多元的，一句話、一個字能分岔出多種面對當地文化的表達可能。在工作坊中，我們不將翻譯限定為字面及字面間的轉換，更讓參與者將內容分岔成身體語言、圖像、聲音、註解 ⋯⋯，在過程中，疏散（distributing）、顛覆（queering）英文與中文、原文與譯本間既有的二元性（binary）。

我也希望，作為這本書閱讀者的你，也能一起找到自己的「翻譯」。一同看進「翻譯」的眼睛、在依賴著「翻譯」作為繩索閱讀前行的同時，也以自己的舞步跳離「翻譯」內部黑暗的殖民陰影。

* * *

Translation is not merely a parallel conversion of language to language; it is also a rewriting and transfer of power.

I still remember the shame I felt years ago as a student in the United States, struggling with my fluency in English. The ghosts of that shame still linger, haunting the established structures of the worlds. The Taiwanese government recognizes 21 official languages, yet the actual school curriculum primarily teaches only Chinese and English. Under the national compulsory education, the knowledge, language and the culture of "others" are systematically marginalized, forgotten, and epistemicide.


As *Aesthetic Programming* "open sources" its content through a feminist approach, the English-to-Chinese translation process becomes an opportunity for a decolonization experiment.

In 2021, Winnie Soon expressed their desire to translate the book into Chinese. Quickly, the first volunteer Chinese translator joined the team, later, we searched for more translation comrade through Taiwan's g0v, discussed together the meaning of translation in the English-centric technolical field. In 2023, Winnie, Geoff, and I conducted a translation workshop at London Delfina Foundation, and named "Aesthetic Programming - Forking Workshop."

Drawing inspiration from the open-source programming community's "fork" concept, the workshop participants are encouraged to branch off content into various forms, such as body language, images, sounds, annotations, etc. Distributing and Queering the existing binary between original text and translation, English and Chinese translation.

I also hope that you, as a reader of this book, can embark on a quest to find your own 'translation.' Together, let us look into the eyes of 'translation', hold on it as a lifeline moving forward in our reading, and at the same time, dance away from the dark colonial shadows lurking within 'translation' with our unique steps.


## Andrew Lin 林彥璋

2021年下半年，我透過紫彤的介紹參與了《美學程式設計》的翻譯計畫（現在想想或許應該正名為「分岔計畫」）。一開始，我僅視它為翻譯工作，將英文內容逐篇翻譯為中文，接著將這些翻譯內容上線供人閱讀。然而，經過多次與其他參與者的討論後，我逐漸理解這個計劃最核心的精神，即透過開源授權實現的「分岔 (fork)」。

在這計劃中，我主要協助維護中文版本的 Git 存儲庫，將翻譯推送到該庫，並產成中文網頁和 PDF 電子書。「開源出版」明顯地好處是消除了傳統書籍的再版限制。任何人都可以在 *Aesthetic Programming* 的 GitLab (https://gitlab.com/aesthetic-programming/book) 頁面上輕鬆分岔出自己的分支，進行修改，甚至擴充章節後重新發布。

這個觀念讓我深感振奮，它顛覆了我們對「書籍作者」的傳統想法，並引出新的討論：當一本書可以持續被分岔、二次編輯、重製發布時，該如何標記版本號（Edition）？在 Git 版本控制系統中，每次推送都會產生一個哈希碼，宛如對應「當前內容」的快照（Snapshot）。這是否意味著開源出版也需要發展一種特殊的版本標記方式呢？

當然，這本書始終是關於程式設計。不同於其他程式設計書籍偏重於程式語法的介紹，本書有更多關於文化的討論以及豐富的參考資料。我特別喜愛每個章節開頭的「setup()」和結尾的「While()」小節，總是能從中發現有趣的小知識或是歷史。希望讀者在閱讀這本書的時候，也可試想：「如果你是本書的編輯群，你會想要加入甚麼章節？」然後試著 fork 它！

* * *

In the latter half of 2021, I joined the translation project of "Aesthetic Programming" through the introduction of Tzutung (紫彤). Looking back, perhaps it should be appropriately named the "Forking Project." Initially, I regarded it simply as a translation task, translating English content into Chinese and then publishing these translations online for readers. However, after numerous discussions with other participants, I gradually understood the project's core spirit, which revolves around the "forking" concept achieved through open-source licensing.

In this project, my main role involves maintaining the Chinese version of the Git repository, pushing translations to the repository, and generating Chinese web pages and PDF ebooks. The obvious advantage of "open-source publishing" is the elimination of traditional book reprint restrictions. Anyone can easily fork their branch on the Aesthetic Programming GitLab page (https://gitlab.com/aesthetic-programming/book), make modifications, or even expand chapters and republish.

This concept energizes me, as it subverts our traditional notions of "book authors" and sparks new discussions: when a book can continuously be forked, edited, and reissued, how should version numbers (editions) be marked? In the Git version control system, each push generates a hash code, resembling a snapshot of the "current content." Does this imply that open-source publishing also needs to develop a unique version marking method?

Of course, this book is always about programming. Unlike other programming books that focus on introducing programming syntax, this book delves more into cultural discussions and provides rich reference materials. I particularly enjoy the "setup()" at the beginning of each chapter and the "While()" section at the end, always discovering interesting bits of knowledge or history. I hope that as readers engage with this book, they also consider: "If you were part of the editorial team, what chapter would you want to add?" and then try forking it!